﻿#if UNITY_EDITOR
using _Project.Scripts.Atlas;
using System.IO;
using System.Linq;
using _Project.Scripts.Data.Scriptable.Atlas;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEngine;

namespace _Project.Scripts
{
    internal class CustomBuildProcess : IPreprocessBuildWithReport
    {
        public int callbackOrder => 0;

        public void OnPreprocessBuild(BuildReport report)
        {
            FirmwareVersion_SetFileName();
        }

        private void FirmwareVersion_SetFileName()
        {
            DirectoryInfo dir = new DirectoryInfo(Application.streamingAssetsPath);
            FileInfo fileInfo = dir.GetFiles(("*.zip")).FirstOrDefault();

            if (fileInfo == null)
            {
                Debug.LogError($"CustomBuildProcess ~ Firmware zip file missing in folder: {Application.streamingAssetsPath}");
                return;
            }

            FirmwareVersion firmwareVersion = AssetDatabase.LoadAssetAtPath<FirmwareVersion>("Assets/_Project/Data/Atlas/FirmwareVersion.asset");

            if (!firmwareVersion.fileName.Equals(fileInfo.Name))
            {
                firmwareVersion.fileName = fileInfo.Name;
                EditorUtility.SetDirty(firmwareVersion);
                AssetDatabase.SaveAssets();
            }
            
            Debug.Log($"CustomBuildProcess ~ Firmware version > fileName = {firmwareVersion.fileName}");
        }
    }
}
#endif