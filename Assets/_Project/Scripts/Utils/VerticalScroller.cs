﻿using UnityEngine;

namespace _Project.Scripts.Utils
{
    public class VerticalScroller : MonoBehaviour
    {
        [Tooltip("Content of desired ScrollRect")]
        public RectTransform ScrollableArea;

        [Tooltip("Center display area (position of zoomed content)")]
        public RectTransform Center;

        [Tooltip("Index of the item to be in center on start. (optional)")]
        public int m_index = -1;
        public int Index {
            get { return m_index; }
            set {
                m_index = value;
            }
        }

        [Range(0, 1f)]
        public float MinAlpha = 0.5f;

        [Range(0, 1f)]
        public float MinScale = 0.7f;

        public void Awake()
        {
            if (!Center) {
                Debug.LogError("Please define the RectTransform for the Center viewport of the scrollable area");
            }
        }

        public void Start()
        {
            if(ScrollableArea.childCount > 0) {
                var deltaX = ScrollableArea.GetChild(0).GetComponent<RectTransform>().rect.width * ScrollableArea.childCount / 3 * 2;
                Vector2 startPosition = new Vector2(-deltaX, ScrollableArea.anchoredPosition.y);
                ScrollableArea.anchoredPosition = startPosition;
            }

            if (m_index > -1) {
                m_index = m_index > ScrollableArea.childCount ? ScrollableArea.childCount - 1 : m_index;
                SnapToElement(m_index);
            }
        }

        public void Update()
        {
            if (ScrollableArea.childCount < 1) {
                return;
            }

            float previousDistance = -1;
            bool centerFinded = false;

            for (var i = 0; i < ScrollableArea.childCount; i++) {
                var child = ScrollableArea.GetChild(i);
                var canvasGroup = child.GetComponent<CanvasGroup>();

                float distance = Camera.main.WorldToScreenPoint(Center.position).x - Camera.main.WorldToScreenPoint(child.GetComponent<RectTransform>().position).x;
                distance = Mathf.Abs(distance);

                if(previousDistance >= 0 && distance >= previousDistance && !centerFinded) {
                    m_index = i - 1;
                    if(m_index < 0) {
                        m_index = 0;
                    }
                    centerFinded = true;
                    if(canvasGroup != null) {
                        canvasGroup.interactable = true;
                    }
                } else if(i == ScrollableArea.childCount - 1 && !centerFinded) {
                    m_index = i;
                    centerFinded = true;
                    if(canvasGroup != null) {
                        canvasGroup.interactable = true;
                    }
                } else {
                    previousDistance = distance;
                    if(canvasGroup != null) {
                        canvasGroup.interactable = false;
                    }
                }

                // Magnifying effect
                float scale = Mathf.Max(MinScale, (2 * (MinScale - 1) / Center.rect.width) * distance + 1);
                child.GetComponent<RectTransform>().transform.localScale = new Vector3(scale, scale, 1f);

                if(canvasGroup != null) {
                    float fade = Mathf.Max(MinAlpha, (2 * (MinAlpha - 1) / Center.rect.width) * distance + 1);
                    canvasGroup.alpha = fade;
                }
            }
            ScrollingElements(-ScrollableArea.GetChild(m_index).GetComponent<RectTransform>().anchoredPosition.x);
        }

        private void ScrollingElements(float position)
        {
            float newX = Mathf.Lerp(ScrollableArea.anchoredPosition.x, position, Time.deltaTime * 5f);
            Vector2 newPosition = new Vector2(newX, ScrollableArea.anchoredPosition.y);
            ScrollableArea.anchoredPosition = newPosition;
        }

        public void SnapToElement(int element)
        {
            float deltaElementPositionX = ScrollableArea.GetChild(0).GetComponent<RectTransform>().rect.width * element;
            Vector2 newPosition = new Vector2(-deltaElementPositionX, ScrollableArea.anchoredPosition.y);
            ScrollableArea.anchoredPosition = newPosition;
        }

        public void ScrollLeft()
        {
            float deltaLeft = ScrollableArea.GetChild(0).GetComponent<RectTransform>().rect.width / 1.2f;
            Vector2 newPositionLeft = new Vector2(ScrollableArea.anchoredPosition.x - deltaLeft, ScrollableArea.anchoredPosition.y);
            ScrollableArea.anchoredPosition = Vector2.Lerp(ScrollableArea.anchoredPosition, newPositionLeft, 1);
        }

        public void ScrollRight()
        {
            float deltaRight = ScrollableArea.GetChild(0).GetComponent<RectTransform>().rect.width / 1.2f;
            Vector2 newPositionRight = new Vector2(ScrollableArea.anchoredPosition.x  + deltaRight, ScrollableArea.anchoredPosition.y);
            ScrollableArea.anchoredPosition = newPositionRight;
        }
    }
}
