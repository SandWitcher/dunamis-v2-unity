﻿using UnityEngine;

namespace _Project.Scripts.Utils
{
    public class InstantiatePrefab : MonoBehaviour
    {
        public void InstantiateGameObject(GameObject prefab)
        {
            Instantiate(prefab);
        }
    }
}
