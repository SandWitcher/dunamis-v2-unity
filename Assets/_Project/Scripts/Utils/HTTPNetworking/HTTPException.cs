using BestHTTP;

namespace _Project.Scripts.Utils.HTTPNetworking
{
    public class HTTPException: System.Exception
    {
        public HTTPResponse Response {
            get; protected set;
        }

        public static HTTPException CreateResponseException(HTTPResponse response) {
            return new HTTPException(response.Message, response);
        }

        public HTTPException(string message, HTTPResponse response) : base(message) {
            Response = response;
        }
    }
}