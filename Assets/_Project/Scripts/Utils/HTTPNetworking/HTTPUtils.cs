using BestHTTP;

namespace _Project.Scripts.Utils.HTTPNetworking
{
    public static class HTTPResponseExtension
    {
        public static bool IsInformational(this HTTPResponse response) {
            return response.StatusCode / 100 == 1;
        }

        public static bool IsRedirection(this HTTPResponse response) {
            return response.StatusCode / 100 == 3;
        }

        public static bool HasClientErrors(this HTTPResponse response) {
            return response.StatusCode / 100 == 4;
        }

        public static bool HasServerErrors(this HTTPResponse response) {
            return response.StatusCode / 100 == 5;
        }
    }
}