﻿using Crosstales;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace _Project.Scripts.Utils
{
    [RequireComponent(typeof(RectTransform), typeof(ScrollRect))]
    public class ScrollRectMagnificent : MonoBehaviour, IEndDragHandler, IDragHandler, IBeginDragHandler {

        public GameObject Prefab;
        public RectTransform content;
        public RectTransform Placeholder;
    
        [Range(0,1)]
        public float disabledScale;

        [Range(0, 1)]
        public float disabledFade;

        public float switchDuration;
        public int placeholderCellIndex;

        private float placeholderOffset;
        private float placeholderPosition;

        private Transform currentCell;

        private Vector2 placeholderRange;

        private float prefabDimension;

        private bool horizontalScroller;
        private int contentChildCount;

        private void Start()
        {
            horizontalScroller = GetComponent<ScrollRect>().horizontal;

            if (horizontalScroller)
            {
                prefabDimension = Prefab.GetComponent<RectTransform>().rect.width;
                placeholderPosition= Placeholder.position.x;
                placeholderOffset = prefabDimension / 2 + content.GetComponent<HorizontalLayoutGroup>().spacing / 2;
            }
            else
            {
                prefabDimension = Prefab.GetComponent<RectTransform>().rect.height;
                placeholderPosition = Placeholder.position.y;
                placeholderOffset = prefabDimension / 2 + content.GetComponent<VerticalLayoutGroup>().spacing / 2;
            }

            placeholderRange = new Vector2(placeholderPosition - placeholderOffset, placeholderPosition + placeholderOffset);
            Debug.Log("prefabDimension: " + prefabDimension);
            Debug.Log("placeHolderPosition: " + placeholderPosition);
            Debug.Log("placeHolderOffset: " + placeholderOffset);
            Debug.Log("placeHolderRange: " + placeholderRange);
        }

        private void Update()
        {
            if (content.childCount != contentChildCount)
            {
                OnBeginDrag(null);
                OnEndDrag(null);
                contentChildCount = content.childCount;
            }
        }

        public void OnDrag(PointerEventData data)
        {
            //Debug.Log("OnDrag");
            //if (content.childCount == 0) return;
            //if (content.childCount == 1) currentCell = content.GetChild(0);

            //float pos;
            //Transform tf;
            //float contentPosition;

            //if (horizontalScroller)
            //    contentPosition = content.anchoredPosition.x;
            //else
            //    contentPosition = content.anchoredPosition.y;
        
            //for (int i = 0; i < content.childCount; i++)
            //{
            //    tf = content.GetChild(i);

            //    if (horizontalScroller)
            //        pos = tf.localPosition.x + contentPosition;
            //    else
            //        pos = tf.localPosition.y + contentPosition;

            //    if (data != null)
            //    {
            //        if (!pos.IsWithin(placeholderRange.x, placeholderRange.y) && tf.localScale.x > disabledScale)
            //        {
            //            tf.DOScale(disabledScale, switchDuration);
            //            tf.GetComponent<CanvasGroup>().DOFade(disabledFade, switchDuration);
            //            tf.GetComponent<CanvasGroup>().interactable = false;
            //        }
            //        else if (pos.IsWithin(placeholderRange.x, placeholderRange.y) && tf != currentCell)
            //        {
            //            tf.GetComponent<CanvasGroup>().interactable = true;
            //            tf.DOScale(1, switchDuration);
            //            tf.GetComponent<CanvasGroup>().DOFade(1, switchDuration);
            //            placeholderCellIndex = i;
            //            currentCell = tf;
            //        }
            //    }
            //    else
            //    {
            //        if (tf != currentCell)
            //        {
            //            tf.GetComponent<CanvasGroup>().interactable = false;
            //            tf.DOScale(disabledScale, switchDuration);
            //            tf.GetComponent<CanvasGroup>().DOFade(disabledFade, switchDuration);
            //        }
            //    }
            //}
        }

        public void OnEndDrag(PointerEventData data)
        {
            if (content.childCount == 0) return;

            float contentPosition = (horizontalScroller) ? content.anchoredPosition.x : content.anchoredPosition.y;
            float pos;
            Transform tf;

            if (data != null)
            {
                currentCell = null;

                for (int i = 0; i < content.childCount; i++)
                {
                    tf = content.GetChild(i);
                    pos = (horizontalScroller) ? tf.localPosition.x + contentPosition : tf.localPosition.y + contentPosition;
 
                    if (pos.IsWithin(placeholderRange.x, placeholderRange.y))
                    {
                        currentCell = tf;
                        placeholderCellIndex = i;
                        break;
                    }
                }

                if (currentCell == null)
                {
                    tf = content.GetChild(0);
                    pos = (horizontalScroller) ? tf.localPosition.x + contentPosition : tf.localPosition.y + contentPosition;
                    if (pos > placeholderRange.x)
                    {
                        currentCell = tf;
                        placeholderCellIndex = 0;
                    }
                    else
                    {
                        currentCell = content.GetChild(content.childCount - 1);
                        placeholderCellIndex = content.childCount;
                    }
                }

                currentCell.DOScale(1, switchDuration);
                // currentCell.GetComponent<CanvasGroup>().DOFade(1, switchDuration);
                currentCell.GetComponent<CanvasGroup>().interactable = true;
            }
 

            Vector2 v;
            if (data == null)
            {
                if (content.childCount > 1)
                {
                    v = (horizontalScroller) ? new Vector2(placeholderOffset, 0) : new Vector2(0, placeholderOffset);
                    content.anchoredPosition += (content.childCount > contentChildCount) ? v : -v;
                }
            }
            else
            {
                v = (horizontalScroller) ? new Vector2(-currentCell.localPosition.x - content.anchoredPosition.x, 0) : new Vector2(0, -currentCell.localPosition.y - content.anchoredPosition.y);
                content.DOAnchorPos(v, switchDuration).SetRelative();
            }
        }

        public void OnBeginDrag(PointerEventData data)
        {
            content.DOKill();

            if (content.childCount == 1)
            {
                currentCell = content.GetChild(0);
                currentCell.GetComponent<CanvasGroup>().interactable = true;
                if (data != null) currentCell.GetComponent<Button>().onClick.Invoke();
            }

            for (int i = 0; i < content.childCount; ++i)
            {
                //tf.GetComponent<CanvasGroup>().DOFade(disabledFade, switchDuration);
                Transform tf = content.GetChild(i);
                if (data != null || (data == null && tf != currentCell))
                {
                    tf.DOScale(disabledScale, switchDuration);
                    tf.GetComponent<CanvasGroup>().interactable = false;
                }
            }
        }
    }
}
