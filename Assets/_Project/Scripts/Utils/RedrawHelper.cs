﻿using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Utils
{
    public class RedrawHelper : MonoBehaviour 
    {

        [SerializeField] RectTransform m_RectTransform = default;

        private void OnEnable()
        {
            Redraw();
        }

        private void Start()
        {
            Redraw();
        }

        public void Redraw()
        {
            LayoutRebuilder.ForceRebuildLayoutImmediate(m_RectTransform);
        }
    }
}
