﻿using I2.Loc;
using Root.DesignPatterns;
using UnityEngine;

namespace _Project.Scripts.Utils
{
    public class ErrorModalDialog : GlobalSingleton<ErrorModalDialog>
    {
        [SerializeField] private Canvas modal = default;

        [SerializeField] private Localize title = default;
        [SerializeField] private Localize message = default;

        private LocalizationParamsManager m_localizationParamsManager;
        private float m_timeScale;

        protected override void Start() {
            base.Start();
            modal = GetComponent<Canvas>();
            m_localizationParamsManager = message.GetComponent<LocalizationParamsManager>();
            modal.enabled = false;
            m_timeScale = Time.timeScale;
        }
	
        public void Exec(string i2path, params string[] parameters) {
            modal.enabled = true;
            m_timeScale = Time.timeScale;
            Time.timeScale = 0;
            title.SetTerm($"{i2path}/Title");
            message.SetTerm($"{i2path}/Message");

            for (int i = 0; i < parameters.Length; ++i)
            {
                m_localizationParamsManager.SetParameterValue("param" + i, parameters[i]);
            }

        }

        public void Close()
        {
            Time.timeScale = m_timeScale;
            modal.enabled = false;
        }
    }
}
