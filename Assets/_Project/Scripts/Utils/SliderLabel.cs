﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Utils
{
    [RequireComponent(typeof(TMP_Text))]
    public class SliderLabel : MonoBehaviour {

        [SerializeField]
        private Slider Slider = default;

        [SerializeField]
        [Tooltip("The text shown will be formatted using this string. {0} is replaced with the actual value")]
        private string formatText = "{0}";

        private TMP_Text m_textDisplay;

        // Use this for initialization
        void Start () {
            m_textDisplay = GetComponent<TMP_Text>();
            HandleValueChanged(Slider.value);
            Slider.onValueChanged.AddListener(HandleValueChanged);
        }
	
        private void HandleValueChanged(float value) {
            m_textDisplay.text = string.Format(formatText, value);
        }
    }
}
