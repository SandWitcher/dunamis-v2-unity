﻿using System;
using Root.DesignPatterns;
using TMPro;
using UnityEngine;

namespace _Project.Scripts.Utils
{
    public class ErrorDialog : SceneSingleton<ErrorDialog>
    {
        [SerializeField]
        private GameObject Modal = default;

        [SerializeField]
        private TMP_Text Text = default;

        void Start() {
            Modal.SetActive(false);
        }
	
        public void Exec(string message) {
            Modal.SetActive(true);
            Text.text = message;
        }

        public void Close() {
            Modal.SetActive(false);
        }

        public void HandleException(Exception exception) {
            if(exception is System.Net.Sockets.SocketException) {
                Exec("Impossible de se connecter au serveur. Vérifiez que l'appareil est connecté à Internet ou réessayez plus tard !");
            } else {
                Exec("Une erreur inconnu est survenu.");
            }
        }

        public void UnexpectedError(string message = null) {
            if(message == null) {
                message = "Une erreur inconnu est survenu.";
            }
            Exec(message);
        }
    }
}
