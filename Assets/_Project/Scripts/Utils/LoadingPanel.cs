using DG.Tweening;
using Root.DesignPatterns;
using UnityEngine;

namespace _Project.Scripts.Utils
{
    public class LoadingPanel : GlobalSingleton<LoadingPanel>
    {
        public GameObject Spinner;
        public TMPro.TMP_Text LoadingMessage;

        [SerializeField]
        private Canvas m_canvas;

        private I2.Loc.Localize localize;

        private int m_pendingTaskCount;
        public int PendingTaskCount {
            get { return m_pendingTaskCount; }
            set {
                m_pendingTaskCount = value;

                if(m_pendingTaskCount <= 0) {
                    m_pendingTaskCount = 0;
                    Close();
                } else {
                    Display();
                }
            }
        }

        protected override void Start()
        {
            base.Start();
            m_canvas = GetComponent<Canvas>();
            localize = LoadingMessage.GetComponent<I2.Loc.Localize>();
            PendingTaskCount = 0;
            Spinner.SetActive(false);
            m_canvas.enabled = false;
        }

        public void IncreasePendingTaskCount()
        {
            PendingTaskCount++;
        }

        public void DecreasePendingTaskCount()
        {
            PendingTaskCount--;
        }

        private void Display()
        {
            if (localize != null)
            {
                DOTween.timeScale = 1;
                m_canvas.enabled = true;
                Spinner.SetActive(true);
            }
        }

        public void SetMessage(string term = "Loading")
        {
            localize.SetTerm(term);
        }

        private void Close()
        {
            Spinner.SetActive(false);
            m_canvas.enabled = false;
            SetMessage();
        }
    }
}
