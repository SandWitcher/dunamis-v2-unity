using System.Runtime.InteropServices;
using UnityEngine;

namespace _Project.Scripts.Utils
{
    public interface IBluetoothCallback
    {
        void OnDidUpdateState();
        void OnDidConnect();
        void OnDidDisconnect();
        void OnDidReceiveWriteRequests(string base64String);
    }

    public class BluetoothService
    {
#if UNITY_ANDROID
    const string JAVA_CLASS_NAME = "com.bluetooth.Service";

    static AndroidJavaObject _android = null;
#endif

#if UNITY_IOS
    [DllImport ("__Internal")]
    private static extern void _iOSBLECreateServicePeripheral ();

    [DllImport ("__Internal")]
    private static extern void _iOSBLECreateServiceCentral ();

    [DllImport ("__Internal")]
    private static extern void _iOSBLEServiceStart (string uuidString);

    [DllImport ("__Internal")]
    private static extern void _iOSBLEServicePause (bool isPause);

    [DllImport ("__Internal")]
    private static extern void _iOSBLEServiceStop ();

    [DllImport ("__Internal")]
    private static extern void _iOSBLEServiceWrite (byte[] data, int length, bool withResponse); 
#endif

        public static void Initialize() {
#if UNITY_IPHONE
        _iOSBluetoothLEInitialize (asCentral, asPeripheral);
#elif UNITY_ANDROID
        if (_android == null) {
            AndroidJavaClass javaClass = new AndroidJavaClass(JAVA_CLASS_NAME);
            _android = javaClass.CallStatic<AndroidJavaObject>("getInstance");
        }

        if (_android != null) {
            // _android.Call("initialize", asCentral, asPeripheral);
        }
#endif
        }

        public static void CreateServicePeripheral()
        {
#if UNITY_IOS
        _iOSBLECreateServicePeripheral();
#endif
        }

        public static void CreateServiceCentral()
        {
#if UNITY_IOS
        _iOSBLECreateServiceCentral();
#elif UNITY_ANDROID 
        using (AndroidJavaClass plugin = new AndroidJavaClass(JAVA_CLASS_NAME))
        {
            plugin.CallStatic("createServiceCentral");
        }
#endif
        }

        public static void StartService(string uuidString)
        {
#if UNITY_IOS
        _iOSBLEServiceStart(uuidString);
#elif UNITY_ANDROID 
        using (AndroidJavaClass plugin = new AndroidJavaClass(JAVA_CLASS_NAME))
        {
            plugin.CallStatic("start", uuidString);
        }
#endif
        }

        public static void PauseService(bool isPause)
        {
#if UNITY_IOS
        _iOSBLEServicePause(isPause);
#elif UNITY_ANDROID 
        using (AndroidJavaClass plugin = new AndroidJavaClass(JAVA_CLASS_NAME))
        {
            plugin.CallStatic("pause", isPause);
        }
#endif
        }
        public static void StopService()
        {
#if UNITY_IOS
        _iOSBLEServiceStop();
#elif UNITY_ANDROID 
        using (AndroidJavaClass plugin = new AndroidJavaClass(JAVA_CLASS_NAME))
        {
            plugin.CallStatic("stop");
        }
#endif
        }

        public static void Write(byte[] data, int length, bool withResponse)
        {
#if UNITY_IOS
        _iOSBLEServiceWrite(data, length, withResponse);
#elif UNITY_ANDROID 
        using (AndroidJavaClass plugin = new AndroidJavaClass(JAVA_CLASS_NAME))
        {
            plugin.CallStatic("write", data);
        }
#endif
        }
    }
}