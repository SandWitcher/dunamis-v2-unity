﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace _Project.Scripts.Utils
{
    public class SceneLoader : MonoBehaviour {

        [Header ("Load scene")]
        public string NameLoadScene;

        [SerializeField]
        private bool AutoStart = false;

        [SerializeField]
        private bool ShowLoadingPanel = true;

        void Start () {
            if (AutoStart) {
                Load();
            }
        }

        public void Load() {

            if (NameLoadScene == SceneManager.GetActiveScene().name)
            {
                throw new UnityException("Cannot load active scene.");
            }
            else
            {
                if (ShowLoadingPanel)
                    LoadingPanel.Instance.PendingTaskCount++;

                StartCoroutine(LoadAsync());
            }
        }

        public void UnsafeLoad()
        {
            if (ShowLoadingPanel)
                LoadingPanel.Instance.PendingTaskCount++;

            StartCoroutine(LoadAsync());
        }

        IEnumerator LoadAsync()
        {
            bool h = ShowLoadingPanel;
            AsyncOperation async = SceneManager.LoadSceneAsync(NameLoadScene);
            async.completed += Async_completed;
            while (!async.isDone)
                yield return null;
        }

        private void Async_completed(AsyncOperation obj)
        {
            if (ShowLoadingPanel)
                LoadingPanel.Instance.PendingTaskCount--;
        }
    }
}
