namespace _Project.Scripts.Utils.StateSystem {
    public interface IPausableState {

        void OnStatePause();

        void OnStateUnpause();
    }
}