namespace _Project.Scripts.Utils.StateSystem {
    public interface IState {
        bool StateActive { get; set; }

        void OnStateEnter();

        void OnStateLeave();
    }
}