using UnityEngine;
using UnityEngine.Events;

namespace _Project.Scripts.Utils.StateSystem {
    public class GameObjectState : AMonoBehaviourState {

        public UnityEvent OnEnter;

        public UnityEvent OnPause;

        public UnityEvent OnUnpause;

        public UnityEvent OnLeave;

        public override void OnStateEnter() {
            Debug.Log("Enter state " + this.name);
            base.OnStateEnter();
            gameObject.SetActive(true);
            OnEnter.Invoke();
        }

        public override void OnStatePause() {
            Debug.Log("Pause state " + this.name);
            gameObject.SetActive(false);
            OnPause.Invoke();
        }

        public override void OnStateUnpause() {
            Debug.Log("Unpause state " + this.name);
            gameObject.SetActive(true);
            OnUnpause.Invoke();
        }

        public override void OnStateLeave() {
            Debug.Log("Leave state " + this.name);
            base.OnStateLeave();
            gameObject.SetActive(false);
            OnLeave.Invoke();
        }
    }
}