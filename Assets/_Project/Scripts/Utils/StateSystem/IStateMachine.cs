namespace _Project.Scripts.Utils.StateSystem {
    public interface IStateMachine {
        void Open(IState state);

        void Close();
    }

}