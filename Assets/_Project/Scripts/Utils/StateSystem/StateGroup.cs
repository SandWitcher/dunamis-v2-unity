using System.Collections.Generic;
using _Project.Scripts.Utils.UINavigation;

namespace _Project.Scripts.Utils.StateSystem {
    public class StateGroup : AMonoBehaviourState {

        private List<AMonoBehaviourState> m_children;

        public override void OnStateEnter() {
            gameObject.SetActive(true);
            base.OnStateEnter();
        }

        public override void OnStatePause() {
            gameObject.SetActive(false);
        }

        public override void OnStateUnpause() {
            gameObject.SetActive(true);
        }

        public override void OnStateLeave() {
            gameObject.SetActive(false);
            base.OnStateLeave();
        }

        public void OpenChild(AMonoBehaviourState child) {
            Navigation.Instance.Open(child);

            if (m_children == null)
                m_children = new List<AMonoBehaviourState>();

            if (!m_children.Contains(child))
                m_children.Add(child);
        }

        public void CloseGroup()
        {
            for (int i = 0; i < m_children.Count; i++)
            {
                Navigation.Instance.Back();
            }
            m_children.Clear();
        }
    }
}