using System.Collections.Generic;

namespace _Project.Scripts.Utils.StateSystem {
    public class StackedStateMachine : IStateMachine {

        private Stack<IState> m_stack = new Stack<IState>();

        public int Count {
            get { return m_stack.Count; }
        }

        /// <summary>
        /// Append state in the stack and call <see cref="IState.OnStateEnter">IState.OnStateEnter()</see>
        ///
        /// if last state implements <see cref="IPausableState">IPausableState interface</see>,
        /// <see cref="IPausableState.OnStatePause">OnStatePause</see> is called on it.
        /// </summary>
        /// <param name="newState"></param>
        public void Open(IState newState) {
            if (m_stack.Count > 0) {
                var last = m_stack.Peek() as IPausableState;
                if(last != null) {
                    last.OnStatePause();
                }
            }
            if (newState != null) {
                m_stack.Push(newState);
                newState.OnStateEnter();
            }
        }

        /// <summary>
        /// Pop last state from stack and call <see cref="IState.OnStateLeave">OnStateLeave()</see>.
        ///
        /// If penultimate state implements <see cref="IPausableState">IPausableState interface</see>,
        /// <see cref="IPausableState.OnStateUnpause">OnStateUnpause</see> is called on it.
        /// </summary>
        public void Close() {
            if (m_stack.Count >= 1) {
                var last = m_stack.Pop();
                last.OnStateLeave();
            }

            if (m_stack.Count >= 1) {
                var newTop = m_stack.Peek() as IPausableState;
                if(newTop != null) {
                    newTop.OnStateUnpause();
                }
            }
        }

        public void RollBack(IState state) {
            if (m_stack.Count >= 1 && state != m_stack.Peek()) {
                var last = m_stack.Pop();
                last.OnStateLeave();
            }

            while(m_stack.Peek() != state) {
                m_stack.Pop();
            }

            if (m_stack.Count >= 1) {
                var newTop = m_stack.Peek() as IPausableState;
                if(newTop != null) {
                    newTop.OnStateUnpause();
                }
            }
        }

        public void Clear() {
            m_stack.Clear();
        }


        public enum StateState {
            NOT_RUNNING,
            PAUSED,
            RUNNING
        }

        public StateState GetStatusForState(IPausableState state) {
            int i = 0;
            foreach (var item in m_stack) {
                if (item == state) {
                    return (i == m_stack.Count - 1) ? StateState.RUNNING : StateState.PAUSED;
                }
                i++;
            }
            return StateState.NOT_RUNNING;
        }
    }

}