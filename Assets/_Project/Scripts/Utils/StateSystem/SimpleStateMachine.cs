namespace _Project.Scripts.Utils.StateSystem {
    public class SimpleStateMachine : IStateMachine {
        protected IState currentState;

        public void Open(IState state) {
            if (currentState != null) {
                currentState.OnStateLeave();
            }
            currentState = state;
            if (currentState != null) {
                currentState.OnStateEnter();
            }
        }

        public void Close() {
            if (currentState != null) {
                currentState.OnStateLeave();
                currentState = null;
            }
        }
    }
}