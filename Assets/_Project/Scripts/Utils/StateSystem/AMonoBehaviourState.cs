using _Project.Scripts.Utils.UINavigation;
using UnityEngine;

namespace _Project.Scripts.Utils.StateSystem {
    public abstract class AMonoBehaviourState : MonoBehaviour, IState, IPausableState {

        public StateGroup Group;

        public bool StateActive { get; set; }

        public virtual void OnStateEnter() {
            StateActive = true;
        }

        public virtual void OnStatePause() {
        }

        public virtual void OnStateUnpause() {
        }

        public virtual void OnStateLeave() {
            StateActive = false;
        }

        // Update is called once per frame
        public virtual void Update() {
            if (StateActive) {
                OnStateUpdate();
            }
        }

        protected virtual void OnStateUpdate() { }

        public virtual void Open() {
            if (Group != null) {
                Group.OpenChild(this);
            } else {
                Navigation.Instance.Open(this);
            }
        }

        public virtual void RollBack() {
            Navigation.Instance.RollBack(this);
        }

    }
}