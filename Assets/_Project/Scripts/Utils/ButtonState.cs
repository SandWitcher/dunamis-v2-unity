using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Utils
{
    public class ButtonState : MonoBehaviour {

        public Sprite NormalSprite;

        public SpriteState SpriteState;
    }
}
