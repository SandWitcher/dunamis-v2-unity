﻿using I2.Loc;
using UnityEngine;

namespace _Project.Scripts.Utils
{
    public class ModalDialog : MonoBehaviour 
    {
        [SerializeField] private Canvas modal = default;

        [SerializeField] private Localize title = default;
        [SerializeField] private Localize message = default;

        [SerializeField] private string i2Path = default;
        [SerializeField] private string[] parameters = default;

        private LocalizationParamsManager m_localizationParamsManager;

        private Color m_cachedAmbientLight;

        private void Awake()
        {
            m_cachedAmbientLight = RenderSettings.ambientLight;

            RenderSettings.ambientLight = Color.white;
        
            if (modal == null) modal = GetComponent<Canvas>();

            modal.worldCamera = Camera.main;
            modal.enabled = false;

            m_localizationParamsManager = message.GetComponent<LocalizationParamsManager>();

            Exec();
        }

        public void Exec()
        {
            title.SetTerm($"{i2Path}/Title");
            message.SetTerm($"{i2Path}/Message");

            if (parameters == null) return;

            for (int i = 0; i < parameters.Length; ++i)
            {
                m_localizationParamsManager.SetParameterValue("param" + i, parameters[i]);
            }

            modal.enabled = true;
        }

        public void Close()
        {
            RenderSettings.ambientLight = m_cachedAmbientLight;
            modal.enabled = false;
            Destroy(gameObject);
        }
    }
}
