﻿using _Project.Scripts.Utils.StateSystem;
using Root.DesignPatterns;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Utils.UINavigation
{
    public class Navigation : SceneSingleton<Navigation> {

        public Button BackButton;

        private StackedStateMachine m_stateMachine = new StackedStateMachine();

        public AMonoBehaviourState StateToLaunchAtStart;

        void Start () {

            Screen.sleepTimeout = 15;

            if(BackButton != null) {
                BackButton.onClick.AddListener(Back);
                BackButton.gameObject.SetActive(false);
            }

            if(StateToLaunchAtStart != null)
            {
                Open(StateToLaunchAtStart);
            }
        }

        public void Open(IState state) {
            m_stateMachine.Open(state);
            BackButton.gameObject.SetActive(true);
        }

        /// <summary>
        /// For Unity Serialization
        /// </summary>
        public void Open(AMonoBehaviourState state) {
            m_stateMachine.Open(state);

            if (m_stateMachine.Count > 1 && BackButton != null) BackButton.gameObject.SetActive(true);

        }

        public void Back() {

            if(m_stateMachine.Count > 1) {
                m_stateMachine.Close();
            }

            if(m_stateMachine.Count <= 1) {
                if (BackButton != null) BackButton.gameObject.SetActive(false);
            }
        }

        public void RollBack(AMonoBehaviourState state) {
            m_stateMachine.RollBack(state);

            if(m_stateMachine.Count <= 1) {
                BackButton.gameObject.SetActive(false);
            }
        }

        public void ClearHistory() {
            m_stateMachine.Clear();
            BackButton.gameObject.SetActive(false);
        }
    }
}
