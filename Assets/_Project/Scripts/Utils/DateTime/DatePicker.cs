﻿using System.Globalization;
using System.Text;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

namespace _Project.Scripts.Utils.DateTime
{
    /// <summary>
    /// a Date Picker similar to Unity
    /// </summary>
    public class DatePicker : MonoBehaviour {

        public bool ShowOnStart = false;

        public Button ItemBase;

        public UIVerticalScroller DayScroller;
	
        public UIVerticalScroller MonthScroller;

        public UIVerticalScroller YearScroller;

        public UDate MinDate = new System.DateTime(1900, 1, 1);

        public UDate MaxDate = new System.DateTime(2100, 12, 31);

        [Header("Transitions")]
        public float Duration = 0.25f;

        public AnimationCurve Curve;

        [Header("Events")]
        public UnityEvent OnClose;

        public System.DateTime CurrentDate {
            get { 
                StringBuilder strbld = new StringBuilder(DayScroller.result, 12);
                strbld.Append(" ");
                strbld.Append(MonthScroller.result);
                strbld.Append(" ");
                strbld.Append(YearScroller.result);
                return System.DateTime.ParseExact(strbld.ToString(), "d MMMM yyyy", CultureInfo.CurrentCulture); 
            }
        }

        void Awake () {
            if(!ShowOnStart) {
                transform.Translate(0, -((RectTransform)transform).rect.height, 0);
                gameObject.SetActive(false);
            }
            Init();
        }

        void Start() {

        }

        void Init() {
#if !UNITY_ANDROID || UNITY_EDITOR
            int currentYear = System.DateTime.Now.Year;
            int currentMonth = System.DateTime.Now.Month;
            int currentDay = System.DateTime.Now.Day;
            int days = System.DateTime.DaysInMonth(currentYear, currentMonth);

            DayScroller._arrayOfElements = new GameObject[days];
            for(int i = 0; i < days; i++) {
                Button item = Instantiate(ItemBase, new Vector3(0, i * 80, 0), Quaternion.Euler(new Vector3(0, 0, 0)), DayScroller.scrollingPanel.transform);
                item.GetComponentInChildren<Text>().text = i.ToString();
                item.gameObject.AddComponent<CanvasGroup>();
                DayScroller._arrayOfElements[i] = item.gameObject;
            }
            DayScroller.startingIndex = currentDay;

            MonthScroller._arrayOfElements = new GameObject[12];
            for(int i = 0; i < 12; i++) {
                Button item = Instantiate(ItemBase, new Vector3(0, i * 80, 0), Quaternion.Euler(new Vector3(0, 0, 0)), MonthScroller.scrollingPanel.transform);
                item.GetComponentInChildren<Text>().text = new System.DateTime(currentYear, i + 1, 1).ToString("MMMM", CultureInfo.CurrentCulture);
                item.gameObject.AddComponent<CanvasGroup>();
                MonthScroller._arrayOfElements[i] = item.gameObject;
            }
            MonthScroller.startingIndex = currentMonth;

            int yearCount = MaxDate.dateTime.Year - MinDate.dateTime.Year + 1;
            YearScroller._arrayOfElements = new GameObject[yearCount];
            for(int i = 0; i < yearCount; i++) {
                Button item = Instantiate(ItemBase, new Vector3(0, i * 80, 0), Quaternion.Euler(new Vector3(0, 0, 0)), YearScroller.scrollingPanel.transform);
                item.GetComponentInChildren<Text>().text = (i + MinDate.dateTime.Year).ToString();
                item.gameObject.AddComponent<CanvasGroup>();
                YearScroller._arrayOfElements[i] = item.gameObject;
            }
            YearScroller.startingIndex = currentYear - MinDate.dateTime.Year - 1;
#endif
        }

        public void Show () {
            gameObject.SetActive(true);
            transform.DOMoveY(0, Duration).SetEase(Curve);
        }
	
        public void Hide () {
            OnClose.Invoke();
            transform.DOMoveY(-((RectTransform)transform).rect.height, Duration).SetEase(Curve).OnComplete(() => gameObject.SetActive(false));
        }
    }
}
