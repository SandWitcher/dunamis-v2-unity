using System;
using UnityEngine;

namespace _Project.Scripts.Utils.DateTime
{
    /// <summary>
    /// UDateTime is a Unity Serializable DateTime wrapper.
    /// 
    /// When you are using UDateTime, a custom editor allow you to edit datetimes in Unity Inspector.
    /// 
    /// You can cast UDateTime objects from and to System.DateTime
    /// <example>
    /// This sample shows how to cast System.DateTime to UDateTime.
    /// <code>
    /// UDateTime myDatetime = new DateTime(2100, 12, 31, 12, 0, 0);
    /// </code>
    /// </example>
    /// <example>
    /// This sample shows how to cast UDateTime to System.DateTime.
    /// <code>
    /// System.DateTime sysDatetime = (System.DateTime)myDatetime;
    /// </code>
    /// </example>
    /// </summary>
    [Serializable]
    public class UDateTime : ISerializationCallbackReceiver 
    {
        private System.DateTime m_dateTime = new System.DateTime(1970, 1, 1);
        public System.DateTime Value {
            get {
                if(UseNow) {
                    return System.DateTime.UtcNow;
                }
                return m_dateTime; 
            }
        }

        /// <summary>
        /// While it set to True, <see cref="Value"/> returns the current UTC DateTime.
        /// </summary>
        public bool UseNow = false;

        [HideInInspector]
        [SerializeField]
        private int m_year;

        [HideInInspector] 
        [SerializeField]
        private int m_month;

        [HideInInspector] 
        [SerializeField]
        private int m_day;

        [HideInInspector] 
        [SerializeField]
        private int m_hour;

        [HideInInspector] 
        [SerializeField]
        private int m_minute;

        [HideInInspector] 
        [SerializeField]
        private int m_second;

        [HideInInspector] 
        [SerializeField]
        private int m_millisecond;

        /// <summary>
        /// Cast UDateTime to System.DateTime
        /// </summary>
        public static implicit operator System.DateTime(UDateTime udt) {
            return udt.Value;
        }

        /// <summary>
        /// Cast System.DateTime to UDateTime
        /// </summary>
        public static implicit operator UDateTime(System.DateTime dt) {
            return new UDateTime() { m_dateTime = dt };
        }

        /// <inheritdoc />
        public void OnAfterDeserialize() {
            m_dateTime = new System.DateTime(m_year, m_month, m_day, m_hour, m_minute, m_second, m_millisecond);
        }

        /// <inheritdoc />
        public void OnBeforeSerialize() {
            m_year = m_dateTime.Year;
            m_month = m_dateTime.Month;
            m_day = m_dateTime.Day;

            m_hour = m_dateTime.Hour;
            m_minute = m_dateTime.Minute;
            m_second = m_dateTime.Second;
            m_millisecond = m_dateTime.Millisecond;
        }
    }
}
