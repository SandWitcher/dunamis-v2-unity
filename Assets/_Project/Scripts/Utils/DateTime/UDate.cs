using System;
using UnityEngine;

namespace _Project.Scripts.Utils.DateTime
{
    /// <summary>
    /// UDate is a Unity Serializable DateTime wrapper.
    ///
    /// When you are using UDate, a custom editor allow you to edit dates in Unity Inspector.
    /// 
    /// You can cast UDate objects from and to System.DateTime
    /// <example>
    /// This sample shows how to cast System.DateTime to UDate.
    /// <code>
    /// UDate myDate = new DateTime(2100, 12, 31);
    /// </code>
    /// </example>
    /// <example>
    /// This sample shows how to cast UDate to System.DateTime.
    /// <code>
    /// System.DateTime sysDate = (System.DateTime)myDate;
    /// </code>
    /// </example>
    /// <warning>When you cast, UDate truncates datetime to day almost. So, time part is lost.</warning>
    /// </summary>
    [Serializable]
    public class UDate : ISerializationCallbackReceiver {
        private System.DateTime m_dateTime = new System.DateTime(1970, 1, 1);
        public System.DateTime dateTime {
            get {
                if(UseNow) {
                    return System.DateTime.Today;
                }
                return m_dateTime; 
            }
        }

        /// <summary>
        /// While it set to True, <see cref="Value"/> returns the current date(today).
        /// </summary>
        [Tooltip("If True, Use the current date.")]
        public bool UseNow = false;

        [HideInInspector] 
        [SerializeField] 
        private int m_year;

        [HideInInspector] 
        [SerializeField]
        private int m_month;

        [HideInInspector] 
        [SerializeField]
        private int m_day;

        /// <summary>
        /// Cast UDate to System.DateTime
        /// </summary>
        public static implicit operator System.DateTime(UDate udt) {
            return udt.dateTime.Date;
        }

        /// <summary>
        /// Cast System.DateTime to UDate
        /// </summary>
        public static implicit operator UDate(System.DateTime dt) {
            return new UDate() { m_dateTime = dt.Date };
        }

        /// <inheritdoc />
        public void OnAfterDeserialize() {
            m_dateTime = new System.DateTime(m_year, m_month, m_day);
        }

        /// <inheritdoc />
        public void OnBeforeSerialize() {
            m_year = m_dateTime.Year;
            m_month = m_dateTime.Month;
            m_day = m_dateTime.Day;
        }
    }
}