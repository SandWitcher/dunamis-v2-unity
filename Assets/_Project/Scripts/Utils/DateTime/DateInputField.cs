﻿using System;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;
using UnityEngine.UI;

namespace _Project.Scripts.Utils.DateTime
{
    /// <summary>
    /// Input Field for Date.
    ///
    /// On Android platforms, it use <see href="https://developer.android.com/reference/android/app/DatePickerDialog">DatePickerDialog</see>.
    ///
    /// For others platforms, you must place a <see cref="DatePicker">DatePicker</see> in your scene and link it via the
    /// <see cref="Picker">Picker</see> parameter.
    /// </summary>
    [AddComponentMenu("UI/Date Input Field")]
    [RequireComponent(typeof(Button))]
    public class DateInputField : MonoBehaviour {

        #region Unity Parameters

        [Tooltip("Not required for Android plateforms.")]
        public DatePicker Picker;

        public TMPro.TMP_Text TextComponent;

        public string Format = "dd / MM / yyyy";

        public bool ReadOnly;

        [Tooltip("The minimal supported date.")]
        public UDate MinDate = new System.DateTime(1900, 1, 1);

        [Tooltip("The maximal supported date.")]
        public UDate MaxDate = new System.DateTime(2100, 12, 31);

        public UDate CurrentDate;

        [System.Serializable]
        public class OnChangeEvent : UnityEvent<System.DateTime> {
            public OnChangeEvent() {}
        }
        public OnChangeEvent OnValueChanged;

        #endregion

        #region Internal params

        // Used for calculating timestamp.
        private static System.DateTime epochStart = new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);

        #endregion

        void Start () {
            GetComponent<Button>().onClick.AddListener(Edit);
#if !UNITY_ANDROID || UNITY_EDITOR
            if(Picker == null) {
                Picker = FindObjectOfType<DatePicker>();
            }
            if(Picker != null) {
                Picker.OnClose.AddListener(() => {
                    CurrentDate = Picker.CurrentDate;
                    DisplayDate(CurrentDate);
                });
            }
#endif
        }

        /// <summary>
        /// Called when the script is loaded or a value is changed in the
        /// inspector (Called in the editor only).
        /// </summary>
        void OnValidate() {
            GetComponent<Button>().interactable = !ReadOnly;
            if(TextComponent != null) {
                DisplayDate(CurrentDate);
            }
        }

#if UNITY_ANDROID && !UNITY_EDITOR
	private bool m_dirtyValue = false;

	void Update() {
		// Look for any change from android listener.
		if(CurrentDate != null && m_dirtyValue) {
			DisplayDate(CurrentDate);
			m_dirtyValue = false;
		}
	}

	/// <summary>
	/// Listener for handle Android DatePickerDialog OnDateSet event
	/// </summary>
    class DateCallback : AndroidJavaProxy {
		DateInputField m_source;

        public DateCallback(DateInputField source) : base("android.app.DatePickerDialog$OnDateSetListener") {
			m_source = source;
		}

        void onDateSet(AndroidJavaObject view, int year, int monthOfYear, int dayOfMonth) {
            m_source.CurrentDate = new System.DateTime(year, monthOfYear + 1, dayOfMonth);
			m_source.m_dirtyValue = true;
        }
    }
#endif

        /// <summary>
        /// Start editing the field.
        ///
        /// On Android, it show Date picker dialog.null
        ///
        /// On others platforms, show <see cref="Picker">date picker</see>.
        /// </summary>
        public void Edit () {
            if(ReadOnly) {
                return;
            }
#if UNITY_ANDROID && !UNITY_EDITOR
		AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

		// Must run on Android UI thread
		activity.Call("runOnUiThread", new AndroidJavaRunnable(() => {
			System.DateTime currentDate = (System.DateTime) CurrentDate;
			AndroidJavaObject datePickerDialog = new AndroidJavaObject("android.app.DatePickerDialog",
			                                                           /* context */ activity,
			                                                           /* listener */ new DateCallback(this),
																	   /* year */ currentDate.Year,
																	   /* month */ currentDate.Month - 1,
																	   /* day */ currentDate.Day);

			// Configure min date and max date
			AndroidJavaObject datePicker = datePickerDialog.Call<AndroidJavaObject>("getDatePicker");
			datePicker.Call("setMinDate", Convert.ToInt64((MinDate.dateTime - epochStart).TotalMilliseconds));
			datePicker.Call("setMaxDate", Convert.ToInt64((MaxDate.dateTime - epochStart).TotalMilliseconds));

			// show picker dialog
			datePickerDialog.Call("show");
		}));
#else
            Picker.Show();
#endif
        }

        /// <summary>
        /// Format and display <i>date</i>.
        /// </summary>
        /// <param name="date">Date to display.</param>
        void DisplayDate(System.DateTime date) {
            Assert.IsNotNull(TextComponent);
            TextComponent.text = date.ToString(Format);
        }
    }
}
