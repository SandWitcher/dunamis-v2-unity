using TMPro;
using UnityEngine;

namespace _Project.Scripts.Utils.Forms
{
    public class TextFormField : AFormField {

        public TMP_InputField Input;

        protected Material NormalFontMaterial;

        public Material IncorrectFontMaterial;

        public string AutofillPath;

        protected override void Start()
        {
            base.Start();
            NormalFontMaterial = Input.textComponent.fontMaterial;

            if (AutofillPath != "" && ES2.Exists(AutofillPath))
            {
                Input.text = ES2.Load<string>(AutofillPath);
            }
        }

        public override void ResetField () {
            Input.text = "";
        }

        public override void ResetAppearance ()
        {
            if (NormalFontMaterial == null) Start();

            base.ResetAppearance();
            Input.textComponent.fontMaterial = NormalFontMaterial;
        }

        public override void MarkAsIncorrect () {
            base.MarkAsIncorrect();
            Input.textComponent.fontMaterial = IncorrectFontMaterial;
        }

        protected override string GetValue() {
            return Input.text;
        }

        public void OpenIfEmpty(TMP_InputField inputField)
        {
            if (inputField.text.Length <= 0)
            {
                inputField.Select();
            }
        }
    }
}
