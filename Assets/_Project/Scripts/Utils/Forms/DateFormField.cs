using _Project.Scripts.Utils.DateTime;

namespace _Project.Scripts.Utils.Forms
{
    public class DateFormField : AFormField {

        public DateInputField Input;

        public string Format = "dd / MM / yyyy";

        public override void ResetField () {
        }

        protected override string GetValue() {
            return ((System.DateTime)Input.CurrentDate).ToString(Format);
        }
    }
}
