using System;
using System.Collections.Generic;
using _Project.Scripts.Data.Scriptable.REST;
using BestHTTP;
using UnityEngine;

namespace _Project.Scripts.Utils.Forms
{
    [Serializable]
    public class FormOptions {
        [Tooltip("Show loading spinner during form processing.")]
        public bool BlockingMode;
    }

    public class Form : MonoBehaviour {

        public Endpoint Endpoint;

        public HTTPMethods Method = HTTPMethods.Post;

        public List<AFormField> Fields;

        public FormOptions Options;

        private Dictionary<string, AFormField> m_fieldCache = new Dictionary<string, AFormField>();

        public void Send() {
            /*
        if(Options.BlockingMode) {
            LoadingPanel.Instance.PendingTaskCount++;
        }

        Uri uri = DataHandler.Instance.BackEndEnvironment.build(this.Endpoint.Id);
        HTTPRequest req = new HTTPRequest(uri, Method, HandleResponse);

        Dictionary<string, string> validationRules = Rules() ;

        bool validationSuccess = true;
        for(int i = 0; i < Fields.Count; i++) {
            AFormField field = Fields[i];

            if(validationRules != null && validationRules.ContainsKey(field.Name)) {
                string ruleString = validationRules[field.Name];

                string[] rules = ruleString.Split('|');
                for(int j = 0; j < rules.Length; j++) {
                    string[] tokens = rules[j].Split(new[] { ':' }, 2);

                    string[] ruleParams = null;
                    if(tokens.Length > 1) {
                        ruleParams = tokens[1].Split(',');
                    }

                    ValidationRule r = ValidationRulePool.Instance.GetRule(tokens[0], this);
                    if(!r.Validate(field.Name, field.Value, ruleParams)) {
                        validationSuccess = false;
                        field.Errors = new List<string> { String.Format(r.Message(), field.Name) };
                    }
                }
            }
            if(validationSuccess) {
                req.AddField(field.Name, field.Value);
            }
        }

        if(validationSuccess) {
            req = ProcessRequest(req);
            req = HTTPRequestManager.Instance.ProcessRequest(req);

            req.Send();
        } else if(Options.BlockingMode) {
            LoadingPanel.Instance.PendingTaskCount--;
        }
        */
        }

        private void HandleResponse(HTTPRequest request, HTTPResponse response) {
            /*
		if(request.Exception != null) {
			ErrorDialog.Instance.UnexpectedError(request.Exception.Message);
		} else {
            ProcessResponse(request, response);
        }
        if(Options.BlockingMode) {
            LoadingPanel.Instance.PendingTaskCount--;
        }*/
        }

        protected virtual HTTPRequest ProcessRequest(HTTPRequest request) {
            return request;
        }

        protected virtual void ProcessResponse(HTTPRequest request, HTTPResponse response) {
        }

        public void setErrorsForField(string fieldname, List<string> errors) {
            // Caching ?
            /*
        for(int i = 0; i < Fields.Count; i++) {
            AFormField field = Fields[i];
            if(field.Name == fieldname) {
                field.Errors = errors;
                break;
            }
        }*/
        }

        /// <summary>
        /// Get the validation rules that apply to the request.
        /// </summary>
        /// <returns>Null for skip validation step.</returns>
        public virtual Dictionary<string, string> Rules() {
            return null;
        }
    }
}