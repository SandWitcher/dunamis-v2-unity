using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Utils.Forms
{
    public abstract class AFormField : MonoBehaviour {

        [SerializeField]
        protected Image Cartridge;

        protected Sprite Full;

        [SerializeField]
        [Tooltip("Appearance when field is incorrect.")]
        protected Sprite Empty;

        virtual protected void Start()
        {
            Full = Cartridge.sprite;
        }

        public string Value {
            get { return GetValue(); }
        }

        public virtual void ResetAppearance () {
            Cartridge.sprite = Full;
        }

        public virtual void ResetField () {
            ResetAppearance();
        }

        public virtual void MarkAsIncorrect () {
            Cartridge.sprite = Empty;
        }

        /// <returns>The current field value</returns>
        protected abstract string GetValue();
    }
}
