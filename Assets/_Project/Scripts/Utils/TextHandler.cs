﻿using TMPro;
using UnityEngine;

namespace _Project.Scripts.Utils
{
    public class TextHandler : MonoBehaviour
    {
        #region Inspector
        public TMP_Text Text;
        public Material MaterialTarget;
        #endregion

        private Material m_materialPreset;

        private void OnValidate()
        {
            if (Text == null)
                Text = GetComponent<TMP_Text>();
            m_materialPreset = Text.fontSharedMaterial;
        }

        private void Awake()
        {
            if (Text == null)
                Text = GetComponent<TMP_Text>();
            m_materialPreset = Text.fontSharedMaterial;
        }

        public void SwitchMaterial(bool state)
        {
            Debug.Log($"{gameObject.name}, text: {Text.text}, material target: {MaterialTarget}, state: {state}");
            Text.fontSharedMaterial = state ? MaterialTarget : m_materialPreset;
        }
    }
}