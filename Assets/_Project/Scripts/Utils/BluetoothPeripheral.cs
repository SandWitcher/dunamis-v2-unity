using UnityEngine;

namespace _Project.Scripts.Utils
{
    class BluetoothPeripheral {

        public string Name {
            get {
                // TODO:
                return "";
            }
            set {
#if UNITY_IOS
			_iOSBluetoothLEPeripheralName(newName);
#elif UNITY_ANDROID
            Debug.LogWarning("Peripheral name is a read-only property on Android devices.");
#endif
            }
        }
    }
}