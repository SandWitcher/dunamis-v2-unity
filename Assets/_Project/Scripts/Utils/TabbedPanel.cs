﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Utils
{
    public class TabbedPanel : MonoBehaviour {

        [SerializeField]
        private Transform TabContainer = default;

        //[SerializeField]
        //private Transform PanelContainer = default;

        [SerializeField]
        private Button TabTemplate = default;

        [SerializeField]
        private List<TabWidget> Panels = default;

        public int CurrentTabIndex = 0;

        // Use this for initialization
        void Start () {
            foreach (Transform child in TabContainer) {
                GameObject.Destroy(child.gameObject);
            }
            for(int i = 0; i < Panels.Count; i++) {
                Button newTab = Instantiate(TabTemplate, TabContainer);
                newTab.GetComponentInChildren<Text>().text = Panels[i].Name;
                newTab.onClick.AddListener(delegate {setActiveTab(i); });
            }
            setActiveTab(CurrentTabIndex);
        }
	
        public void setActiveTab(int idx) {
            int old = CurrentTabIndex;
            if(idx >= Panels.Count) {
                CurrentTabIndex = Panels.Count - 1;
            }else if(idx < 0) {
                CurrentTabIndex = 0;
            } else {
                CurrentTabIndex = idx;
            }

            if(old != CurrentTabIndex) {
                Panels[old].gameObject.SetActive(false);
                Panels[CurrentTabIndex].gameObject.SetActive(true);
            }
        }
    }
}
