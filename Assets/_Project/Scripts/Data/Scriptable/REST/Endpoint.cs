using UnityEngine;

namespace _Project.Scripts.Data.Scriptable.REST
{
    [CreateAssetMenu(fileName = "NewEndpoint", menuName = "REST/Endpoint", order = 1)]
    public class Endpoint : ScriptableObject {
        public string Id;
        public string Path;
    }
}