using System;
using UnityEngine;

namespace _Project.Scripts.Data.Scriptable.REST
{
    [CreateAssetMenu(fileName = "NewEnvironment", menuName = "REST/Environment", order = 1)]
    public class Environment : ScriptableObject {
        public string Base;
        public Endpoint[] Endpoints;

        // private Dictionary<string, string> m_cache;

        public Uri build(string id) {
            for(int i = 0; i < Endpoints.Length; i++) {
                Endpoint e = Endpoints[i];
                if(e.Id == id) {
                    return new Uri(new Uri(Base), e.Path);
                }
            }

            throw new ApplicationException("Endpoint \"" + id +"\" does not exists.");
        }

        public Uri BuildAbsolutePath(string path) {
            return new Uri(new Uri(Base), path);
        }
    }
}