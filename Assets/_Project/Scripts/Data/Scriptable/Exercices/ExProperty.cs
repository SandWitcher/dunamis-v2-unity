using System.Collections.Generic;
using UnityEngine;

namespace _Project.Scripts.Data.Scriptable.Exercices
{
    [CreateAssetMenu(fileName = "NewEndpoint", menuName = "Exercices/Property", order = 1)]
    public class ExProperty : ScriptableObject {
        public string Name;
        public List<string> Path;
    }
}