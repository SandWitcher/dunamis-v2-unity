using System;
using System.Text;
using UnityEngine;

namespace _Project.Scripts.Data.Scriptable.Atlas
{
    [Serializable]
    public class AxisCorrector {
        [Range(-180, 180)]
        public float Offset = 0;

        public float Multiplier = 1;
    }

    [Serializable]
    public class AngleLimits
    {
        /// <summary>
        /// X: Roll left. Y: Roll right.
        /// </summary>
        [Tooltip("X: Roll left. Y: Roll right.")]
        public Vector2 Roll;

        /// <summary>
        /// X: Pitch up. Y: Pitch forward.
        /// </summary>
        [Tooltip("X: Pitch up. Y: Pitch forward.")]
        public Vector2 Pitch;

        /// <summary>
        /// X: Yaw left. Y: Yaw right.
        /// </summary>
        [Tooltip("X: Yaw left. Y: Yaw right.")]
        public Vector2 Yaw;
    }

    [Serializable]
    public class IMUCorrector {
        public AxisCorrector Roll;

        public AxisCorrector Yaw;

        public AxisCorrector Pitch;
    }

    [CreateAssetMenu(fileName = "NewAtlasCorrector", menuName = "ATLAS/Corrector", order = 1)]
    public class Corrector : ScriptableObject {
        public IMUCorrector IMU;

        public AngleLimits Limits;

        public Vector4 ForceLimits;
    }
}