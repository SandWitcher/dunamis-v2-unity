using System;
using System.Collections.Generic;

namespace _Project.Scripts.Data.Api.Accounts {
    [Serializable]
    public class UserLoginErrors : FormErrors
    {
        public List<string> username;

        public List<string> password;
    }
}
