using System;
using System.Collections.Generic;

namespace _Project.Scripts.Data.Api.Accounts {
    [Serializable]
    public class UserRegistrationErrors: FormErrors
    {
        public List<string> username;

        public List<string> email;

        public List<string> password;
    }
}
