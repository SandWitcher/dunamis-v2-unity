using System;
using UnityEngine;

namespace _Project.Scripts.Data.Api.Accounts {
    [Serializable]
    public struct AuthToken
    {
        [SerializeField]
        private string token;
        public string Key {
            get { return token; }
            private set {
                token = value;
            }
        }
    }
}
