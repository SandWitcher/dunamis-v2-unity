using System;
using System.Collections.Generic;
using _Project.Scripts.Network;
using _Project.Scripts.Utils;
using _Project.Scripts.Utils.HTTPNetworking;
using BestHTTP;
using RSG;
using UnityEngine;

namespace _Project.Scripts.Data.Api.Accounts {
    [Serializable]
    public class User
    {
        public int id;

        public string first_name;

        public string last_name;

        public string email;

        public string username;

        public string helper;

        public int secret_question_index;

        public bool subscribed;

        [NonSerialized]
        public List<Player> Players;

        public IPromise<List<Player>> FetchPlayers () {
            var promise = new Promise<List<Player>>();

            Uri uri = DataHandler.Instance.BackEndEnvironment.BuildAbsolutePath("/api/accounts/players/");
            HTTPRequest req = new HTTPRequest(uri, HTTPMethods.Get, (request, response) => {
                if(request.Exception != null) {
                    promise.Reject(request.Exception);
                } else if(!response.IsSuccess) {
                    promise.Reject(HTTPException.CreateResponseException(response));
                } else {
                    this.Players = JsonUtility.FromJson<Api.WrappedResourceList<Player>>(response.DataAsText).data;
                    promise.Resolve(this.Players);
                }
            });
            req = HTTPRequestManager.Instance.ProcessRequest(req);
            //req = Session.Instance.AuthenticateUserRequest(req);
            req.Send();

            return promise;
        }
    }
}
