using System;
using System.Collections.Generic;

namespace _Project.Scripts.Data.Api.Accounts {
    [Serializable]
    public class PlayerLoginErrors : FormErrors
    {
        public List<string> name;

        public List<string> password;
    }
}
