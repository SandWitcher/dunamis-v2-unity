using System;

namespace _Project.Scripts.Data.Api.Accounts {
    [Serializable]
    public class Player
    {
        public int id;

        public string name;

        //[SerializeField]
        public string birth_date;
        public DateTime BirthDate {
            get { return DateTime.Parse(birth_date); }
        }

        public int waist_size;

        public int hip_size;

        public int BeltSize {
            get {
                // Assume that sizes are correct.
                if((waist_size >= 85 && waist_size <= 102) || (hip_size >= 90 && hip_size <= 106)) {
                    return 2;
                } else {
                    return 1;
                }
            }
        }
    }
}
