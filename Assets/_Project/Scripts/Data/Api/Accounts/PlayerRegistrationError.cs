using System;
using System.Collections.Generic;

namespace _Project.Scripts.Data.Api.Accounts {
    [Serializable]
    public class PlayerRegistrationErrors : FormErrors
    {
        public List<string> name;

        public List<string> password;
    }
}
