using System;
using System.Collections.Generic;

namespace _Project.Scripts.Data.Api {
	[Serializable]
	public class PaginatedResource<T>
	{
		public List<T> results;

		public string next;

		public string previous;

		public int count;
	}
}