using System;

namespace _Project.Scripts.Data.Api {
    [Serializable]
    public class Error
    {
        /// <summary>
        /// Human readable error message
        /// </summary>
        public string message;

        /// <summary>
        /// Machine readable error code
        /// </summary>
        public string code;
    }
}
