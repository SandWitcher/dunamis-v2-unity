using System;
using System.Collections.Generic;
using UnityEngine;

namespace _Project.Scripts.Data.Api {
    [Serializable]
    public class FormErrors
    {
        [SerializeField]
        private List<string> non_field_errors = default;
        public List<string> NonFieldErrors {
            get { return non_field_errors; }
        }
    }
}
