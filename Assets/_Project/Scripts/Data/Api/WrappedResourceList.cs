using System;
using System.Collections.Generic;

namespace _Project.Scripts.Data.Api {
    [Serializable]
    public class WrappedResourceList<T>
    {
        public List<T> data;
    }
}
