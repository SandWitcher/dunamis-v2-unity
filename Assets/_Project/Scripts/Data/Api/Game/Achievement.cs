using System;

namespace _Project.Scripts.Data.Api.Game {
    [Serializable]
    public class Achievement
    {
        public uint id;

        public string name;

        public string description;

        public string api_name;

        public string icon;

        public bool is_unlocked;
    }
}