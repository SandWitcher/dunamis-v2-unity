using System;

namespace _Project.Scripts.Data
{
    [Serializable]
    public class GameExercice {

        enum EProp {
            Force, 
            Souplesse,
            Posture
        }

        EProp Property;

        public float Duration;
    }
}