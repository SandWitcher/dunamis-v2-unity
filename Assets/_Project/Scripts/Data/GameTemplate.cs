using System;
using System.Collections.Generic;

namespace _Project.Scripts.Data
{
    [Serializable]
    public class GameTemplate {
        public string Name;
        public List<GameExercice> Exercices;
    }
}