﻿using _Project.Scripts.Data.Scriptable.Atlas;
using Blatand.Core.Android;
using Michsky.UI.ModernUIPack;
using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;


namespace _Project.Scripts.Atlas
{
    public class DfuUpdater : MonoBehaviour
    {
        #region Inspector

        /// <summary>
        /// Reference to the FirmwareVersion SO placed in "Assets/_Project/Data/Atlas/FirmwareVersion.asset".
        /// </summary>
        [Tooltip("Reference to the FirmwareVersion SO placed in \"Assets / _Project / Data / Atlas / FirmwareVersion.asset\".")]
        public FirmwareVersion firmwareVersion;

        #endregion


        #region Private fields

        private ProgressBar _dfuProgressBar;
        private Animator _dfuModalAnimator;
        private BeltScanner _beltScanner;
        private DfuHandler _dfuHandler;

        #endregion


        #region Unity methods

        private void Awake()
        {
            _dfuModalAnimator = GetComponent<Animator>();
            _dfuProgressBar = GetComponentInChildren<ProgressBar>();
            _beltScanner = FindObjectOfType<BeltScanner>();

            _beltScanner.OnDeviceConnected += OnDeviceConnected;
        }

        private void OnDestroy()
        {
            if (_beltScanner != null) _beltScanner.OnDeviceConnected -= OnDeviceConnected;
        }

        #endregion


        #region BeltScanner events
        
        // ATLAS-####: Launches the services discovery and sets the OnServicesDiscovered handler.
        // DfuTarg: Forwards the device to OnDfuConnected(Device device) method.
        private void OnDeviceConnected(Device device)
        {
            if (BeltScanner.MatchFilter(device.Name, BeltScanner.Filter.Atlas))
            {
                BluetoothManager.Instance.ConnectedBeltGatt.OnServicesDiscovered += OnAtlasServicesDiscovered;
                BluetoothManager.Instance.ConnectedBeltGatt.DiscoverServices();
            }
            else
            {
                OnDfuConnected(device);
            }
        }

        #endregion


        #region Atlas 

        // Compares the firmware's version in device and application. 
        // If it matches, no update is needed. Our BeltController can be set.
        // Else, the application disconnects from the device and opens a dfu-update popup.
        private void OnAtlasServicesDiscovered()
        {
            BluetoothManager.Instance.ConnectedBeltGatt.OnServicesDiscovered -= OnAtlasServicesDiscovered;

            BluetoothManager.Instance.FirmwareRevisionCharacteristic.OnValueRead += characteristic =>
            {
                BluetoothManager.Instance.FirmwareRevisionCharacteristic.ClearRead();

                string deviceFirmwareVersion = characteristic.Value.ToString();

                if (!deviceFirmwareVersion.Equals(firmwareVersion.Version) || Debug.isDebugBuild)
                {
                    Debug.LogWarning($"DfuUpdater ~ DFU update needed, app version: {firmwareVersion.Version} | device firmware: {deviceFirmwareVersion}");
                    BluetoothManager.Instance.Close();
                    _dfuModalAnimator.Play("Fade-in");
                }
                else
                {
                    Debug.Log($"DfuUpdater ~ DFU up to date, version: {firmwareVersion.Version}");
                    BeltControllerV2.Instance.ResetConnection();
                }
            };

            BluetoothManager.Instance.FirmwareRevisionCharacteristic.Read();
        }

        #endregion


        #region Dfu
        
        // Called when our connected device is in Dfu mode. Starts the Dfu Update and handles events from blatand.DfuHandler.
        private void OnDfuConnected(Device device)
        {
            if (_dfuHandler == null) _dfuHandler = new DfuHandler();

            _dfuHandler.OnProgressChanged += OnProgressChanged;
            _dfuHandler.OnError += OnError;
            _dfuHandler.OnDfuCompleted += OnDfuCompleted;

            StartCoroutine(StartDfUHandler(device));
        }

        private void OnProgressChanged(DfuHandler.Progress progress)
        {
            _dfuProgressBar.specifiedValue = progress.percent;
            _dfuProgressBar.speed = Mathf.Clamp(Mathf.RoundToInt(progress.speed), 1, 5);
        }

        private void OnError(DfuHandler.Error error)
        {
            Debug.LogWarning($"DfuUpdater ~ Error {error.message}");
        }

        private void OnDfuCompleted()
        {
            _dfuHandler.OnProgressChanged -= OnProgressChanged;
            _dfuHandler.OnError -= OnError;
            _dfuHandler.OnDfuCompleted -= OnDfuCompleted;
            _dfuHandler.Stop();
            _dfuHandler = null;

            _beltScanner.filter = BeltScanner.Filter.Atlas;

            // OnDfuCompleted() being called outside of Unity's main thread, it generates an exception if we try to do Unity stuff.
            UnityMainThreadDispatcher.Instance().Enqueue(() =>
            {
                _dfuModalAnimator.Play("Fade-out");
                DeleteCachedZip();
            });
        }

        #endregion


        #region Dfu modal buttons

        /// <summary>
        /// Starts scanning devices named "DfuTarg" using BeltScanner.
        /// </summary>
        public void StartDfuScan()
        {
            _beltScanner.filter = BeltScanner.Filter.DfuTarg;
            _beltScanner.StartScan();
        }

        #endregion


        #region Helpers

        // Downloads our zip file from the Android StreamingAssets folder,
        // Creates a temporary .zip file in cache folder
        // Starts the dfu update by providing a path to our cached .zip file.
        private IEnumerator StartDfUHandler(Device device)
        {
            UnityWebRequest webRequest = UnityWebRequest.Get(Path.Combine(Application.streamingAssetsPath, firmwareVersion.fileName));

            yield return webRequest.SendWebRequest();

            string cachedPath = Path.Combine(Application.temporaryCachePath, firmwareVersion.fileName);

            File.WriteAllBytes(cachedPath, webRequest.downloadHandler.data);

            _dfuHandler.Start(device, cachedPath);
        }

        // Deletes our cached .zip file when the update is completed.
        private void DeleteCachedZip()
        {
            string cachedPath = Path.Combine(Application.temporaryCachePath, firmwareVersion.fileName);
            if (File.Exists(cachedPath))
            {
                File.Delete(cachedPath);
                Debug.Log($"DfuUpdate ~ Delete cache file at path: {cachedPath}");
            }
            else
            {
                Debug.LogWarning($"DfuUpdater ~ No file found at path: {cachedPath}");
            }
        }

        #endregion
    }
}
