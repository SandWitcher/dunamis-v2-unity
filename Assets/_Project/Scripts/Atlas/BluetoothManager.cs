using Blatand.Core;
using Blatand.Core.Android;
using Root.DesignPatterns;
using UnityEngine;

namespace _Project.Scripts.Atlas
{
    public class BluetoothManager : GlobalSingleton<BluetoothManager>
    {
        #region Device Information

        [Header("Device Information")]

        public CharacteristicDefinition FirmwareRevisionCharacteristicDefinition;
        private GattCharacteristic m_firmwareRevisionCharacteristic;
        public GattCharacteristic FirmwareRevisionCharacteristic
        {
            get
            {
                if (m_beltGatt == null) return null;
                return m_firmwareRevisionCharacteristic = m_beltGatt.FindCharacteristic(FirmwareRevisionCharacteristicDefinition);
            }
        }

        #endregion


        #region Device Status

        [Header("Device Status")]

        public CharacteristicDefinition ErrorCodeCharacteristicDefinition;
        private GattCharacteristic m_errorCodeCharacteristic;
        public GattCharacteristic ErrorCodeCharacteristic
        {
            get
            {
                if (m_beltGatt == null) return null;
                return m_errorCodeCharacteristic = m_beltGatt.FindCharacteristic(ErrorCodeCharacteristicDefinition);
            }
        }

        public CharacteristicDefinition BatteryLevelCharacteristicDefinition;
        private GattCharacteristic m_batteryLevelCharacteristic;
        public GattCharacteristic BatteryLevelCharacteristic
        {
            get
            {
                if (m_beltGatt == null) return null;
                return m_batteryLevelCharacteristic = m_beltGatt.FindCharacteristic(BatteryLevelCharacteristicDefinition);
            }
        }

        #endregion


        #region Live Service

        [Header("Live Service")]

        public CharacteristicDefinition ModeCharacteristicDefinition;
        private GattCharacteristic m_modeCharacteristic;
        public GattCharacteristic ModeCharacteristic
        {
            get
            {
                if(m_beltGatt == null) return null;
                return m_modeCharacteristic = m_beltGatt.FindCharacteristic(ModeCharacteristicDefinition);
            }
        }

        public CharacteristicDefinition ResistanceLevelCharacteristicDefinition;
        private GattCharacteristic m_resistanceLevelCharacteristic;
        public GattCharacteristic ResistanceLevelCharacteristic
        {
            get
            {
                if(m_beltGatt == null) return null;
                return m_resistanceLevelCharacteristic = m_beltGatt.FindCharacteristic(ResistanceLevelCharacteristicDefinition);
            }
        }

        public CharacteristicDefinition IMUCharacteristicDefinition;
        private GattCharacteristic m_IMUCharacteristic;
        public GattCharacteristic IMUCharacteristic
        {
            get
            {
                if(m_beltGatt == null) return null;
                return m_IMUCharacteristic = m_beltGatt.FindCharacteristic(IMUCharacteristicDefinition);
            }
        }

        public CharacteristicDefinition SEA1CharacteristicDefinition;
        private GattCharacteristic m_SEA1Characteristic;
        public GattCharacteristic SEA1Characteristic
        {
            get
            {
                if (m_beltGatt == null) return null;
                return m_SEA1Characteristic = m_beltGatt.FindCharacteristic(SEA1CharacteristicDefinition);
            }
        }

        public CharacteristicDefinition SEA2CharacteristicDefinition;
        private GattCharacteristic m_SEA2Characteristic;
        public GattCharacteristic SEA2Characteristic
        {
            get
            {
                if (m_beltGatt == null) return null;
                return m_SEA2Characteristic = m_beltGatt.FindCharacteristic(SEA2CharacteristicDefinition);
            }
        }

        public CharacteristicDefinition SEA3CharacteristicDefinition;
        private GattCharacteristic m_SEA3Characteristic;
        public GattCharacteristic SEA3Characteristic
        {
            get
            {
                if (m_beltGatt == null) return null;
                return m_SEA3Characteristic = m_beltGatt.FindCharacteristic(SEA3CharacteristicDefinition);
            }
        }

        public CharacteristicDefinition SEA4CharacteristicDefinition;
        private GattCharacteristic m_SEA4Characteristic;
        public GattCharacteristic SEA4Characteristic
        {
            get
            {
                if (m_beltGatt == null) return null;
                return m_SEA4Characteristic = m_beltGatt.FindCharacteristic(SEA4CharacteristicDefinition);
            }
        }

        public CharacteristicDefinition DecompressionSetPointDefinition;
        private GattCharacteristic m_decompressionSetPointCharacteristic;
        public GattCharacteristic DecompressionSetPointCharacteristic
        {
            get
            {
                if (m_beltGatt == null) return null;
                return m_decompressionSetPointCharacteristic = m_beltGatt.FindCharacteristic(DecompressionSetPointDefinition);
            }
        }

        public CharacteristicDefinition GlobalSeaCharacteristicDefinition;
        private GattCharacteristic m_globalSeaCharacteristic;
        public GattCharacteristic GlobalSeaCharacteristic
        {
            get
            {
                if (m_beltGatt == null) return null;
                return m_globalSeaCharacteristic = m_beltGatt.FindCharacteristic(GlobalSeaCharacteristicDefinition);
            }
        }

        #endregion


        private Gatt m_beltGatt;
        public Gatt ConnectedBeltGatt {
            get { return  m_beltGatt; }
            set
            {
                m_beltGatt = value;
            }
        }

        public void Disconnect()
        {
            if (m_beltGatt == null) return;
            Debug.Log("Disconnect " + m_beltGatt.ToString());
            m_beltGatt.Disconnect();
        }

        public void Close()
        {
            if (m_beltGatt == null) return;
            Debug.Log("Close " + m_beltGatt.ToString());
            m_beltGatt.Close();
            m_beltGatt = null;
        }

        private void ClearCharacteristics()
        {
            // device information
            m_firmwareRevisionCharacteristic = null;

            // device status
            m_errorCodeCharacteristic = null;
            m_batteryLevelCharacteristic = null;

            // live service
            m_modeCharacteristic = null;

            m_decompressionSetPointCharacteristic = null;
            m_resistanceLevelCharacteristic = null;

            m_IMUCharacteristic = null;

            m_SEA1Characteristic = null;
            m_SEA2Characteristic = null;
            m_SEA3Characteristic = null;
            m_SEA4Characteristic = null;

            m_globalSeaCharacteristic = null;
        }

#if UNITY_ANDROID
	protected override void OnDestroy () {
        Close();
        base.OnDestroy();
	}
#endif
    }
}
