﻿using Blatand.Core.Android;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace _Project.Scripts.Atlas
{
    public class BeltScanner : MonoBehaviour
    {

        #region Inspector

        /// <summary>
        /// Sets the filter to use to scan for specific devices.
        /// </summary>
        [Tooltip("Sets the filter to use to scan for specific devices.")]
        public Filter filter;

        /// <summary>
        /// Starts/stops scan when the gameObject attached to this component is enabled/disabled.
        /// </summary>
        [Tooltip("Starts/stops scan when the gameObject attached to this component is enabled/disabled.")]
        public bool bindScanToEnable;

        #endregion


        #region Constants

        private const int Occurence = 5; // number of times a device has to be discovered to trigger a connection
        private const int TimeOut = 120; // time out delay to stop scanning for devices 

        #endregion


        #region Private fields

        private Scanner _scanner; // blatand.Scanner
        private Dictionary<string, int> _devices; // dictionary containing the devices names and the number of times they have been discovered.
        private Coroutine _timeOutCoroutine; // coroutine used to stop scanning after a time out delay
        private Coroutine _autoConnectCoroutine; // coroutine used to connect several times to the device as it does not always connect on first try

        private bool _isScanning; // is the blatand.Scanner currently running ?
        private bool _wasScanning; // was the blatand.Scanner running when OnApplicationPause was called ?

        #endregion


        #region Events

        /// <summary>
        /// Triggered when a new device is connected.
        /// </summary>
        public event Action<Device> OnDeviceConnected;

        /// <summary>
        /// Triggered when the scan stops due to time running out and no connection has been established.
        /// </summary>
        public event Action OnTimeOutReached;

        #endregion


        #region Enums

        /// <summary>
        /// Filter device by name.
        /// </summary>
        public enum Filter
        {
            /// <summary>
            /// Starts with "ATLAS-".
            /// </summary>
            Atlas, 
            /// <summary>
            /// Equals "DfuTarg".
            /// </summary>
            DfuTarg
        }

        #endregion


        #region Unity methods

        private void OnEnable()
        {
            if (bindScanToEnable) StartScan();
        }

        private void OnDisable()
        {
            if (bindScanToEnable) StopScan();
        }

        private void OnApplicationPause(bool pauseStatus)
        {
            if (pauseStatus && _isScanning)
            {
                _wasScanning = true;
                StopScan();
            }

            if (!pauseStatus && _wasScanning)
            {
                StartScan();
                _wasScanning = false;
            }
        }

        #endregion


        #region Scanner events

        // Adds the discovered device to dictionary if it matches with the filter criteria.
        // Establishes connection if it has already been found several times.
        private void OnDeviceDiscovered(Device device)
        {
            if (!MatchFilter(device.Name, filter)) return;
            AddToDict(device.Name);
            if (_devices[device.Name] >= Occurence) AutoConnect(device);
        }

        // Removes the discovered device from the dictionary.
        private void OnDeviceLost(Device device)
        {
            if (!MatchFilter(device.Name, filter)) return;
            RemoveFromDict(device.Name);

        }

        private void OnBatchDevicesDelivered(List<Device> devices)
        {
            foreach (Device device in devices)
            {
                OnDeviceDiscovered(device);
            }
        }

        #endregion


        #region Private methods

        // Matches a device name with the filter.
        public static bool MatchFilter(string deviceName, Filter filter)
        {
            if (deviceName == null) return false;

            switch (filter)
            {
                case Filter.Atlas:
                    return deviceName.StartsWith("ATLAS-");
                case Filter.DfuTarg:
                    return deviceName.Equals("DfuTarg");
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        // Adds a new entry to the dictionary or increase the device name's discovery count by 1.
        private void AddToDict(string deviceName)
        {
            if (_devices.ContainsKey(deviceName))
                _devices[deviceName]++;
            else
                _devices.Add(deviceName, 1);
        }

        private void RemoveFromDict(string deviceName)
        {
            if (!_devices.ContainsKey(deviceName))
                Debug.LogWarning($"BeltScanner: _devices does not contain key {deviceName}");
            else
                _devices.Remove(deviceName);
        }

        // Automatically connects to a device and stops scanner.
        private void AutoConnect(Device device)
        {
            device.OnConnected += OnConnected;
            StopScan();
            _autoConnectCoroutine = StartCoroutine(AutoConnectCoroutine(device));
        }

        private IEnumerator AutoConnectCoroutine(Device device)
        {
            while (BluetoothManager.Instance.ConnectedBeltGatt == null)
            {
                device.Connect();
                yield return new WaitForSecondsRealtime(5);
            }
        }

        // Raises event on device connected
        private void OnConnected(Device device, Gatt gatt)
        {
            device.OnConnected -= OnConnected;
            BluetoothManager.Instance.ConnectedBeltGatt = gatt;
            OnDeviceConnected?.Invoke(device);
            StopCoroutine(_autoConnectCoroutine);
            _autoConnectCoroutine = null;
        }

        private IEnumerator TimeOutCoroutine()
        {
            yield return new WaitForSecondsRealtime(TimeOut);
            Debug.Log($"BeltScanner ~ Scan time out reached for filter: {filter}");
            OnTimeOutReached?.Invoke();
            StopScan();
        }

        #endregion


        #region Public methods

        /// <summary>
        /// Creates a new Scanner instance, binds its events to BeltScanner events handler, clears devices dictionary and then starts scanning for devices.
        /// </summary>
        public void StartScan()
        {
            if (_scanner != null) Debug.LogWarning("BeltScanner ~ Scanner is already instantiated");

            _timeOutCoroutine = StartCoroutine(TimeOutCoroutine());
            
            _scanner = Scanner.NewScanner();
            _devices = new Dictionary<string, int>();

            _scanner.OnDeviceDiscovered += OnDeviceDiscovered;
            _scanner.OnDeviceLost += OnDeviceLost;
            _scanner.OnBatchDevicesDelivered += OnBatchDevicesDelivered;

            _scanner.Start();

            _isScanning = true;
        }

        /// <summary>
        /// Unbinds the scanner events, stops it and sets scanner and dictionary to null.
        /// </summary>
        public void StopScan()
        {
            if (_scanner == null) Debug.LogWarning("BeltScanner ~ Scanner is null");

            StopCoroutine(_timeOutCoroutine);

            _scanner.Stop();
            
            _scanner.OnDeviceDiscovered -= OnDeviceDiscovered;
            _scanner.OnDeviceLost -= OnDeviceLost;
            _scanner.OnBatchDevicesDelivered -= OnBatchDevicesDelivered;

            _scanner = null;
            _devices = null;

            _isScanning = false;

        }

        #endregion
    }
}
