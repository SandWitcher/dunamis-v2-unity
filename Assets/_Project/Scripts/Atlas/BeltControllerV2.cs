﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.Data.Scriptable.Atlas;
using _Project.Scripts.Utils;
using Blatand.Core.Android;
using Crosstales;
using Root.DesignPatterns;
using Sirenix.Utilities;
using UnityEngine;

namespace _Project.Scripts.Atlas
{
    public class BeltControllerV2 : SceneSingleton<BeltControllerV2>
    {
        public Corrector corrector;

        #region Name 

        private string _beltName;
        public string BeltName
        {
            get { return _beltName; }
            set
            {
                if (_beltName.Equals(value)) return;
                _beltName = value;
                Debug.Log($"{nameof(BeltControllerV2)} : {nameof(BeltName)} = {_beltName}");
                OnBeltNameUpdate?.Invoke(value);
            }
        }

        #endregion

        #region Firmware revision

        private string _firmwareRevisionNumber;
        private string _firmwareRevisionString;
        public string FirmwareRevision
        {
            get
            {
                return _firmwareRevisionString;
            }
            set
            {
                _firmwareRevisionString = value;
                string convert = new string(_firmwareRevisionString.SkipWhile(c => !char.IsDigit(c))
                    .TakeWhile(c => char.IsDigit(c) || c == '.' || c == ',')
                    .ToArray());

                if (!convert.IsNullOrWhitespace()) _firmwareRevisionNumber = convert;
            }
        }

        public void UpdateFirmwareRevision(GattCharacteristic characteristic)
        {
            FirmwareRevision = characteristic.Value.ToString();
        }

        #endregion

        #region Belt Values Properties

        // base orientation 
        private Vector3 _orientation;
        public Vector3 Orientation => _orientation;

        // user offset orientation 
        private Vector3 _offsetOrientation;
        public Vector3 OffsetOrientation => _offsetOrientation;

        // user base min orientation
        private Vector2 _minOrientation = new Vector2(-InitialRollAngle, InitialPitchUpAngle);
        public Vector2 MinOrientation
        {
            get { return _minOrientation; }
            set
            {
                _minOrientation.Set(
                    Mathf.Clamp(value.x, -MaxRollAngle, -InitialRollAngle),
                    Mathf.Clamp(value.y, MaxPitchUpAngle, InitialPitchUpAngle));
            }
        }

        // user base max orientation
        private Vector2 _maxOrientation = new Vector2(InitialRollAngle, InitialPitchForwardAngle);
        public Vector2 MaxOrientation
        {
            get { return _maxOrientation; }
            set
            {
                _maxOrientation.Set(
                    Mathf.Clamp(value.x, InitialRollAngle, MaxRollAngle), 
                    Mathf.Clamp(value.y, InitialPitchForwardAngle, MaxPitchForwardAngle));
            }
        }

        // user orientation translated as input 
        private Vector2 _input = default;
        public Vector2 Input => _input;

        // forces applied by the user on the belt
        private Vector4 _force;
        public Vector4 Force => _force;

        // actuators positions
        private Vector4 _position;
        public Vector4 Position => _position;

        #endregion


        #region Belt Controller Properties

        private bool _connected;
        public bool Connected
        {
            get { return _connected; }
            private set
            {
                if (_connected == value) return;
                _connected = value;
                Debug.Log($"{nameof(BeltControllerV2)} : {nameof(Connected)} = {_connected}");
                OnConnectionStateUpdate?.Invoke(_connected);
            }
        }

        private bool _ticking;
        public bool Ticking
        {
            get { return _ticking; }
            private set
            {
                if (_ticking == value) return;
                _ticking = value;
                Debug.Log($"{nameof(BeltControllerV2)} : {nameof(Ticking)} = {_ticking}");
                OnTickingUpdate?.Invoke(_ticking);
            }
        }

        #endregion


        #region Fields

        private Vector3 _offset = new Vector3(); // current offset 

        // tick
        private float _tickTimer     = 0.0f;
        private const float TickDelay = 2.0f;

        // battery level
        private float _batteryLevelTimer     = 0.0f;
        private const float BatteryLevelDelay = 60;

        // connection
        private float _connectionTimer       = 0.0f;
        private const float ConnectionTimeOut = 5.0f;

        // sync
        private bool _modeSynced          = true;
        private bool _resistivitySynced   = true;
        private bool _decompressionSynced = true;
        private bool _synced              = false;


        // notification
        private bool _notification = false;

        #endregion


        #region Actions

        // orientation & forces/positions
        public event Action<Vector3> OnOrientationUpdate;
        public event Action<Vector4> OnForceUpdate;
        public event Action<Vector4> OnPositionUpdate;
        public event Action<Vector3> OnSetOffset;

        // resistivity
        public event Action<bool> OnResistivitySynced;
        public event Action<int[]> OnResistivityWrite;
        public event Action<int[]> OnResistivityUpdate;

        // decompression
        public event Action<bool> OnDecompressionSynced;
        public event Action<int[]> OnDecompressionWrite;
        public event Action<int[]> OnDecompressionUpdate;

        // mode
        public event Action<bool> OnModeSynced;
        public event Action<EMode> OnModeWrite;
        public event Action<EMode> OnModeUpdate;

        // battery level
        public event Action<int> OnBatteryLevelUpdate;
        public event Action<int> OnBatteryLowUpdate;

        // OnFullSyncUpdate called when cached resistivity, decompression, mode match their remote values
        public event Action<bool> OnFullSyncUpdate;

        // miscellaneous
        public event Action<bool> OnConnectionStateUpdate;
        public event Action<string> OnBeltNameUpdate;
        public event Action<bool> OnTickingUpdate;
        public event Action TickUpdate;

        #endregion


        #region Constants

        // initial orientation
        public const float InitialRollAngle = 10;
        public const float InitialPitchForwardAngle = 20;
        public const float InitialPitchUpAngle = -5;
        public const float InitialYawAngle = 20;

        // max orientation
        public const float MaxRollAngle = 40;
        public const float MaxPitchForwardAngle = 80;
        public const float MaxPitchUpAngle = -35;
        public const float MaxYawAngle = 45;

        public const float InitialForce = 2000;

        // resistivity
        public const int MaxResistivity = 20;
        public const int MinResistivity = 1;

        // decompression
        public const int MaxDecompression = 40;
        public const int MinDecompression = 1;

        // axis
        public enum Axis
        {
            Roll = 0,
            Pitch = 1,
            Yaw = 2
        }

        public enum Movement
        {
            RollLeft,
            RollRight,
            PitchUp,
            PitchForward
        }

        #endregion


        /***********************
         *** CHARACTERISTICS ***
         ***********************/

        #region Resistivity

        private int[] _resistivity = {1, 1, 1, 1};
        public int[] Resistivity
        {
            get { return (int[]) _resistivity.Clone(); }
            set
            {
                Debug.Log($"resistivity cache: {_resistivity[0]},{_resistivity[1]},{_resistivity[2]},{_resistivity[3]}\n" +
                          $"resistivity input: {value[0]},{value[1]},{value[2]},{value[3]}");
                
#if !UNITY_EDITOR && UNITY_ANDROID
            GattCharacteristic characteristic = BluetoothManager.Instance.ResistanceLevelCharacteristic;
            int uint8 = GattCharacteristic.GattCharacteristicValue.FORMAT_UINT8;
#endif

                for (int i = 0; i < value.Length; i++)
                {
                    value[i] = Mathf.Clamp(value[i], MinResistivity, MaxResistivity);

                    if (value[i] == _resistivity[i]) continue;

                    _resistivitySynced = false;
                    _resistivity[i] = value[i];
#if !UNITY_EDITOR && UNITY_ANDROID
                if (!characteristic.Value.SetInt(value[i], uint8, i))
                {
                    throw new ArgumentOutOfRangeException(nameof(characteristic), characteristic, $"input value: {value[i]}");
                };
#endif
                }

                if (_resistivitySynced) return;


                OnResistivityWrite?.Invoke(_resistivity);

#if !UNITY_EDITOR && UNITY_ANDROID
            characteristic.Write();
#else
                StartCoroutine(DummyUpdateResistivity(value));
#endif
            }
        }

        private void UpdateResistivity(GattCharacteristic characteristic)
        {
            int uint8 = GattCharacteristic.GattCharacteristicValue.FORMAT_UINT8;
            int[] resistivity =
            {
                // ReSharper disable once RedundantArgumentDefaultValue
                characteristic.Value.ToInt(uint8, 0),
                characteristic.Value.ToInt(uint8, 1),
                characteristic.Value.ToInt(uint8, 2),
                characteristic.Value.ToInt(uint8, 3),
            };

            OnResistivityUpdate?.Invoke(resistivity);

            if (resistivity.Where((seaResistivity, i) => seaResistivity != _resistivity[i]).Any())
            {
                OnResistivitySynced?.Invoke(_resistivitySynced = false);
                return;
            }

            OnResistivitySynced?.Invoke(_resistivitySynced = true);
        }

        #endregion


        #region Decompression

        private int[] _decompression = {0, 0, 0, 0};

        public int[] Decompression
        {
            get { return (int[]) _decompression.Clone(); }
            set
            {
                Debug.Log(
                    $"decompression cache: {_decompression[0]},{_decompression[1]},{_decompression[2]},{_decompression[3]}\n" +
                    $"decompression input: {value[0]},{value[1]},{value[2]},{value[3]}");

#if !UNITY_EDITOR && UNITY_ANDROID
            GattCharacteristic characteristic = BluetoothManager.Instance.DecompressionSetPointCharacteristic;
            int uint8 = GattCharacteristic.GattCharacteristicValue.FORMAT_UINT8;
#endif

                for (int i = 0; i < value.Length; i++)
                {
                    value[i] = Mathf.Clamp(value[i], MinDecompression, MaxDecompression);

                    if (value[i] == _decompression[i]) continue;

                    _decompressionSynced = false;
                    _decompression[i] = value[i];
#if !UNITY_EDITOR && UNITY_ANDROID
                if (!characteristic.Value.SetInt(value[i], uint8, i))
                {
                    throw new ArgumentOutOfRangeException(nameof(characteristic), characteristic, $"input value: {value[i]}, offset: {i}, format type: {uint8}");
                };
#endif
                }


                if (_decompressionSynced) return;

                OnDecompressionWrite?.Invoke(value);

#if !UNITY_EDITOR && UNITY_ANDROID
            characteristic.Write();
#else
                StartCoroutine(DummyUpdateDecompression(value, 0.5f));
#endif

            }
        }

        private void UpdateDecompression(GattCharacteristic characteristic)
        {
            int uint8 = GattCharacteristic.GattCharacteristicValue.FORMAT_UINT8;

            int[] decompression =
            {
                // ReSharper disable once RedundantArgumentDefaultValue
                characteristic.Value.ToInt(uint8, 0),
                characteristic.Value.ToInt(uint8, 1),
                characteristic.Value.ToInt(uint8, 2),
                characteristic.Value.ToInt(uint8, 3),
            };

            OnDecompressionUpdate?.Invoke(decompression);

            if (decompression.Where((t, i) => t != Decompression[i]).Any())
            {
                OnDecompressionSynced?.Invoke(_decompressionSynced = false);
                return;
            }

            OnDecompressionSynced?.Invoke(_decompressionSynced = true);
        }

        #endregion


        #region Mode

        public enum EMode
        {
            Stop = 0x00,
            Following = 0x01,
            Disturbance = 0x02,
            Resistive = 0x03
        }

        private EMode _mode = EMode.Stop;

        public EMode Mode
        {
            get { return _mode; }
            set
            {
                if (_mode == value) return;

                Debug.Log($"mode input: {value}\nmode cached: {_mode}");

                int[] tempDecompression = _decompression;
                int[] tempResistivity = _resistivity;

                bool wasStopped = _mode == EMode.Stop;

                if (wasStopped)
                {
                    _decompression = new int[4];
                    _resistivity = new [] {1, 1, 1, 1};
                }

                _mode = value;
                _modeSynced = false;

                OnModeWrite?.Invoke(value);

#if !UNITY_EDITOR && UNITY_ANDROID
            GattCharacteristic characteristic = BluetoothManager.Instance.ModeCharacteristic;
            int uint8 = GattCharacteristic.GattCharacteristicValue.FORMAT_UINT8;

            characteristic.Value.SetInt((int)value, uint8);
            characteristic.Write();
#else
                StartCoroutine(DummyUpdateMode(value));
#endif

                if (wasStopped)
                {
                    Decompression = tempDecompression;
                    Resistivity = tempResistivity;
                }
            }
        }

        private void UpdateMode(GattCharacteristic characteristic)
        {
            int uint8 = GattCharacteristic.GattCharacteristicValue.FORMAT_UINT8;
            EMode mode = (EMode) characteristic.Value.ToInt(uint8);

            OnModeUpdate?.Invoke(mode);
            OnModeSynced?.Invoke(_modeSynced = (mode == _mode));
        }

        #endregion


        #region IMU 

        private void UpdateOrientation(GattCharacteristic characteristic)
        {
            int sInt16 = GattCharacteristic.GattCharacteristicValue.FORMAT_SINT16;

            float roll = RectifyAxis(corrector.IMU.Roll, characteristic.Value.ToInt(sInt16));
            float pitch = RectifyAxis(corrector.IMU.Pitch, characteristic.Value.ToInt(sInt16, 2));

            if (Mathf.Abs(roll - _orientation.x) > 25 || Mathf.Abs(pitch - _orientation.y) > 35)
            {
                Debug.LogWarning($"{nameof(BeltControllerV2)}, Orientation drift: {_orientation}, Received: {new Vector2(roll, pitch)}");
                return;
            }

            _orientation.Set(
                roll,
                pitch,
                RectifyAxis(corrector.IMU.Yaw, characteristic.Value.ToInt(sInt16, 4)));
        
            UpdateOffsetOrientation();
            UpdateInput();

            OnOrientationUpdate?.Invoke(Orientation);
        }

        private void UpdateInput()
        {
            float horizontal = Mathf.Clamp(Orientation[0].Map(MinOrientation[0] - _offset[0], MaxOrientation[0] - _offset[0], -1f, 1f), -1, 1);

            float vertical = OffsetOrientation[1] < 0
                ? Mathf.Clamp(Orientation[1].Map(MinOrientation[1] - _offset[1], 0, -1f, 0), -1, 0)
                : Mathf.Clamp(Orientation[1].Map(0, MaxOrientation[1] - _offset[1], 0, 1f), 0, 1);

            _input.Set(horizontal, vertical);
        }

        private void UpdateOffsetOrientation()
        {
            _offsetOrientation = _offset + Orientation;
        }

        #endregion


        #region Battery

        private List<int> _batteryThresholds;

#if UNITY_EDITOR
        [Range(0, 100)]
        public int dummyBatteryLevel = 100;
        private int _dummyBatteryLevel;
#endif

        public int BatteryLevel { get; private set; }
    
        private void UpdateBatteryLevel(GattCharacteristic characteristic)
        {
            int uint8 = GattCharacteristic.GattCharacteristicValue.FORMAT_UINT8;
            int batteryLevel = characteristic.Value.ToInt(uint8);

            if (BatteryLevel != batteryLevel)
            {
                Debug.Log($"battery remote: {batteryLevel}\nbattery cache: {BatteryLevel}");
                BatteryLevel = batteryLevel;
                OnBatteryLevelUpdate?.Invoke(batteryLevel);
            }

            if (!_batteryThresholds.Contains(batteryLevel)) return;
            _batteryThresholds.Remove(item: batteryLevel);
            ErrorModalDialog.Instance.Exec("Battery Low", batteryLevel.ToString());
            OnBatteryLowUpdate?.Invoke(batteryLevel);
        }

        #endregion


        #region Force and position

        // ReSharper disable once UnusedMember.Local
        private void UpdateForcesAndPositions(GattCharacteristic characteristic)
        {
            if (!_notification) return;

            int uint16 = GattCharacteristic.GattCharacteristicValue.FORMAT_UINT16;

            //_position[0] = BluetoothManager.Instance.SEA1Characteristic.Value.ToInt(uint16);
            //_force[0]    = BluetoothManager.Instance.SEA1Characteristic.Value.ToInt(uint16, 2);

            //_position[1] = BluetoothManager.Instance.SEA2Characteristic.Value.ToInt(uint16);
            //_force[1] = BluetoothManager.Instance.SEA2Characteristic.Value.ToInt(uint16, 2);

            //_position[2] = BluetoothManager.Instance.SEA3Characteristic.Value.ToInt(uint16);
            //_force[2] = BluetoothManager.Instance.SEA3Characteristic.Value.ToInt(uint16, 2);

            //_position[3] = BluetoothManager.Instance.SEA4Characteristic.Value.ToInt(uint16);
            //_force[3] = BluetoothManager.Instance.SEA4Characteristic.Value.ToInt(uint16, 2);

            if (_firmwareRevisionNumber.Contains("2.0.1"))
            {
                for (int i = 0; i < 4; i++)
                {
                    _position[i] = characteristic.Value.ToInt(uint16, offset: 4*i);
                    _force[i] = characteristic.Value.ToInt(uint16, offset: (4*i)+2);
                }

                OnForceUpdate?.Invoke(Force);
                OnPositionUpdate?.Invoke(Position);
            }
        }

        #endregion

        /***************************
     *** END CHARACTERISTICS ***
     ***************************/


        #region Private methods

        private void EnableNotification()
        {
#if UNITY_EDITOR || !UNITY_ANDROID
            return;
#endif

#pragma warning disable 162
            // ReSharper disable once HeuristicUnreachableCode
            BluetoothManager.Instance.ModeCharacteristic.EnableNotification();
            BluetoothManager.Instance.ModeCharacteristic.OnValueFetched += UpdateMode;

            BluetoothManager.Instance.IMUCharacteristic.EnableNotification();
            BluetoothManager.Instance.IMUCharacteristic.OnValueFetched += UpdateOrientation;

            BluetoothManager.Instance.DecompressionSetPointCharacteristic.EnableNotification();
            BluetoothManager.Instance.DecompressionSetPointCharacteristic.OnValueFetched += UpdateDecompression;

            BluetoothManager.Instance.ResistanceLevelCharacteristic.EnableNotification();
            BluetoothManager.Instance.ResistanceLevelCharacteristic.OnValueFetched += UpdateResistivity;

            //BluetoothManager.Instance.BatteryLevelCharacteristic.EnableNotification();
            BluetoothManager.Instance.BatteryLevelCharacteristic.OnValueFetched += UpdateBatteryLevel;

            //BluetoothManager.Instance.SEA1Characteristic.EnableNotification();
            //BluetoothManager.Instance.SEA2Characteristic.EnableNotification();
            //BluetoothManager.Instance.SEA3Characteristic.EnableNotification();
            //BluetoothManager.Instance.SEA4Characteristic.EnableNotification();

            //BluetoothManager.Instance.GlobalSeaCharacteristic.EnableNotification();
            //BluetoothManager.Instance.GlobalSeaCharacteristic.OnValueFetched += UpdateForcesAndPositions;

            BluetoothManager.Instance.FirmwareRevisionCharacteristic.OnValueRead += UpdateFirmwareRevision;
            BluetoothManager.Instance.FirmwareRevisionCharacteristic.Read();

            _notification = true;
#pragma warning restore 162
        }

        private void DisableNotification()
        {
#if UNITY_EDITOR || !UNITY_ANDROID
            return;
#endif

#pragma warning disable 162
            // ReSharper disable once HeuristicUnreachableCode
            BluetoothManager.Instance.ModeCharacteristic.OnValueFetched -= UpdateMode;
            BluetoothManager.Instance.ModeCharacteristic.DisableNotification();

            BluetoothManager.Instance.IMUCharacteristic.OnValueFetched -= UpdateOrientation;
            BluetoothManager.Instance.IMUCharacteristic.DisableNotification();

            BluetoothManager.Instance.DecompressionSetPointCharacteristic.OnValueFetched -= UpdateDecompression;
            BluetoothManager.Instance.DecompressionSetPointCharacteristic.DisableNotification();

            BluetoothManager.Instance.ResistanceLevelCharacteristic.OnValueFetched -= UpdateResistivity;
            BluetoothManager.Instance.ResistanceLevelCharacteristic.DisableNotification();

            BluetoothManager.Instance.BatteryLevelCharacteristic.OnValueFetched -= UpdateBatteryLevel;
            //BluetoothManager.Instance.BatteryLevelCharacteristic.DisableNotification();

            //BluetoothManager.Instance.SEA1Characteristic.DisableNotification();
            //BluetoothManager.Instance.SEA2Characteristic.DisableNotification();
            //BluetoothManager.Instance.SEA3Characteristic.DisableNotification();
            //BluetoothManager.Instance.SEA4Characteristic.DisableNotification();

            //BluetoothManager.Instance.GlobalSeaCharacteristic.OnValueFetched -= UpdateForcesAndPositions;
            //BluetoothManager.Instance.GlobalSeaCharacteristic.DisableNotification();

            BluetoothManager.Instance.FirmwareRevisionCharacteristic.OnValueRead -= UpdateFirmwareRevision;

            _notification = false;
#pragma warning restore 162
        }

        private void FullSyncUpdate(bool synced)
        {
            Debug.Log($"full sync: {_synced}                  | mode sync: {_modeSynced}\n" +
                      $"decompression sync: {_decompressionSynced} | resistivity sync: {_resistivitySynced}");

            if (!synced && _synced)
            {
                OnFullSyncUpdate?.Invoke(_synced = false);
                return;
            }

            if (!_decompressionSynced || !_modeSynced || !_resistivitySynced)
            {
                if (_synced) OnFullSyncUpdate?.Invoke(_synced = false);
            }
            else
            {
                if(!_synced) OnFullSyncUpdate?.Invoke(_synced = true);
            }
        }

        #endregion


        #region Public methods

        public void SetOffset()
        {
            _offset.Set(-Orientation.x, -Orientation.y, -Orientation.z);
            OnSetOffset?.Invoke(_offset);
        }

        public void ResetOrientationLimits()
        {
            MinOrientation.Set(-InitialRollAngle, InitialPitchUpAngle);
            MaxOrientation.Set(InitialRollAngle, InitialPitchForwardAngle);
        }

        public void ResetConnection()
        {
            _connectionTimer = 0;
            _tickTimer = 0;
            _batteryThresholds = new List<int>() { 5, 10, 15, 20 };

            Connected = true;

            EMode tempMode = _mode;
            _mode = EMode.Stop;

            int[] tempResistivity = _resistivity;
            _resistivity = new [] { 1,1,1,1};

            int[] tempDecompression = _decompression;
            _decompression = new[] {0, 0, 0, 0};

            EnableNotification();

            Mode = tempMode;
            Resistivity = tempResistivity;
            Decompression = tempDecompression;

            Ticking = true;
        }

        #endregion


        #region Static methods

        private static float RectifyAxis(AxisCorrector axisCorrector, float value)
        {
            return (value + axisCorrector.Offset) * axisCorrector.Multiplier;
        }


        public static string MovementString(Movement movement)
        {
            switch (movement)
            {
                case Movement.RollLeft:
                    return "Roll Left";
                case Movement.RollRight:
                    return "Roll Right";
                case Movement.PitchUp:
                    return "Pitch Backwards";
                case Movement.PitchForward:
                    return "Pitch Forward";
                default:
                    throw new ArgumentOutOfRangeException(nameof(movement), movement, null);
            }
        }

        #endregion


        #region Unity methods

        protected void Start ()
        {
            //base.Start();


            //if (instance != this) return;

            ResetConnection();

            OnDecompressionSynced += FullSyncUpdate;
            OnResistivitySynced   += FullSyncUpdate;
            OnModeSynced          += FullSyncUpdate;

            OnOrientationUpdate += orientation => _connectionTimer = 0;

            // debug
            OnFullSyncUpdate += b => Debug.Log($"OnFullSyncUpdate: {b}");
            OnPositionUpdate += position =>
                Debug.Log($"SEA1: {position[0]} | SEA2: {position[1]} | SEA3: {position[2]} | SEA4: {position[3]}");

        }

        private void Update()
        {
#if UNITY_EDITOR
            DummyUpdateOrientation();
#endif

            if (!Connected) return;

#if !UNITY_EDITOR && UNITY_ANDROID
        if (BluetoothManager.Instance.ConnectedBeltGatt == null) return;
#endif

            // check connection state
            if (_connectionTimer - ConnectionTimeOut > 0)
            {
                _connectionTimer = 0;
                Connected = false;
            }
            else if (_synced && _notification)
            {
                _connectionTimer += Time.unscaledDeltaTime;
            }

            // read battery level every minute
            if (_batteryLevelTimer - BatteryLevelDelay > 0)
            {
                _batteryLevelTimer = 0;

#if !UNITY_EDITOR && UNITY_ANDROID
            // BUG: Reading battery level can lead to a bluetooth disconnection
            // https://gitlab.com/japet-atlas/firmware-v2/issues/2
            //BluetoothManager.Instance.BatteryLevelCharacteristic.Read();
#else
                BatteryLevel--;
#endif
            }
            else
            {
                _batteryLevelTimer += Time.unscaledDeltaTime;
            }

            if (!Ticking) return;

            // call Tick() once per second
            if (_tickTimer - TickDelay > 0)
            {
                _tickTimer = 0;
                TickUpdate?.Invoke();
            }
            else
            {
                _tickTimer += Time.unscaledDeltaTime;
            }
        }

#if UNITY_EDITOR
        private void OnValidate()
        {
            name = "BeltControllerV2 - DO NOT CALL FROM EDITOR";

            if (_dummyBatteryLevel == dummyBatteryLevel) return;

            _dummyBatteryLevel = dummyBatteryLevel;
            DummyUpdateBatteryLevel(_dummyBatteryLevel);
        }
#endif

        protected void OnDestroy()
        {
            Debug.Log($"{nameof(BeltControllerV2)} local id: {this.GetInstanceID()}\n{nameof(BeltControllerV2)} id: {Instance.GetInstanceID()}\nOnDestroy() called");

            // check if the current element is the initial singleton or the GO generated when loading a scene
            //if (instance == this)
            DisableNotification();

            //base.OnDestroy();
        }

        #endregion


        #region Application methods

        private void OnApplicationPause(bool pauseStatus)
        {
            Debug.Log($"{nameof(BeltControllerV2)}, OnApplicationPause()");

            if (pauseStatus)
                DisableNotification();
            else
                EnableNotification();
        }

        #endregion


        #region Dummies

        private IEnumerator DummyUpdateResistivity(int[] resistivity, float delay = -1)
        {
            if (delay < 0) delay = UnityEngine.Random.value;
            yield return new WaitForSecondsRealtime(delay);


            OnResistivityUpdate?.Invoke(resistivity);

            if (resistivity.Where((t, i) => t != _resistivity[i]).Any())
            {
                OnResistivitySynced?.Invoke(_resistivitySynced = false);
                yield break;
            }

            OnResistivitySynced?.Invoke(_resistivitySynced = true);
        }

        private IEnumerator DummyUpdateDecompression(int[] decompression, float delay = -1)
        {
            if (delay < 0) delay = UnityEngine.Random.value;
            yield return new WaitForSecondsRealtime(delay);


            OnDecompressionUpdate?.Invoke(decompression);

            if (decompression.Where((t, i) => t != _decompression[i]).Any())
            {
                OnDecompressionSynced?.Invoke(_decompressionSynced = false);
                yield break;
            }

            OnDecompressionSynced?.Invoke(_decompressionSynced = true);
        }

        private IEnumerator DummyUpdateMode(EMode mode, float delay = -1)
        {
            if (delay < 0) delay = UnityEngine.Random.value;
            if (mode == EMode.Stop) delay += 2;

            yield return new WaitForSecondsRealtime(delay);

            OnModeUpdate?.Invoke(mode);
            OnModeSynced?.Invoke(_modeSynced = (mode == _mode));
        }

        private void DummyUpdateBatteryLevel(int batteryLevel)
        {
            if (BatteryLevel != batteryLevel)
            {
                BatteryLevel = batteryLevel;
                OnBatteryLevelUpdate?.Invoke(batteryLevel);
            }

            if (_batteryThresholds == null || !_batteryThresholds.Contains(batteryLevel)) return;

            _batteryThresholds.Remove(item: batteryLevel);
            ErrorModalDialog.Instance.Exec("Battery Low", batteryLevel.ToString());
            OnBatteryLowUpdate?.Invoke(batteryLevel);
        }


        private void DummyUpdateOrientation()
        {
            _orientation[0] += UnityEngine.Input.GetAxis("Horizontal") / 4f;
            _orientation[1] += UnityEngine.Input.GetAxis("Vertical") / 4f;

            UpdateOffsetOrientation();
            UpdateInput();

            OnOrientationUpdate?.Invoke(Orientation);
        }

        #endregion
    }
}
