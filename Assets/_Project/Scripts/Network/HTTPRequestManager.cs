using BestHTTP;
using Root.DesignPatterns;

namespace _Project.Scripts.Network
{
    public class HTTPRequestManager : GlobalSingleton<HTTPRequestManager> {
	
        public HTTPRequest ProcessRequest(HTTPRequest request) {
            request.SetHeader("Accept-Language", "FR");
            return request;
        }
    }
}
