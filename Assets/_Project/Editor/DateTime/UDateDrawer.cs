using _Project.Scripts.Utils.DateTime;
using UnityEditor;
using UnityEngine;

/// <seealso cref="UDate"/>  
[CustomPropertyDrawer(typeof(UDate))]
public class UDateDrawer : PropertyDrawer {

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {

        property.isExpanded = EditorGUI.Foldout(position, property.isExpanded, label);
        if(property.isExpanded) {
            EditorGUI.indentLevel++;

            var UseNowProperty = property.FindPropertyRelative("UseNow");
            EditorGUILayout.PropertyField(property.FindPropertyRelative("UseNow"), new GUIContent(UseNowProperty.displayName));

            if(!UseNowProperty.boolValue) {
                var yearProp = property.FindPropertyRelative("m_year");
                EditorGUILayout.DelayedIntField(yearProp, new GUIContent(yearProp.displayName));

                var monthProp = property.FindPropertyRelative("m_month");
                EditorGUILayout.DelayedIntField(monthProp, new GUIContent(monthProp.displayName));

                var dayProp = property.FindPropertyRelative("m_day");
                EditorGUILayout.DelayedIntField(dayProp, new GUIContent(dayProp.displayName));
            }
            EditorGUI.indentLevel--;
        }
    }
}
