using System;
using _Project.Scripts.Utils.DateTime;
using UnityEditor;
using UnityEngine;

/// <seealso cref="UDateTime"/>  
[CustomPropertyDrawer(typeof(UDateTime))]
public class UDateTimeDrawer : PropertyDrawer {
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
        
        property.isExpanded = EditorGUI.Foldout(position, property.isExpanded, label);
        if(property.isExpanded) {
            EditorGUI.indentLevel++;

            var UseNowProperty = property.FindPropertyRelative("UseNow");
            EditorGUILayout.PropertyField(property.FindPropertyRelative("UseNow"), new GUIContent(UseNowProperty.displayName));

            if(!UseNowProperty.boolValue) {
                EditorGUILayout.LabelField("Date", EditorStyles.boldLabel);
                var yearProp = property.FindPropertyRelative("m_year");
                EditorGUILayout.DelayedIntField(yearProp, new GUIContent(yearProp.displayName));

                var monthProp = property.FindPropertyRelative("m_month");
                EditorGUILayout.DelayedIntField(monthProp, new GUIContent(monthProp.displayName));

                var dayProp = property.FindPropertyRelative("m_day");
                EditorGUILayout.DelayedIntField(dayProp, new GUIContent(dayProp.displayName));

                EditorGUILayout.Space();

                EditorGUILayout.LabelField("Time", EditorStyles.boldLabel);
                var hourProp = property.FindPropertyRelative("m_hour");
                EditorGUILayout.DelayedIntField(hourProp, new GUIContent(hourProp.displayName));

                var minuteProp = property.FindPropertyRelative("m_minute");
                EditorGUILayout.DelayedIntField(minuteProp, new GUIContent(minuteProp.displayName));

                var secondProp = property.FindPropertyRelative("m_second");
                EditorGUILayout.DelayedIntField(secondProp, new GUIContent(secondProp.displayName));

                var msProp = property.FindPropertyRelative("m_millisecond");
                EditorGUILayout.DelayedIntField(msProp, new GUIContent(msProp.displayName));
            }

            EditorGUI.indentLevel--;
        }
    }
}
