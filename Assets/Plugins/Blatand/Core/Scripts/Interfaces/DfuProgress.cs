﻿using System;
using UnityEngine;

namespace Blatand.Core
{

    /// <summary>
    /// This abstract class is used to implement DfuProgressListener callbacks for status, progress and error events.
    /// </summary>
    public abstract class DfuProgress : AndroidJavaProxy
    {
        public DfuProgress() : base("com.blatand.DfuProgress")
        {
        }

        public const int ERROR_TYPE_OTHER = 0;
        public const int ERROR_TYPE_COMMUNICATION_STATE = 1;
        public const int ERROR_TYPE_COMMUNICATION = 2;
        public const int ERROR_TYPE_DFU_REMOTE = 3;

        /// <summary>
        /// Method called when the DFU service started connecting with the DFU target.
        /// </summary>
        /// <param name="deviceAddress">The target device address.</param>
        public abstract void OnDeviceConnecting(string deviceAddress);

        /// <summary>
        /// Method called when the service has successfully connected, discovered services and found DFU service on the DFU target.
        /// </summary>
        /// <param name="deviceAddress">The target device address.</param>
        public abstract void OnDeviceConnected(string deviceAddress);

        /// <summary>
        /// Method called when the DFU process is starting.
        /// This includes reading the DFU Version characteristic, sending DFU_START command as well as the Init packet, if set.
        /// </summary>
        /// <param name="deviceAddress">The target device address.</param>
        public abstract void OnDfuProcessStarting(string deviceAddress);

        /// <summary>
        /// Method called when the DFU process was started and bytes about to be sent.
        /// </summary>
        /// <param name="deviceAddress">The target device address.</param>
        public abstract void OnDfuProcessStarted(string deviceAddress);

        /// <summary>
        /// Method called when the service discovered that the DFU target is in the application mode
        /// and must be switched to DFU mode.The switch command will be sent and the DFU process
        /// should start again.There will be no <see cref="OnDeviceDisconnected"/> event following this call.
        /// </summary>
        /// <param name="deviceAddress">The target device address.</param>
        public abstract void OnEnablingDfuMode(string deviceAddress);

        /// <summary>
        /// Method called when the DFU process succeeded.
        /// </summary>
        /// <param name="deviceAddress">The target device address.</param>
        public abstract void OnDfuCompleted(string deviceAddress);

        /// <summary>
        /// Method called when the new firmware is being validated on the target device.
        /// </summary>
        /// <param name="deviceAddress">The target device address.</param>
        public abstract void OnFirmwareValidating(string deviceAddress);

        /// <summary>
        /// Method called when the service started to disconnect from the target device.
        /// </summary>
        /// <param name="deviceAddress">The target device address.</param>
        public abstract void OnDeviceDisconnecting(string deviceAddress);

        /// <summary>
        /// Method called when the service disconnected from the device. The device has been reset.
        /// </summary>
        /// <param name="deviceAddress">The target device address.</param>
        public abstract void OnDeviceDisconnected(string deviceAddress);

        /// <summary>
        /// Method called during uploading the firmware.
        /// It will not be called twice with the same value of percent, however, in case of small firmware files, some values may be omitted.
        /// </summary>
        /// <param name="deviceAddress">The target device address.</param>
        /// <param name="percent">The current status of upload (0-99).</param>
        /// <param name="speed">The current speed in bytes per millisecond.</param>
        /// <param name="avgSpeed">The average speed in bytes per millisecond.</param>
        /// <param name="currentPart">The number pf part being sent. In case the ZIP file contains a Soft Device
        ///                           and/or a Bootloader together with the application the SD+BL are sent as part 1,
        ///                           then the service starts again and send the application as part 2.</param>
        /// <param name="partsTotal">int: total number of parts.</param>
        public abstract void OnProgressChanged(string deviceAddress, int percent, float speed, float avgSpeed, int currentPart, int partsTotal);

        /// <summary>
        /// Method called when an error occur.
        /// </summary>
        /// <param name="deviceAddress">The target device address.</param>
        /// <param name="error">The target device address.</param>
        /// <param name="errorType">The error type, one of:
        ///                         <see cref="ERROR_TYPE_COMMUNICATION_STATE"/>
        ///                         <see cref="ERROR_TYPE_COMMUNICATION"/>
        ///                         <see cref="ERROR_TYPE_DFU_REMOTE"/>
        ///                         <see cref="ERROR_TYPE_OTHER"/></param>
        /// <param name="message">The error message.</param>
        public virtual void OnError(string deviceAddress, int error, int errorType, string message)
        {
            string errorTypeStr;

            switch (errorType)
            {
                case ERROR_TYPE_COMMUNICATION_STATE:
                    errorTypeStr = "COMMUNICATION STATE";
                    break;
                case ERROR_TYPE_COMMUNICATION:
                    errorTypeStr = "COMMUNICATION";
                    break;
                case ERROR_TYPE_DFU_REMOTE:
                    errorTypeStr = "DFU_REMOTE";
                    break;
                case ERROR_TYPE_OTHER:
                    errorTypeStr = "OTHER";
                    break;
                default:
                    throw  new ArgumentOutOfRangeException();
            }
            Debug.Log($"DfuProgress ~ Error type: {errorTypeStr}, code: {error}, message: {message}");
        }

        /// <summary>
        /// Method called when the DFU process has been aborted.
        /// </summary>
        /// <param name="deviceAddress">The target device address.</param>
        public abstract void OnDfuAborted(string deviceAddress);
    }
}
