using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Blatand.Core.Android
{
    public class Scanner : AAndroidObjectWrapper
    {
        private class InternalScanCallback : ScanCallback
        {
            /// <summary>
            /// Scanner linked with this callbacks
            /// </summary>
            Scanner m_source;

			public InternalScanCallback(Scanner source) : base() {
				m_source = source;
			}

            public override void OnScanResult(int callbackType, /* ScanResult */ AndroidJavaObject result)
            {
                Device device = new Device(result.Call<AndroidJavaObject>("getDevice"));
                UnityMainThreadDispatcher.Instance().Enqueue(() => {
                    switch (callbackType)
                    {
                        case ScanSettings_CALLBACK_TYPE_ALL_MATCHES:
                            m_source.OnDeviceDiscovered?.Invoke(device);
                            break;
                        case ScanSettings_CALLBACK_TYPE_FIRST_MATCH:
                            m_source.OnDeviceDiscovered?.Invoke(device);
                            break;
                        case ScanSettings_CALLBACK_TYPE_MATCH_LOST:
                            m_source.OnDeviceLost?.Invoke(device);
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                });
            }

            public override void OnScanFailed(int errorCode)
            {
                base.OnScanFailed(errorCode);
                m_source.OnScanFailed?.Invoke(errorCode);
            }

            public override void OnBatchScanResults(AndroidJavaObject results)
            {
                List<Device> devices = new List<Device>();
             
                int size = results.Call<int>("size");

                for (int i = 0; i < size; i++)
                {
                    AndroidJavaObject result = results.Call<AndroidJavaObject>("get", i);

                    devices.Add(new Device(result.Call<AndroidJavaObject>("getDevice")));
                }

                m_source.OnBatchDevicesDelivered?.Invoke(devices);
            }
        }

        private AndroidJavaObject m_callbacks;

        public bool IsScanning { get; private set; }

        #region Events

        /// <summary>
        /// Triggered when a BLE device is detected.
        /// </summary>
        public event Action<Device> OnDeviceDiscovered;

        /// <summary>
        /// Triggered when a BLE device is lost.
        /// </summary>
        public event Action<Device> OnDeviceLost;

        /// <summary>
        /// Triggered when a batch of devices is received.
        /// A batch is created when there is a report delay > 0.
        /// A batch can also be created due to a high number of devices around.
        /// </summary>
        public event Action<List<Device>> OnBatchDevicesDelivered;

        /// <summary>
        /// Triggered when a scan failed due to different reason, see ScanCallback.
        /// </summary>
        public event Action<int> OnScanFailed;

        #endregion

        public static Scanner NewScanner()
        {
            return new Scanner(BluetoothAdapter.GetSystemAdapter().AndroidObject.Call<AndroidJavaObject>("getBluetoothLeScanner"));
        }

        internal Scanner(AndroidJavaObject androidObject) : base (androidObject)
        {
            Assert.IsNotNull(androidObject);
        }

        /// <summary>
        /// Start Bluetooth LE scan.
        /// </summary>
        public void Start()
        {
            if(IsScanning) return;

            if (m_callbacks == null) m_callbacks = new AndroidJavaObject("com.blatand.ScanCallbackInterfacer", new InternalScanCallback(this));
            m_androidObject.Call("startScan", m_callbacks);

            IsScanning = true;
        }

        /// <summary>
        /// Stop an ongoing Bluetooth LE scan.
        /// </summary>
        public void Stop()
        {
            if (!IsScanning) return;

            m_androidObject.Call("stopScan", m_callbacks);

            IsScanning = false;
        }
    }
}

