﻿using Sirenix.Utilities;
using System;
using UnityEngine;

namespace Blatand.Core.Android
{
    public class DfuHandler : AAndroidObjectWrapper
    {
        private class InternalDfuProgress : DfuProgress
        {
            private readonly DfuHandler _source;

            public InternalDfuProgress(DfuHandler source)
            {
                _source = source;
            }

            public override void OnDeviceConnecting(string deviceAddress)
            {
                _source.OnDeviceConnecting?.Invoke();
            }

            public override void OnDeviceConnected(string deviceAddress)
            {
                _source.OnDeviceConnected?.Invoke();
            }

            public override void OnDfuProcessStarting(string deviceAddress)
            {
                _source.OnDfuProcessStarting?.Invoke();
            }

            public override void OnDfuProcessStarted(string deviceAddress)
            {
                _source.OnDfuProcessStarted?.Invoke();
            }

            public override void OnEnablingDfuMode(string deviceAddress)
            {
                _source.OnEnablingDfuMode?.Invoke();
            }

            public override void OnDfuCompleted(string deviceAddress)
            {
                _source.OnDfuCompleted?.Invoke();
                _source.Stop();
            }

            public override void OnFirmwareValidating(string deviceAddress)
            {
                _source.OnFirmwareValidating?.Invoke();
            }

            public override void OnDeviceDisconnecting(string deviceAddress)
            {
                _source.OnDeviceDisconnecting?.Invoke();
            }

            public override void OnDeviceDisconnected(string deviceAddress)
            {
                _source.OnDeviceDisconnected?.Invoke();
            }

            public override void OnProgressChanged(string deviceAddress, int percent, float speed, float avgSpeed, int currentPart, int partsTotal)
            {
                _source.OnProgressChanged?.Invoke(new Progress()
                {
                    percent = percent, 
                    speed = speed,
                    averageSpeed = avgSpeed,
                    currentPart = currentPart,
                    partsTotal = partsTotal
                });
            }

            public override void OnError(string deviceAddress, int error, int errorType, string message)
            {
                base.OnError(deviceAddress, error, errorType, message);

                _source.OnError?.Invoke(new Error()
                {
                    error = error,
                    errorType = errorType,
                    message = message
                });
            }

            public override void OnDfuAborted(string deviceAddress)
            {
                _source.OnDfuProcessAborted?.Invoke();
            }
        }

        private AndroidJavaObject _progressInterfacer;

        private Device _device;

        #region Properties



        #endregion

        #region Events

        public event Action OnDeviceConnecting;
        public event Action OnDeviceConnected;

        public event Action OnDeviceDisconnecting;
        public event Action OnDeviceDisconnected;

        public event Action OnDfuProcessStarting;
        public event Action OnDfuProcessStarted;
        
        public event Action OnDfuCompleted;
        public event Action OnFirmwareValidating;

        public event Action<Progress> OnProgressChanged;

        public event Action<Error> OnError;
        public event Action OnDfuProcessAborted;

        public event Action OnEnablingDfuMode;

        #endregion

        #region Structures

        public struct Progress
        {
            public int percent;
            public float speed;
            public float averageSpeed;
            public int currentPart;
            public int partsTotal;
        }

        public struct Error
        {
            public int error;
            public int errorType;
            public string message;
        }

        #endregion

        /// <summary>
        /// Starts a DFU on the connected device with the .zip file containing firmware's files.
        /// </summary>
        /// <param name="device">The target device to update.</param>
        /// <param name="path">The path to the firmware's zip file, located in the "cache" folder.</param>
        public void Start(Device device, string path)
        {
            if (device == null || path.IsNullOrWhitespace()) return;

            _device = device;

            if (_progressInterfacer == null)
            {
                _progressInterfacer = new AndroidJavaObject("com.blatand.DfuProgressInterfacer", new InternalDfuProgress(this));
            }
            
            BlatandPlugin.Instance.StartDfuUpdate(device.AndroidObject, path, _progressInterfacer);
        }

        /// <summary>
        /// Stops a DFU by disconnecting from the device.
        /// </summary>
        public void Stop()
        {
            _device.Disconnect();
        }
    }
}
