using System;
using UnityEngine;

namespace Blatand.Core.Android
{
    public class Device : AAndroidObjectWrapper
    {
        /// <summary>
        /// Get the friendly Bluetooth name of the remote device.
        /// </summary>
        /// <value>the Bluetooth name, or null if there was a problem.</value>
        public string Name => m_androidObject == null 
            ? $"ATLAS-{Time.timeSinceLevelLoad:0000}" 
            : m_androidObject.Call<string>("getName");

        /// <summary>
        /// Returns the hardware address of this Bluetooth device.
        /// </summary>
        /// <value>Bluetooth hardware address as string</value>
        public string Address => m_androidObject.Call<string>("getAddress");

        private Gatt m_gatt;
        public Gatt Gatt => m_gatt;

        #region Events

        public event Action<Device, Gatt> OnConnected;

        public event Action<Device, Gatt> OnDisconnected;

        #endregion

        public Device(AndroidJavaObject androidObject) : base(androidObject)
        {

        }

        public Gatt Connect()
        {
            AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

            m_gatt = new Gatt();
            m_gatt.OnConnected += TriggerOnConnected;
            m_gatt.OnDisconnected += TriggerOnDisconnected;

            AndroidJavaObject callbacks = new AndroidJavaObject("com.blatand.GattCallbackInterfacer", new Gatt.InternalGattCallback(m_gatt));
            Debug.Log("Callbacks: " + callbacks);

            m_gatt.m_callbacks = callbacks;
            if (GetSDKInt() >= 23)
            {
                m_gatt.AndroidObject = m_androidObject.Call<AndroidJavaObject>("connectGatt", activity, false, callbacks, 2);
            }
            else
            {
                m_gatt.AndroidObject = m_androidObject.Call<AndroidJavaObject>("connectGatt", activity, false, callbacks);
            }
   
            return m_gatt;
        }
        public void Disconnect()
        {
            m_gatt?.Disconnect();
        }


        private void TriggerOnConnected(Gatt gatt)
        {
            OnConnected?.Invoke(this, gatt);
        }

        private void TriggerOnDisconnected(Gatt gatt)
        {
            OnDisconnected?.Invoke(this, gatt);
        }

        private static int GetSDKInt()
        {
            using (AndroidJavaClass version = new AndroidJavaClass("android.os.Build$VERSION"))
            {
                return version.GetStatic<int>("SDK_INT");
            }
        }
    }
}