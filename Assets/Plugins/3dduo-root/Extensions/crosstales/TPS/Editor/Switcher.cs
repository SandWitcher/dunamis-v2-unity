﻿#if UNITY_5 || UNITY_2017 || UNITY_2018
using UnityEngine;
using UnityEditor;

namespace Crosstales.TPS
{
    /// <summary>Platform switcher.</summary>
    public static class Switcher
    {

        /// <summary>Switches the current platform to the target via CLI.</summary>
        public static void SwitchCLI()
        {
            Switch(getArgument("-tpsBuild"), getArgument("-tpsExecuteMethod"), ("true".CTEquals(getArgument("-tpsBatchmode")) ? true : false), ("false".CTEquals(getArgument("-tpsQuit")) ? false : true), ("true".CTEquals(getArgument("-tpsNoGraphics")) ? true : false), ("true".CTEquals(getArgument("-tpsCopySettings")) ? true : false));
        }

        /// <summary>Switches the current platform to the target.</summary>
        /// <param name="build">Build type name for Unity, like 'win64'</param>
        /// <param name="executeMethod">Execute method after switch (optional)</param>
        /// <param name="batchmode">Start Unity in batch-mode (default: false, optional)</param>
        /// <param name="quit">Quit Unity in batch-mode (default: true, optional)</param>
        /// <param name="noGraphics">Disable graphic devices in batch-mode (default: false, optional)</param>
        /// <param name="copySettings">Copy the project settings (default: false, optional)</param>
        public static void Switch(string build, string executeMethod = "", bool batchmode = false, bool quit = true, bool noGraphics = false, bool copySettings = false)
        {
            if (string.IsNullOrEmpty(build))
                throw new System.ArgumentNullException("build");

            Util.Config.EXECUTE_METHOD = executeMethod;
            Util.Config.BATCHMODE = batchmode;
            Util.Config.QUIT = quit;
            Util.Config.NO_GRAPHICS = noGraphics;
            Util.Config.COPY_SETTINGS = copySettings;

            //Debug.Log("build: " + build);
            //Debug.Log("executeMethod: " + executeMethod);
            //Debug.Log("batchmode: " + batchmode);
            //Debug.Log("quit: " + quit);
            //Debug.Log("noGraphics: " + noGraphics);
            //Debug.Log("copySettings: " + copySettings);

            Switch(getBuildTarget(build), build, MobileTextureSubtarget.Generic);
        }

        /// <summary>Switches the current platform to the target.</summary>
        /// <param name="target">Target platform for the switch</param>
        /// <param name="build">Build type name for Unity, like 'win64'</param>
        /// <param name="subTarget">Texture format (Android)</param>
        public static void Switch(BuildTarget target, string build, MobileTextureSubtarget subTarget)
        {
            if (target == EditorUserBuildSettings.activeBuildTarget) //ignore switch
            {
                Debug.LogWarning("Target platform is equals the current platform - switch ignored.");

                if (!string.IsNullOrEmpty(Util.Config.EXECUTE_METHOD))
                {
                    string[] parts = Util.Config.EXECUTE_METHOD.Split('.');

                    if (parts.Length > 1)
                    {
                        string className = parts[0];

                        for (int ii = 1; ii < parts.Length - 1; ii++)
                        {
                            className += "." + parts[ii];
                        }

                        System.Type type = System.Type.GetType(className);
                        System.Reflection.MethodInfo method = type.GetMethod(parts[parts.Length - 1], System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public);

                        method.Invoke(null, null);
                    }
                    else
                    {
                        Debug.LogWarning("Execute method is invalid!");
                    }
                }
            }
            else
            {
                Util.Helper.SwitchPlatform(target, build, subTarget);
            }
        }

        /// <summary>Test the switch with an execute method.</summary>
        public static void SayHello()
        {
            Debug.LogError("Hello everybody, I was called by TPS!");
        }

        private static string getArgument(string name)
        {
            var args = System.Environment.GetCommandLineArgs();

            for (int ii = 0; ii < args.Length; ii++)
            {
                if (args[ii] == name && args.Length > ii + 1)
                    return args[ii + 1];
            }

            return null;
        }

        private static BuildTarget getBuildTarget(string build)
        {
            if ("win32".CTEquals(build) || "win".CTEquals(build))
            {
                return BuildTarget.StandaloneWindows;
            }
            else if ("win64".CTEquals(build))
            {
                return BuildTarget.StandaloneWindows64;
            }
#if UNITY_2017_3_OR_NEWER
            else if (build.CTContains("osx"))
            {
                return BuildTarget.StandaloneOSX;
            }
#else
            else if ("osx".CTEquals(build) || "OSXIntel".CTEquals(build))
            {
                return BuildTarget.StandaloneOSXIntel;
            }
            else if ("osxintel64".CTEquals(build))
            {
                return BuildTarget.StandaloneOSXIntel64;
            }
            else if ("osxuniversal".CTEquals(build))
            {
                return BuildTarget.StandaloneOSXUniversal;
            }
#endif
            else if ("linux".CTEquals(build))
            {
                return BuildTarget.StandaloneLinux;
            }
            else if ("linux64".CTEquals(build))
            {
                return BuildTarget.StandaloneLinux64;
            }
            else if ("linuxuniversal".CTEquals(build))
            {
                return BuildTarget.StandaloneLinuxUniversal;
            }
            else if ("android".CTEquals(build))
            {
                return BuildTarget.Android;
            }
#if UNITY_5 || UNITY_2017 || UNITY_2018
            else if ("ios".CTEquals(build))
            {
                return BuildTarget.iOS;
            }
#endif
            else if ("wsaplayer".CTEquals(build) || "WindowsStoreApps".CTEquals(build))
            {
                return BuildTarget.WSAPlayer;
            }
#if UNITY_4_6 || UNITY_4_7 || UNITY_5_0 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3
            else if ("webplayer".CTEquals(build))
            {
                return BuildTarget.WebPlayer;
            }
#endif
            else if ("webgl".CTEquals(build))
            {
                return BuildTarget.WebGL;
            }
#if UNITY_5_3 || UNITY_5_3_OR_NEWER
            else if ("tvOS".CTEquals(build))
            {
                return BuildTarget.tvOS;
            }
#endif
#if !UNITY_2017_3_OR_NEWER
            else if ("samsungtv".CTEquals(build))
            {
                return BuildTarget.SamsungTV;
            }
#endif
#if !UNITY_5_5_OR_NEWER
            else if ("ps3".CTEquals(build))
            {
                return BuildTarget.PS3;
            }
#endif
            else if ("ps4".CTEquals(build))
            {
                return BuildTarget.PS4;
            }
#if !UNITY_5_5_OR_NEWER
            else if ("xbox360".CTEquals(build))
            {
                return BuildTarget.XBOX360;
            }
#endif
            else if ("xboxone".CTEquals(build))
            {
                return BuildTarget.XboxOne;
            }
#if UNITY_5_6_OR_NEWER
            else if ("switch".CTEquals(build))
            {
                return BuildTarget.Switch;
            }
#endif

            return BuildTarget.StandaloneWindows;
        }

    }
}
#endif
// © 2018 crosstales LLC (https://www.crosstales.com)
