﻿using UnityEditor;
using UnityEngine;
using Crosstales.TPS.Util;

namespace Crosstales.TPS.EditorIntegration
{
    /// <summary>Unity "Preferences" extension.</summary>
    public class ConfigPreferences : ConfigBase
    {

        #region Variables

        private static int tab = 0;
        private static int lastTab = 0;
        private static ConfigPreferences cp;

        #endregion


        #region Static methods

        [PreferenceItem(Constants.ASSET_NAME)]
        private static void PreferencesGUI()
        {
            if (cp == null) {
                cp = ScriptableObject.CreateInstance(typeof(ConfigPreferences)) as ConfigPreferences;
            }

            tab = GUILayout.Toolbar(tab, new string[] { "Switch", "Config", "Help", "About" });

            if (tab != lastTab)
            {
                lastTab = tab;
                GUI.FocusControl(null);
            }
            if (tab == 0)
            {
                cp.showSwitch();
            }
            else if (tab == 1)
            {
                cp.showConfiguration();
            }
            else if (tab == 2)
            {
                cp.showHelp();
            }
            else
            {
                cp.showAbout();
            }
        }

        #endregion
    }
}
// © 2016-2018 crosstales LLC (https://www.crosstales.com)