﻿using UnityEngine;
using UnityEditor;
using Crosstales.TPS.Util;
using Crosstales.TPS.Task;

namespace Crosstales.TPS.EditorIntegration
{
    /// <summary>Base class for editor windows.</summary>
    public abstract class ConfigBase : EditorWindow
    {

        #region Variables

        private static string updateText = UpdateCheck.TEXT_NOT_CHECKED;
        private static UpdateStatus updateStatus = UpdateStatus.NOT_CHECKED;

        private System.Threading.Thread worker;

        private Vector2 scrollPosSwitch;
        private Vector2 scrollPosPlatforms;
        private Vector2 scrollPosConfig;
        private Vector2 scrollPosHelp;
        private Vector2 scrollPosAboutUpdate;
        private Vector2 scrollPosAboutReadme;
        private Vector2 scrollPosAboutVersions;

        private static string readme;
        private static string versions;

        private int aboutTab = 0;

        private const int rowWidth = 10000;
        private static int rowHeight = 0; //later = 36
        private static int rowCounter = 0;

        private const int logoWidth = 36;
        private const int platformWidth = 128;
        private const int architectureWidth = 64;
        private const int textureWidth = 72;
        private const int cacheWidth = 40;
        private const int actionWidth = 48;

        private static int platformX = 0;
        private static int platformY = 0;
        private static int platformTextSpace = 12; //later = 18

        private static int cacheTextSpace = 6; //later = 18

        private static int actionTextSpace = 6; //later = 8

        private static readonly string[] vcsOptions = { "None", "git", "SVN", "Mercurial" };

        private static readonly string[] archWinOptions = { "32bit", "64bit" };
        private static readonly string[] archMacOptions = { "32bit", "64bit", "Universal" };
        private static readonly string[] archLinuxOptions = { "32bit", "64bit", "Universal" };

        private static readonly string[] texAndroidOptions = { "Generic", "DXT", "PVRTC", "ATC", "ETC", "ETC2", "ASTC" };

        #endregion


        #region Properties

        private static string buildWindows
        {
            get
            {
#if UNITY_2017_2_OR_NEWER
                if (Config.ARCH_WINDOWS == 0)
                {
                    return "Win";
                }

                return "Win64";
#else
                if (Config.ARCH_WINDOWS == 0)
                {
                    return "win32";
                }

                return "win64";
#endif
            }
        }

        private static string buildMac
        {
            get
            {
#if UNITY_2017_3_OR_NEWER
                return "OSXUniversal";
#else
                if (Config.ARCH_MAC == 0)
                {
                    return "osx";
                }
                else if (Config.ARCH_MAC == 1)
                {
                    return "osxintel64";
                }

                return "osxuniversal";
#endif
            }
        }

        private static string buildLinux
        {
            get
            {
#if UNITY_2017_2_OR_NEWER
                if (Config.ARCH_LINUX == 0)
                {
                    return "Linux";
                }
                else if (Config.ARCH_LINUX == 1)
                {
                    return "Linux64";
                }

                return "LinuxUniversal";
#else
                if (Config.ARCH_LINUX == 0)
                {
                    return "linux";
                }
                else if (Config.ARCH_LINUX == 1)
                {
                    return "linux64";
                }

                return "linuxuniversal";
#endif
            }
        }

        private static BuildTarget targetWindows
        {
            get
            {
                if (Config.ARCH_WINDOWS == 0)
                {
                    return BuildTarget.StandaloneWindows;
                }

                return BuildTarget.StandaloneWindows64;
            }
        }

        private static BuildTarget targetMac
        {
            get
            {
#if UNITY_2017_3_OR_NEWER
                return BuildTarget.StandaloneOSX;
#else                
                if (Config.ARCH_MAC == 0)
                {
                    return BuildTarget.StandaloneOSXIntel;
                }
                else if (Config.ARCH_MAC == 1)
                {
                    return BuildTarget.StandaloneOSXIntel64;
                    
                }

                return BuildTarget.StandaloneOSXUniversal;
#endif
            }
        }

        private static BuildTarget targetLinux
        {
            get
            {
                if (Config.ARCH_LINUX == 0)
                {
                    return BuildTarget.StandaloneLinux;
                }
                else if (Config.ARCH_LINUX == 1)
                {
                    return BuildTarget.StandaloneLinux64;
                }

                return BuildTarget.StandaloneLinuxUniversal;
            }
        }

#if UNITY_5 || UNITY_2017 || UNITY_2018
        private static MobileTextureSubtarget texAndroid
        {
            get
            {
                if (Config.TEX_ANDROID == 1)
                {
                    return MobileTextureSubtarget.DXT;
                }
                else if (Config.TEX_ANDROID == 2)
                {
                    return MobileTextureSubtarget.PVRTC;
                }
                else if (Config.TEX_ANDROID == 3)
                {
                    return MobileTextureSubtarget.ETC;
                }
                else if (Config.TEX_ANDROID == 4)
                {
                    return MobileTextureSubtarget.ETC;
                }
                else if (Config.TEX_ANDROID == 5)
                {
                    return MobileTextureSubtarget.ETC2;
                }
                else if (Config.TEX_ANDROID == 6)
                {
                    return MobileTextureSubtarget.ASTC;
                }

                return MobileTextureSubtarget.Generic;
            }
        }
#else
        private static AndroidBuildSubtarget texAndroid
        {
            get
            {
                if (Config.TEX_ANDROID == 1)
                {
                    return AndroidBuildSubtarget.DXT;
                }
                else if (Config.TEX_ANDROID == 2)
                {
                    return AndroidBuildSubtarget.PVRTC;
                }
                else if (Config.TEX_ANDROID == 3)
                {
                    return AndroidBuildSubtarget.ATC;
                }
                else if (Config.TEX_ANDROID == 4)
                {
                    return AndroidBuildSubtarget.ETC;

                }
                
                return AndroidBuildSubtarget.Generic;
            }
        }

#endif

        #endregion


        #region Protected methods

        protected void showSwitch()
        {
            GUI.skin.label.wordWrap = true;

            if (Helper.isEditorMode)
            {
                platformX = 0;
                platformY = 0;
                platformTextSpace = 12; //later = 18

                rowHeight = 0; //later = 36
                rowCounter = 0;

                cacheTextSpace = 6; //later = 18
                actionTextSpace = 6; //later = 8

                // header
                drawHeader();

                // scrollPosSwitch = EditorGUILayout.BeginScrollView(scrollPosSwitch, false, false);
                // {
                //header
                // drawHeader();
                // }

                scrollPosSwitch = EditorGUILayout.BeginScrollView(scrollPosSwitch, false, false);
                {
                    //content
                    drawContent();
                }
                EditorGUILayout.EndScrollView();

                Helper.SeparatorUI();

                //              EditorGUILayout.BeginHorizontal();
                //              {
                GUILayout.Label("Cache usage:\t" + Helper.ScanTotalCache());

                //GUILayout.Space(24);

                //                  EditorGUILayout.SelectableLabel(Helper.ScanTotalCache());
                //              }
                //              EditorGUILayout.EndHorizontal();

                Config.SHOW_DELETE = EditorGUILayout.Toggle(new GUIContent("Show Delete Buttons", "Shows or hides the delete button for the cache."), Config.SHOW_DELETE);

                GUILayout.Space(6);
            }
            else
            {
                EditorGUILayout.HelpBox("Disabled in Play-mode!", MessageType.Info);
            }
        }

        protected void showConfiguration()
        {
            scrollPosPlatforms = EditorGUILayout.BeginScrollView(scrollPosPlatforms, false, false);
            {
                GUILayout.Label("General Settings", EditorStyles.boldLabel);
                Config.CUSTOM_PATH_CACHE = EditorGUILayout.BeginToggleGroup(new GUIContent("Custom Cache Path", "Enable or disable a custom cache path (default: " + Constants.DEFAULT_CUSTOM_PATH_CACHE + ")."), Config.CUSTOM_PATH_CACHE);
                {
                    EditorGUI.indentLevel++;

                    EditorGUILayout.BeginHorizontal();
                    {
                        EditorGUILayout.SelectableLabel(Config.PATH_CACHE);

                        if (GUILayout.Button(new GUIContent(" Select", Helper.Icon_Folder, "Select path for the cache")))
                        {
                            string path = EditorUtility.OpenFolderPanel("Select path for the cache", Config.PATH_CACHE.Substring(0, Config.PATH_CACHE.Length - (Constants.CACHE_DIRNAME.Length + 1)), "");

                            if (!string.IsNullOrEmpty(path))
                            {
                                Config.PATH_CACHE = path + "/" + Constants.CACHE_DIRNAME;
                                GAApi.Event(typeof(ConfigBase).Name, "Custom cache path changed");
                            }
                        }
                    }
                    EditorGUILayout.EndHorizontal();

                    EditorGUI.indentLevel--;
                }
                EditorGUILayout.EndToggleGroup();

                if (Config.CUSTOM_PATH_CACHE)
                {
                    GUI.enabled = false;
                }
                Config.VCS = EditorGUILayout.Popup("Version Control", Config.VCS, vcsOptions);
                GUI.enabled = true;

                Config.BATCHMODE = EditorGUILayout.BeginToggleGroup(new GUIContent("Batch mode", "Enable or disable batch mode for CLI operations (default: " + Constants.DEFAULT_BATCHMODE + ")"), Config.BATCHMODE);
                {
                    EditorGUI.indentLevel++;

                    Config.QUIT = EditorGUILayout.Toggle(new GUIContent("Quit", "Enable or disable quit Unity Editor for CLI operations (default: " + Constants.DEFAULT_QUIT + ")."), Config.QUIT);

                    Config.NO_GRAPHICS = EditorGUILayout.Toggle(new GUIContent("No graphics", "Enable or disable graphics device in Unity Editor for CLI operations (default: " + Constants.DEFAULT_NO_GRAPHICS + ")."), Config.NO_GRAPHICS);

                    EditorGUI.indentLevel--;
                }
                EditorGUILayout.EndToggleGroup();
                
                //Config.BATCHMODE = EditorGUILayout.Toggle(new GUIContent("Batch mode", "Enable or disable batch mode for CLI operations (default: " + Constants.DEFAULT_BATCHMODE + ")"), Config.BATCHMODE);

                Config.EXECUTE_METHOD = EditorGUILayout.TextField(new GUIContent("Execute Method", "Execute static method <ClassName.MethodName> in Unity after a switch (default: empty)."), Config.EXECUTE_METHOD);

                Config.COPY_SETTINGS = EditorGUILayout.Toggle(new GUIContent("Copy Settings", "Enable or disable copying the 'ProjectSettings'-folder (default: " + Constants.DEFAULT_COPY_SETTINGS + ")."), Config.COPY_SETTINGS);

                //Config.COPY_ASSETS = EditorGUILayout.Toggle(new GUIContent("Copy Assets", "Enable or disable copying the 'Assets'-folder (default: " + Constants.DEFAULT_COPY_ASSETS + ")."), Config.COPY_ASSETS);

                //Constants.DEBUG = EditorGUILayout.Toggle(new GUIContent("Debug", "Enable or disable debug logs (default: " + Constants.DEFAULT_DEBUG + ")."), Constants.DEBUG);

                Config.UPDATE_CHECK = EditorGUILayout.Toggle(new GUIContent("Update Check", "Enable or disable the update-check (default: " + Constants.DEFAULT_UPDATE_CHECK + ")"), Config.UPDATE_CHECK);

                Config.REMINDER_CHECK = EditorGUILayout.Toggle(new GUIContent("Reminder Check", "Enable or disable the reminder-check (default: " + Constants.DEFAULT_REMINDER_CHECK + ")"), Config.REMINDER_CHECK);

                Config.TELEMETRY = EditorGUILayout.Toggle(new GUIContent("Telemetry", "Enable or disable anonymous telemetry data (default: " + Constants.DEFAULT_TELEMETRY + ")"), Config.TELEMETRY);

                Helper.SeparatorUI();

                GUILayout.Label("UI Settings", EditorStyles.boldLabel);
                Config.CONFIRM_SWITCH = EditorGUILayout.Toggle(new GUIContent("Confirm Switch", "Enable or disable the switch confirmation dialog (default: " + Constants.DEFAULT_CONFIRM_SWITCH + ")."), Config.CONFIRM_SWITCH);
                Config.SHOW_COLUMN_PLATFORM = EditorGUILayout.Toggle(new GUIContent("Column: Platform", "Enable or disable the column 'Platform' in the 'Switch'-aboutTab (default: " + Constants.DEFAULT_SHOW_COLUMN_PLATFORM + ")."), Config.SHOW_COLUMN_PLATFORM);
                Config.SHOW_COLUMN_ARCHITECTURE = EditorGUILayout.Toggle(new GUIContent("Column: Arch", "Enable or disable the column 'Arch' in the 'Switch'-aboutTab (default: " + Constants.DEFAULT_SHOW_COLUMN_ARCHITECTURE + ")."), Config.SHOW_COLUMN_ARCHITECTURE);
                Config.SHOW_COLUMN_TEXTURE = EditorGUILayout.Toggle(new GUIContent("Column: Texture", "Enable or disable the column 'Texture' in the 'Switch'-aboutTab (default: " + Constants.DEFAULT_SHOW_COLUMN_TEXTURE + ")."), Config.SHOW_COLUMN_TEXTURE);
                Config.SHOW_COLUMN_CACHE = EditorGUILayout.Toggle(new GUIContent("Column: Cache", "Enable or disable the column 'Cache' in the 'Switch'-aboutTab (default: " + Constants.DEFAULT_SHOW_COLUMN_CACHE + ")."), Config.SHOW_COLUMN_CACHE);

                Helper.SeparatorUI();

                GUILayout.Label("Active Platforms", EditorStyles.boldLabel);

                //scrollPosPlatforms = EditorGUILayout.BeginScrollView(scrollPosPlatforms, false, false);
                Config.PLATFORM_WINDOWS = EditorGUILayout.Toggle(new GUIContent("Windows", "Enable or disable the support for the Windows platform (default: " + Constants.DEFAULT_PLATFORM_WINDOWS + ")."), Config.PLATFORM_WINDOWS);
                Config.PLATFORM_MAC = EditorGUILayout.Toggle(new GUIContent("Mac", "Enable or disable the support for the macOS platform (default: " + Constants.DEFAULT_PLATFORM_MAC + ")."), Config.PLATFORM_MAC);
                Config.PLATFORM_LINUX = EditorGUILayout.Toggle(new GUIContent("Linux", "Enable or disable the support for the Linux platform (default: " + Constants.DEFAULT_PLATFORM_LINUX + ")."), Config.PLATFORM_LINUX);
                Config.PLATFORM_ANDROID = EditorGUILayout.Toggle(new GUIContent("Android", "Enable or disable the support for the Android platform (default: " + Constants.DEFAULT_PLATFORM_ANDROID + ")."), Config.PLATFORM_ANDROID);
                Config.PLATFORM_IOS = EditorGUILayout.Toggle(new GUIContent("iOS", "Enable or disable the support for the iOS platform (default: " + Constants.DEFAULT_PLATFORM_IOS + ")."), Config.PLATFORM_IOS);

#if UNITY_5 || UNITY_2017 || UNITY_2018
                Config.PLATFORM_WSA = EditorGUILayout.Toggle(new GUIContent("WSA", "Enable or disable the support for the WSA platform (default: " + Constants.DEFAULT_PLATFORM_WSA + ")."), Config.PLATFORM_WSA);
#endif

#if UNITY_4_6 || UNITY_4_7 || UNITY_5_0 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3
                Config.PLATFORM_WEBPLAYER = EditorGUILayout.Toggle(new GUIContent("WebPlayer", "Enable or disable the support for the WebPlayer platform (default: " + Constants.DEFAULT_PLATFORM_WEBPLAYER + ")."), Config.PLATFORM_WEBPLAYER);
#endif

#if UNITY_5 || UNITY_2017 || UNITY_2018
                Config.PLATFORM_WEBGL = EditorGUILayout.Toggle(new GUIContent("WebGL", "Enable or disable the support for the WebGL platform (default: " + Constants.DEFAULT_PLATFORM_WEBGL + ")."), Config.PLATFORM_WEBGL);
#endif

#if UNITY_5_3 || UNITY_5_3_OR_NEWER
                Config.PLATFORM_TVOS = EditorGUILayout.Toggle(new GUIContent("tvOS", "Enable or disable the support for the tvOS platform (default: " + Constants.DEFAULT_PLATFORM_TVOS + ")."), Config.PLATFORM_TVOS);
#endif

#if UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_3_OR_NEWER
                Config.PLATFORM_TIZEN = EditorGUILayout.Toggle(new GUIContent("Tizen", "Enable or disable the support for the Tizen platform (default: " + Constants.DEFAULT_PLATFORM_TIZEN + ")."), Config.PLATFORM_TIZEN);
#endif

#if !UNITY_2017_3_OR_NEWER
                Config.PLATFORM_SAMSUNGTV = EditorGUILayout.Toggle(new GUIContent("SamsungTV", "Enable or disable the support for the SamsungTV platform (default: " + Constants.DEFAULT_PLATFORM_SAMSUNGTV + ")."), Config.PLATFORM_SAMSUNGTV);
#endif

#if !UNITY_5_5_OR_NEWER
                Config.PLATFORM_PS3 = EditorGUILayout.Toggle(new GUIContent("PS3", "Enable or disable the support for the Sony PS3 platform (default: " + Constants.DEFAULT_PLATFORM_PS3 + ")."), Config.PLATFORM_PS3);
#endif

                Config.PLATFORM_PS4 = EditorGUILayout.Toggle(new GUIContent("PS4", "Enable or disable the support for the Sony PS4 platform (default: " + Constants.DEFAULT_PLATFORM_PS4 + ")."), Config.PLATFORM_PS4);
                Config.PLATFORM_PSP2 = EditorGUILayout.Toggle(new GUIContent("PSP2 (Vita)", "Enable or disable the support for the Sony PSP2 (Vita) platform (default: " + Constants.DEFAULT_PLATFORM_PSP2 + ")."), Config.PLATFORM_PSP2);

#if !UNITY_5_5_OR_NEWER
                Config.PLATFORM_XBOX360 = EditorGUILayout.Toggle(new GUIContent("XBox360", "Enable or disable the support for the Microsoft XBox360 platform (default: " + Constants.DEFAULT_PLATFORM_XBOX360 + ")."), Config.PLATFORM_XBOX360);
#endif

                Config.PLATFORM_XBOXONE = EditorGUILayout.Toggle(new GUIContent("XBoxOne", "Enable or disable the support for the Microsoft XBoxOne platform (default: " + Constants.DEFAULT_PLATFORM_XBOXONE + ")."), Config.PLATFORM_XBOXONE);

#if UNITY_5_2 || UNITY_5_3 || UNITY_5_3_OR_NEWER
                Config.PLATFORM_WIIU = EditorGUILayout.Toggle(new GUIContent("WiiU", "Enable or disable the support for the Nintendo WiiU platform (default: " + Constants.DEFAULT_PLATFORM_WIIU + ")."), Config.PLATFORM_WIIU);
#endif

#if UNITY_5_2 || UNITY_5_3 || UNITY_5_3_OR_NEWER
                Config.PLATFORM_3DS = EditorGUILayout.Toggle(new GUIContent("3DS", "Enable or disable the support for the Nintendo 3DS platform (default: " + Constants.DEFAULT_PLATFORM_3DS + ")."), Config.PLATFORM_3DS);
#endif

#if UNITY_5_6_OR_NEWER
            Config.PLATFORM_SWITCH = EditorGUILayout.Toggle(new GUIContent("Switch", "Enable or disable the support for the Nintendo Switch platform (default: " + Constants.DEFAULT_PLATFORM_SWITCH + ")."), Config.PLATFORM_SWITCH);
#endif
            }
            EditorGUILayout.EndScrollView();

            Helper.SeparatorUI();

            EditorGUILayout.BeginHorizontal();
            {
                if (GUILayout.Button(new GUIContent(" Delete Cache", Helper.Icon_Delete, "Delete the complete cache")))
                {
                    if (EditorUtility.DisplayDialog("Delete the complete cache?", "If you delete the complete cache, Unity must re-import all assets for every platform switch." + System.Environment.NewLine + "This operation could take some time." + System.Environment.NewLine + System.Environment.NewLine + "Would you like to delete the cache?", "Yes", "No"))
                    {
                        if (Config.DEBUG)
                            Debug.Log("Complete cache deleted");

                        Helper.DeleteCache();
                        GAApi.Event(typeof(ConfigBase).Name, "Delete complete cache");
                    }
                }

                if (GUILayout.Button(new GUIContent(" Reset", Helper.Icon_Reset, "Resets the configuration settings for this project.")))
                {
                    if (EditorUtility.DisplayDialog("Reset configuration?", "Reset the configuration of " + Constants.ASSET_NAME + "?", "Yes", "No"))
                    {
                        Config.Reset();
                        save();

                        GAApi.Event(typeof(ConfigPreferences).Name, "Reset configuration");
                    }
                }
            }
            EditorGUILayout.EndHorizontal();

            GUILayout.Space(6);
        }

        protected void showHelp()
        {
            scrollPosHelp = EditorGUILayout.BeginScrollView(scrollPosHelp, false, false);
            {
                GUILayout.Label("Resources", EditorStyles.boldLabel);

                //GUILayout.Space(8);

                GUILayout.BeginHorizontal();
                {
                    GUILayout.BeginVertical();
                    {

                        if (GUILayout.Button(new GUIContent(" Manual", Helper.Icon_Manual, "Show the manual.")))
                        {
                            Application.OpenURL(Constants.ASSET_MANUAL_URL);
                            GAApi.Event(typeof(ConfigBase).Name, "ASSET_MANUAL_URL");
                        }

                        GUILayout.Space(6);

                        if (GUILayout.Button(new GUIContent(" Forum", Helper.Icon_Forum, "Visit the forum page.")))
                        {
                            Application.OpenURL(Constants.ASSET_FORUM_URL);
                            GAApi.Event(typeof(ConfigBase).Name, "ASSET_FORUM_URL");
                        }
                    }
                    GUILayout.EndVertical();

                    GUILayout.BeginVertical();
                    {

                        if (GUILayout.Button(new GUIContent(" API", Helper.Icon_API, "Show the API.")))
                        {
                            Application.OpenURL(Constants.ASSET_API_URL);
                            GAApi.Event(typeof(ConfigBase).Name, "ASSET_API_URL");
                        }

                        GUILayout.Space(6);

                        if (GUILayout.Button(new GUIContent(" Product", Helper.Icon_Product, "Visit the product page.")))
                        {
                            Application.OpenURL(Constants.ASSET_WEB_URL);
                            GAApi.Event(typeof(ConfigBase).Name, "ASSET_WEB_URL");
                        }
                    }
                    GUILayout.EndVertical();

                }
                GUILayout.EndHorizontal();

                Helper.SeparatorUI();

                GUILayout.Label("Videos", EditorStyles.boldLabel);

                GUILayout.BeginHorizontal();
                {
                    if (GUILayout.Button(new GUIContent(" Promo", Helper.Video_Promo, "View the promotion video on 'Youtube'.")))
                    {
                        Application.OpenURL(Constants.ASSET_VIDEO_PROMO);
                        GAApi.Event(typeof(ConfigBase).Name, "ASSET_VIDEO_PROMO");
                    }

                    if (GUILayout.Button(new GUIContent(" Tutorial", Helper.Video_Tutorial, "View the tutorial video on 'Youtube'.")))
                    {
                        Application.OpenURL(Constants.ASSET_VIDEO_TUTORIAL);
                        GAApi.Event(typeof(ConfigBase).Name, "ASSET_VIDEO_TUTORIAL");
                    }
                }
                GUILayout.EndHorizontal();

                GUILayout.Space(6);

                if (GUILayout.Button(new GUIContent(" All Videos", Helper.Icon_Videos, "Visit our 'Youtube'-channel for more videos.")))
                {
                    Application.OpenURL(Constants.ASSET_SOCIAL_YOUTUBE);
                    GAApi.Event(typeof(ConfigBase).Name, "ASSET_SOCIAL_YOUTUBE");
                }
            }
            EditorGUILayout.EndScrollView();

            GUILayout.Space(6);
        }

        protected void showAbout()
        {
            GUILayout.Label(Constants.ASSET_NAME, EditorStyles.boldLabel);

            GUILayout.BeginHorizontal();
            {
                GUILayout.BeginVertical(GUILayout.Width(60));
                {
                    GUILayout.Label("Version:");

                    GUILayout.Space(12);

                    GUILayout.Label("Web:");

                    GUILayout.Space(2);

                    GUILayout.Label("Email:");

                }
                GUILayout.EndVertical();

                GUILayout.BeginVertical(GUILayout.Width(170));
                {
                    GUILayout.Space(0);

                    GUILayout.Label(Constants.ASSET_VERSION);

                    GUILayout.Space(12);

                    EditorGUILayout.SelectableLabel(Constants.ASSET_AUTHOR_URL, GUILayout.Height(16), GUILayout.ExpandHeight(false));

                    GUILayout.Space(2);

                    EditorGUILayout.SelectableLabel(Constants.ASSET_CONTACT, GUILayout.Height(16), GUILayout.ExpandHeight(false));
                }
                GUILayout.EndVertical();

                GUILayout.BeginVertical(GUILayout.ExpandWidth(true));
                {
                    //GUILayout.Space(0);
                }
                GUILayout.EndVertical();

                GUILayout.BeginVertical(GUILayout.Width(64));
                {
                    if (GUILayout.Button(new GUIContent(string.Empty, Helper.Logo_Asset, "Visit asset website")))
                    {
                        Application.OpenURL(Constants.ASSET_URL);
                        GAApi.Event(typeof(ConfigBase).Name, "ASSET_URL");
                    }

                    if (!Constants.isPro)
                    {
                        if (GUILayout.Button(new GUIContent(" Upgrade", "Upgrade " + Constants.ASSET_NAME + " to the PRO-version")))
                        {
                            Application.OpenURL(Constants.ASSET_PRO_URL);
                            GAApi.Event(typeof(ConfigBase).Name, "ASSET_PRO_URL");
                        }
                    }
                }
                GUILayout.EndVertical();
            }
            GUILayout.EndHorizontal();

            GUILayout.Label("© 2016-2018 by " + Constants.ASSET_AUTHOR);

            Helper.SeparatorUI();

            GUILayout.BeginHorizontal();
            {
                if (GUILayout.Button(new GUIContent(" AssetStore", Helper.Logo_Unity, "Visit the 'Unity AssetStore' website.")))
                {
                    Application.OpenURL(Constants.ASSET_CT_URL);
                    GAApi.Event(typeof(ConfigBase).Name, "ASSET_CT_URL");
                }

                if (GUILayout.Button(new GUIContent(" " + Constants.ASSET_AUTHOR, Helper.Logo_CT, "Visit the '" + Constants.ASSET_AUTHOR + "' website.")))
                {
                    Application.OpenURL(Constants.ASSET_AUTHOR_URL);
                    GAApi.Event(typeof(ConfigBase).Name, "ASSET_AUTHOR_URL");
                }
            }
            GUILayout.EndHorizontal();

            Helper.SeparatorUI();

            aboutTab = GUILayout.Toolbar(aboutTab, new string[] { "Readme", "Versions", "Update" });

            if (aboutTab == 2)
            {
                scrollPosAboutUpdate = EditorGUILayout.BeginScrollView(scrollPosAboutUpdate, false, false);
                {
                    Color fgColor = GUI.color;

                    GUI.color = Color.yellow;

                    if (updateStatus == UpdateStatus.NO_UPDATE)
                    {
                        GUI.color = Color.green;
                        GUILayout.Label(updateText);
                    }
                    else if (updateStatus == UpdateStatus.UPDATE)
                    {
                        GUILayout.Label(updateText);

                        if (GUILayout.Button(new GUIContent(" Download", "Visit the 'Unity AssetStore' to download the latest version.")))
                        {
                            Application.OpenURL(Constants.ASSET_URL);
                            GAApi.Event(typeof(ConfigBase).Name, "UPDATE");
                        }
                    }
                    else if (updateStatus == UpdateStatus.UPDATE_PRO)
                    {
                        GUILayout.Label(updateText);

                        if (GUILayout.Button(new GUIContent(" Upgrade", "Upgrade to the PRO-version in the 'Unity AssetStore'.")))
                        {
                            Application.OpenURL(Util.Constants.ASSET_PRO_URL);
                            GAApi.Event(typeof(ConfigBase).Name, "UPGRADE PRO");
                        }
                    }
                    else if (updateStatus == UpdateStatus.UPDATE_VERSION)
                    {
                        GUILayout.Label(updateText);

                        if (GUILayout.Button(new GUIContent(" Upgrade", "Upgrade to the newer version in the 'Unity AssetStore'")))
                        {
                            Application.OpenURL(Util.Constants.ASSET_CT_URL);
                            GAApi.Event(typeof(ConfigBase).Name, "UPGRADE");
                        }
                    }
                    else if (updateStatus == UpdateStatus.DEPRECATED)
                    {
                        GUILayout.Label(updateText);

                        if (GUILayout.Button(new GUIContent(" More Information", "Visit the 'crosstales'-site for more information.")))
                        {
                            Application.OpenURL(Util.Constants.ASSET_AUTHOR_URL);
                            GAApi.Event(typeof(ConfigBase).Name, "DEPRECATED");
                        }
                    }
                    else
                    {
                        GUI.color = Color.cyan;
                        GUILayout.Label(updateText);
                    }

                    GUI.color = fgColor;
                }
                EditorGUILayout.EndScrollView();

                if (updateStatus == UpdateStatus.NOT_CHECKED || updateStatus == UpdateStatus.NO_UPDATE)
                {
                    bool isChecking = !(worker == null || (worker != null && !worker.IsAlive));

                    GUI.enabled = Util.Helper.isInternetAvailable && !isChecking;

                    if (GUILayout.Button(new GUIContent(isChecking ? "Checking... Please wait." : " Check For Update", Helper.Icon_Check, "Checks for available updates of " + Util.Constants.ASSET_NAME)))
                    {
                        worker = new System.Threading.Thread(() => UpdateCheck.UpdateCheckForEditor(out updateText, out updateStatus));
                        worker.Start();

                        GAApi.Event(typeof(ConfigBase).Name, "UpdateCheck");
                    }

                    GUI.enabled = true;
                }
            }
            else if (aboutTab == 0)
            {
                if (readme == null)
                {
                    string path = Application.dataPath + Config.ASSET_PATH + "README.txt";

                    try
                    {
                        readme = System.IO.File.ReadAllText(path);
                    }
                    catch (System.Exception)
                    {
                        readme = "README not found: " + path;
                    }
                }

                scrollPosAboutReadme = EditorGUILayout.BeginScrollView(scrollPosAboutReadme, false, false);
                {
                    GUILayout.Label(readme);
                }
                EditorGUILayout.EndScrollView();
            }
            else
            {
                if (versions == null)
                {
                    string path = Application.dataPath + Config.ASSET_PATH + "Documentation/VERSIONS.txt";

                    try
                    {
                        versions = System.IO.File.ReadAllText(path);
                    }
                    catch (System.Exception)
                    {
                        versions = "VERSIONS not found: " + path;
                    }
                }

                scrollPosAboutVersions = EditorGUILayout.BeginScrollView(scrollPosAboutVersions, false, false);
                {
                    GUILayout.Label(versions);
                }

                EditorGUILayout.EndScrollView();
            }

            Helper.SeparatorUI();

            GUILayout.BeginHorizontal();
            {
                if (GUILayout.Button(new GUIContent(string.Empty, Helper.Social_Facebook, "Follow us on 'Facebook'.")))
                {
                    Application.OpenURL(Constants.ASSET_SOCIAL_FACEBOOK);
                    GAApi.Event(typeof(ConfigBase).Name, "ASSET_SOCIAL_FACEBOOK");
                }

                if (GUILayout.Button(new GUIContent(string.Empty, Helper.Social_Twitter, "Follow us on 'Twitter'.")))
                {
                    Application.OpenURL(Constants.ASSET_SOCIAL_TWITTER);
                    GAApi.Event(typeof(ConfigBase).Name, "ASSET_SOCIAL_TWITTER");
                }

                if (GUILayout.Button(new GUIContent(string.Empty, Helper.Social_Linkedin, "Follow us on 'LinkedIn'.")))
                {
                    Application.OpenURL(Constants.ASSET_SOCIAL_LINKEDIN);
                    GAApi.Event(typeof(ConfigBase).Name, "ASSET_SOCIAL_LINKEDIN");
                }

                if (GUILayout.Button(new GUIContent(string.Empty, Helper.Social_Xing, "Follow us on 'XING'.")))
                {
                    Application.OpenURL(Constants.ASSET_SOCIAL_XING);
                    GAApi.Event(typeof(ConfigBase).Name, "ASSET_SOCIAL_XING");
                }
            }
            GUILayout.EndHorizontal();

            GUILayout.Space(6);
        }

        protected static void save()
        {
            Config.Save();

            if (Config.DEBUG)
                Debug.Log("Config data saved");
        }

        #endregion


        #region Private methods

        private static void drawColumnZebra()
        {
            if (Config.PLATFORM_WINDOWS)
            {
                drawZebra(targetWindows);
            }

            if (Config.PLATFORM_MAC)
            {
                drawZebra(targetMac);
            }

            if (Config.PLATFORM_LINUX)
            {
                drawZebra(targetLinux);
            }

            if (Config.PLATFORM_ANDROID)
            {
                drawZebra(BuildTarget.Android);
            }

            if (Config.PLATFORM_IOS)
            {
#if UNITY_5 || UNITY_2017 || UNITY_2018
                drawZebra(BuildTarget.iOS);
#else
                drawZebra(BuildTarget.iPhone);
#endif
            }

#if UNITY_5 || UNITY_2017 || UNITY_2018
            if (Config.PLATFORM_WSA)
            {
                drawZebra(BuildTarget.WSAPlayer);
            }
#endif

#if UNITY_4_6 || UNITY_4_7 || UNITY_5_0 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3
            if (Config.PLATFORM_WEBPLAYER)
            {
                drawZebra(BuildTarget.WebPlayer);
            }
#endif

#if UNITY_5 || UNITY_2017 || UNITY_2018
            if (Config.PLATFORM_WEBGL)
            {
                drawZebra(BuildTarget.WebGL);
            }
#endif

#if UNITY_5_3 || UNITY_5_3_OR_NEWER
            if (Config.PLATFORM_TVOS)
            {
                drawZebra(BuildTarget.tvOS);
            }
#endif

#if !UNITY_2017_3_OR_NEWER
            if (Config.PLATFORM_SAMSUNGTV)
            {
                drawZebra(BuildTarget.SamsungTV);
            }
#endif

#if !UNITY_5_5_OR_NEWER
            if (Config.PLATFORM_PS3)
            {
                drawZebra(BuildTarget.PS3);
            }
#endif

            if (Config.PLATFORM_PS4)
            {
                drawZebra(BuildTarget.PS4);
            }

#if !UNITY_5_5_OR_NEWER
            if (Config.PLATFORM_XBOX360)
            {
                drawZebra(BuildTarget.XBOX360);
            }
#endif

            if (Config.PLATFORM_XBOXONE)
            {
                drawZebra(BuildTarget.XboxOne);
            }

#if UNITY_5_6_OR_NEWER
            if (Config.PLATFORM_SWITCH)
            {
                drawZebra(BuildTarget.Switch);
            }
#endif

        }

        private static void drawColumnLogo()
        {
            GUILayout.BeginVertical(GUILayout.Width(logoWidth));
            {
                if (Config.PLATFORM_WINDOWS)
                {
                    drawLogo(Helper.Logo_Windows);
                }

                if (Config.PLATFORM_MAC)
                {
                    drawLogo(Helper.Logo_Mac);
                }

                if (Config.PLATFORM_LINUX)
                {
                    drawLogo(Helper.Logo_Linux);
                }

                if (Config.PLATFORM_ANDROID)
                {
                    drawLogo(Helper.Logo_Android);
                }

                if (Config.PLATFORM_IOS)
                {
                    drawLogo(Helper.Logo_Ios);
                }

#if UNITY_5 || UNITY_2017 || UNITY_2018
                if (Config.PLATFORM_WSA)
                {
                    drawLogo(Helper.Logo_Wsa);
                }
#endif

#if UNITY_4_6 || UNITY_4_7 || UNITY_5_0 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3
                if (Config.PLATFORM_WEBPLAYER)
                {
                    drawLogo(Helper.Logo_Webplayer);
                }
#endif

#if UNITY_5 || UNITY_2017 || UNITY_2018
                if (Config.PLATFORM_WEBGL)
                {
                    drawLogo(Helper.Logo_Webgl);
                }
#endif

#if UNITY_5_3 || UNITY_5_3_OR_NEWER
                if (Config.PLATFORM_TVOS)
                {
                    drawLogo(Helper.Logo_Tvos);
                }
#endif

#if UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_3_OR_NEWER
                if (Config.PLATFORM_TIZEN)
                {
                    drawLogo(Helper.Logo_Tizen);
                }
#endif

#if !UNITY_2017_3_OR_NEWER
                if (Config.PLATFORM_SAMSUNGTV)
                {
                    drawLogo(Helper.Logo_Samsungtv);
                }
#endif

#if !UNITY_5_5_OR_NEWER
                if (Config.PLATFORM_PS3)
                {
                    drawLogo(Helper.Logo_Ps3);
                }
#endif

                if (Config.PLATFORM_PS4)
                {
                    drawLogo(Helper.Logo_Ps4);
                }

                if (Config.PLATFORM_PSP2)
                {
                    drawLogo(Helper.Logo_Psp);
                }

#if !UNITY_5_5_OR_NEWER
                if (Config.PLATFORM_XBOX360)
                {
                    drawLogo(Helper.Logo_Xbox360);
                }
#endif

                if (Config.PLATFORM_XBOXONE)
                {
                    drawLogo(Helper.Logo_Xboxone);
                }

#if UNITY_5_2 || UNITY_5_3 || UNITY_5_3_OR_NEWER
                if (Config.PLATFORM_WIIU)
                {
                    drawLogo(Helper.Logo_Wiiu);
                }
#endif

#if UNITY_5_2 || UNITY_5_3 || UNITY_5_3_OR_NEWER
                if (Config.PLATFORM_3DS)
                {
                    drawLogo(Helper.Logo_3ds);
                }
#endif

#if UNITY_5_6_OR_NEWER
                if (Config.PLATFORM_SWITCH)
                {
                    drawLogo(Helper.Logo_Switch);
                }
#endif

            }
            GUILayout.EndVertical();
        }

        private static void drawColumnPlatform()
        {
            GUILayout.BeginVertical(GUILayout.Width(platformWidth));
            {
                if (Config.PLATFORM_WINDOWS)
                {
                    drawPlatform("Standalone Windows");
                }

                if (Config.PLATFORM_MAC)
                {
                    drawPlatform("Standalone Mac");
                }

                if (Config.PLATFORM_LINUX)
                {
                    drawPlatform("Standalone Linux");
                }

                if (Config.PLATFORM_ANDROID)
                {
                    drawPlatform("Android");
                }

                if (Config.PLATFORM_IOS)
                {
                    drawPlatform("iOS");
                }

#if UNITY_5 || UNITY_2017 || UNITY_2018
                if (Config.PLATFORM_WSA)
                {
                    drawPlatform("WSA");
                }
#endif

#if UNITY_4_6 || UNITY_4_7 || UNITY_5_0 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3
                if (Config.PLATFORM_WEBPLAYER)
                {
                    drawPlatform("WebPlayer");
                }
#endif

#if UNITY_5 || UNITY_2017 || UNITY_2018
                if (Config.PLATFORM_WEBGL)
                {
                    drawPlatform("WebGL");
                }
#endif

#if UNITY_5_3 || UNITY_5_3_OR_NEWER
                if (Config.PLATFORM_TVOS)
                {
                    drawPlatform("tvOS");
                }
#endif

#if UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_3_OR_NEWER
                if (Config.PLATFORM_TIZEN)
                {
                    drawPlatform("Tizen");
                }
#endif

#if !UNITY_2017_3_OR_NEWER
                if (Config.PLATFORM_SAMSUNGTV)
                {
                    drawPlatform("Samsung TV");
                }
#endif

#if !UNITY_5_5_OR_NEWER
                if (Config.PLATFORM_PS3)
                {
                    drawPlatform("PS3");
                }
#endif

                if (Config.PLATFORM_PS4)
                {
                    drawPlatform("PS4");
                }

                if (Config.PLATFORM_PSP2)
                {
                    drawPlatform("PSP2 (Vita)");
                }

#if !UNITY_5_5_OR_NEWER
                if (Config.PLATFORM_XBOX360)
                {
                    drawPlatform("XBox360");
                }
#endif

                if (Config.PLATFORM_XBOXONE)
                {
                    drawPlatform("XBoxOne");
                }

#if UNITY_5_2 || UNITY_5_3 || UNITY_5_3_OR_NEWER
                if (Config.PLATFORM_WIIU)
                {
                    drawPlatform("WiiU");
                }
#endif

#if UNITY_5_2 || UNITY_5_3 || UNITY_5_3_OR_NEWER
                if (Config.PLATFORM_3DS)
                {
                    drawPlatform("3DS");
                }
#endif

#if UNITY_5_6_OR_NEWER
                if (Config.PLATFORM_SWITCH)
                {
                    drawPlatform("Switch");
                }
#endif
            }
            GUILayout.EndVertical();
        }

        private static void drawColumnArchitecture()
        {
            GUILayout.BeginVertical(GUILayout.Width(architectureWidth));
            {
                int heightSpace = 12;


                if (Config.PLATFORM_WINDOWS)
                {
                    GUILayout.Space(heightSpace);
                    Config.ARCH_WINDOWS = EditorGUILayout.Popup("", Config.ARCH_WINDOWS, archWinOptions, new GUILayoutOption[] { GUILayout.Width(architectureWidth - 10) });
                    heightSpace = 18;
                }

                if (Config.PLATFORM_MAC)
                {
                    GUILayout.Space(heightSpace);
                    Config.ARCH_MAC = EditorGUILayout.Popup("", Config.ARCH_MAC, archMacOptions, new GUILayoutOption[] { GUILayout.Width(architectureWidth - 10) });
                    heightSpace = 18;
                }

                if (Config.PLATFORM_LINUX)
                {
                    GUILayout.Space(heightSpace);
                    Config.ARCH_LINUX = EditorGUILayout.Popup("", Config.ARCH_LINUX, archLinuxOptions, new GUILayoutOption[] { GUILayout.Width(architectureWidth - 10) });
                    heightSpace = 18;
                }
            }
            GUILayout.EndVertical();
        }

        private static void drawColumnTexture()
        {
            GUILayout.BeginVertical(GUILayout.Width(textureWidth));
            {
                int heightSpace = 12;

                if (Config.PLATFORM_WINDOWS)
                {
                    GUILayout.Space(heightSpace);
                    heightSpace = 35;
                }

                if (Config.PLATFORM_MAC)
                {
                    GUILayout.Space(heightSpace);
                    heightSpace = 35;
                }

                if (Config.PLATFORM_LINUX)
                {
                    GUILayout.Space(heightSpace);
                    heightSpace = 35;
                }

                if (Config.PLATFORM_ANDROID)
                {
                    GUILayout.Space(heightSpace);
                    Config.TEX_ANDROID = EditorGUILayout.Popup("", Config.TEX_ANDROID, texAndroidOptions, new GUILayoutOption[] { GUILayout.Width(textureWidth - 10) });
                    heightSpace = 18;
                }
            }
            GUILayout.EndVertical();
        }

        private static void drawColumnCached()
        {
            GUILayout.BeginVertical(GUILayout.Width(cacheWidth));
            {
                if (Config.PLATFORM_WINDOWS)
                {
                    drawCached(targetWindows);
                }

                if (Config.PLATFORM_MAC)
                {
                    drawCached(targetMac);
                }

                if (Config.PLATFORM_LINUX)
                {
                    drawCached(targetLinux);
                }

                if (Config.PLATFORM_ANDROID)
                {
                    drawCached(BuildTarget.Android);
                }

                if (Config.PLATFORM_IOS)
                {
#if UNITY_5 || UNITY_2017 || UNITY_2018
                    drawCached(BuildTarget.iOS);
#else
                    drawCached(BuildTarget.iPhone);
#endif
                }

#if UNITY_5 || UNITY_2017 || UNITY_2018
                if (Config.PLATFORM_WSA)
                {
                    drawCached(BuildTarget.WSAPlayer);
                }
#endif

#if UNITY_4_6 || UNITY_4_7 || UNITY_5_0 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3
                if (Config.PLATFORM_WEBPLAYER)
                {
                    drawCached(BuildTarget.WebPlayer);
                }
#endif

#if UNITY_5 || UNITY_2017 || UNITY_2018
                if (Config.PLATFORM_WEBGL)
                {
                    drawCached(BuildTarget.WebGL);
                }
#endif

#if UNITY_5_3 || UNITY_5_3_OR_NEWER
                if (Config.PLATFORM_TVOS)
                {
                    drawCached(BuildTarget.tvOS);
                }
#endif

#if !UNITY_2017_3_OR_NEWER
                if (Config.PLATFORM_SAMSUNGTV)
                {
                    drawCached(BuildTarget.SamsungTV);
                }
#endif

#if !UNITY_5_5_OR_NEWER
                if (Config.PLATFORM_PS3)
                {
                    drawCached(BuildTarget.PS3);
                }
#endif

                if (Config.PLATFORM_PS4)
                {
                    drawCached(BuildTarget.PS4);
                }

#if !UNITY_5_5_OR_NEWER
                if (Config.PLATFORM_XBOX360)
                {
                    drawCached(BuildTarget.XBOX360);
                }
#endif

                if (Config.PLATFORM_XBOXONE)
                {
                    drawCached(BuildTarget.XboxOne);
                }

#if UNITY_5_6_OR_NEWER
                if (Config.PLATFORM_SWITCH)
                {
                    drawCached(BuildTarget.Switch);
                }
#endif

            }
            GUILayout.EndVertical();
        }

        private static void drawColumnAction()
        {
            GUILayout.BeginVertical();
            {
                if (Config.PLATFORM_WINDOWS)
                {
                    drawAction(targetWindows, buildWindows, Helper.Logo_Windows);
                }

                if (Config.PLATFORM_MAC)
                {
                    drawAction(targetMac, buildMac, Helper.Logo_Mac);
                }

                if (Config.PLATFORM_LINUX)
                {
                    drawAction(targetLinux, buildLinux, Helper.Logo_Linux);
                }

                if (Config.PLATFORM_ANDROID)
                {
#if UNITY_2017_2_OR_NEWER
                    drawAction(BuildTarget.Android, "Android", Helper.Logo_Android);
#else
                    drawAction(BuildTarget.Android, "android", Helper.Logo_Android);
#endif
                }

                if (Config.PLATFORM_IOS)
                {
#if UNITY_5 || UNITY_2017 || UNITY_2018
                    drawAction(BuildTarget.iOS, "iOS", Helper.Logo_Ios);
#else
                    drawAction(BuildTarget.iPhone, "iPhone", Helper.Logo_Ios);
#endif
                }

#if UNITY_5 || UNITY_2017 || UNITY_2018
                if (Config.PLATFORM_WSA)
                {
#if UNITY_2017_2_OR_NEWER
                    drawAction(BuildTarget.WSAPlayer, "WindowsStoreApps", Helper.Logo_Wsa);
#else
                    drawAction(BuildTarget.WSAPlayer, "wsaplayer", Helper.Logo_Wsa);
#endif
                }
#endif

#if UNITY_4_6 || UNITY_4_7 || UNITY_5_0 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3
                if (Config.PLATFORM_WEBPLAYER)
                {
                    drawAction(BuildTarget.WebPlayer, "webplayer", Helper.Logo_Webplayer);
                }
#endif

#if UNITY_5 || UNITY_2017 || UNITY_2018
                if (Config.PLATFORM_WEBGL)
                {
#if UNITY_2017_2_OR_NEWER
                    drawAction(BuildTarget.WebGL, "WebGL", Helper.Logo_Webgl);
#else
                    drawAction(BuildTarget.WebGL, "webgl", Helper.Logo_Webgl);
#endif
                }
#endif

#if UNITY_5_3 || UNITY_5_3_OR_NEWER
                if (Config.PLATFORM_TVOS)
                {
                    drawAction(BuildTarget.tvOS, "tvOS", Helper.Logo_Tvos);
                }
#endif

#if !UNITY_2017_3_OR_NEWER
                if (Config.PLATFORM_SAMSUNGTV)
                {
                    drawAction(BuildTarget.SamsungTV, "samsungtv", Helper.Logo_Samsungtv);
                }
#endif

#if !UNITY_5_5_OR_NEWER
                if (Config.PLATFORM_PS3)
                {
                    drawAction(BuildTarget.PS3, "ps3", Helper.Logo_Ps3);
                }
#endif

                if (Config.PLATFORM_PS4)
                {
#if UNITY_2017_2_OR_NEWER
                    drawAction(BuildTarget.PS4, "PS4", Helper.Logo_Ps4);
#else
                    drawAction(BuildTarget.PS4, "ps4", Helper.Logo_Ps4);
#endif
                }

#if !UNITY_5_5_OR_NEWER
                if (Config.PLATFORM_XBOX360)
                {
                    drawAction(BuildTarget.XBOX360, "xbox360", Helper.Logo_Xbox360);
                }
#endif

                if (Config.PLATFORM_XBOXONE)
                {
#if UNITY_2017_2_OR_NEWER
                    drawAction(BuildTarget.XboxOne, "XboxOne", Helper.Logo_Xboxone);
#else
                    drawAction(BuildTarget.XboxOne, "xboxone", Helper.Logo_Xboxone);
#endif
                }

#if UNITY_5_6_OR_NEWER
                if (Config.PLATFORM_SWITCH)
                {
#if UNITY_2017_2_OR_NEWER
                    drawAction(BuildTarget.Switch, "Switch", Helper.Logo_Switch);
#else
                    drawAction(BuildTarget.Switch, "switch", Helper.Logo_Switch);
#endif
                }
#endif

            }
            GUILayout.EndVertical();
        }

        private static void drawVerticalSeparator(bool title = false)
        {
            GUILayout.BeginVertical(GUILayout.Width(2));
            {
                if (title)
                {
                    GUILayout.Box(string.Empty, new GUILayoutOption[] { /*GUILayout.ExpandHeight(true)*/ GUILayout.Height(24), GUILayout.Width(1) });
                }
                else
                {
                    GUILayout.Box(string.Empty, new GUILayoutOption[] { GUILayout.Height(platformY + rowHeight - 4), GUILayout.Width(1) });
                }
            }
            GUILayout.EndVertical();
        }

        private static void drawZebra(BuildTarget target)
        {
            platformY += rowHeight;
            rowHeight = 36;

            if (EditorUserBuildSettings.activeBuildTarget == target)
            {
                Color currentPlatform = new Color(0f, 0.33f, 0.71f); //CT-blue

                if (target == BuildTarget.Android)
                {
                    if (EditorUserBuildSettings.androidBuildSubtarget == texAndroid)
                    {
                        EditorGUI.DrawRect(new Rect(platformX, platformY, rowWidth, rowHeight), currentPlatform);
                    }
                    else
                    {
                        if (rowCounter % 2 == 0)
                        {
                            EditorGUI.DrawRect(new Rect(platformX, platformY, rowWidth, rowHeight), Color.gray);
                        }
                    }
                }
                else
                {
                    EditorGUI.DrawRect(new Rect(platformX, platformY, rowWidth, rowHeight), currentPlatform);
                }
            }
            else
            {

                if (rowCounter % 2 == 0)
                {
                    EditorGUI.DrawRect(new Rect(platformX, platformY, rowWidth, rowHeight), Color.gray);
                }
            }

            rowCounter++;
        }

        private static void drawLogo(Texture2D logo)
        {
            platformY += rowHeight;
            rowHeight = 36;

            GUILayout.Label(string.Empty);

            GUI.DrawTexture(new Rect(platformX + 4, platformY + 4, 28, 28), logo);
        }


        private static void drawPlatform(string label)
        {
            GUILayout.Space(platformTextSpace);
            GUILayout.Label(label /*, EditorStyles.boldLabel */);

            platformTextSpace = 18;
        }

        private static void drawCached(BuildTarget target)
        {
            GUILayout.Space(cacheTextSpace);

            if (Helper.isCached(target, texAndroid))
            {
                //GUILayout.Label(Helper.Icon_Cachefull);
                //GUILayout.Label(new GUIContent (" x", Helper.Icon_Cachefull, "Cached"));
                GUILayout.Label(new GUIContent(string.Empty, Helper.Icon_Cachefull, "Cached: " + target + System.Environment.NewLine + Helper.ScanCache(target, texAndroid)));
            }
            else
            {
                //GUILayout.Label(Helper.Icon_Cacheempty);
                //GUILayout.Label(new GUIContent (" -", Helper.Icon_Cacheempty));
                GUILayout.Label(new GUIContent(string.Empty, Helper.Icon_Cacheempty, "Not cached: " + target));
            }

            cacheTextSpace = 11;
        }

        private static void drawAction(BuildTarget target, string build, Texture2D logo)
        {
            GUILayout.Space(actionTextSpace);

            GUILayout.BeginHorizontal();
            {
                if (EditorUserBuildSettings.activeBuildTarget == target)
                {
                    if (target == BuildTarget.Android)
                    {
                        GUI.enabled = EditorUserBuildSettings.androidBuildSubtarget != texAndroid;
                    }
                    else
                    {
                        GUI.enabled = false;
                    }
                }


                if (GUILayout.Button(new GUIContent(string.Empty, logo, " Switch to " + target)))
                {
                    //if (GUILayout.Button (new GUIContent (" Switch", logo, "Switch to " + build))) {
                    if (!Config.CONFIRM_SWITCH || EditorUtility.DisplayDialog("Switch to " + target + "?", Constants.ASSET_NAME + " will now close Unity, save and restore the necessary files and then restart Unity." + System.Environment.NewLine + "This operation could take some time." + System.Environment.NewLine + System.Environment.NewLine + "Would you like to switch the platform?", "Yes, switch to " + target, "Cancel"))
                    {
                        if (Config.DEBUG)
                            Debug.Log("Switch initiated: " + target);

                        save();

                        //GAApi.Event(typeof(ConfigBase).Name, "Switch to " + target);

                        if (target == BuildTarget.Android)
                        {

                            if (Helper.isCached(target, texAndroid))
                            {
                                Helper.SwitchPlatform(target, build, texAndroid);
                            }
                            else
                            {
                                EditorUserBuildSettings.androidBuildSubtarget = texAndroid;

                                Helper.SwitchPlatform(target, build, texAndroid);
                            }
                        }
                        else
                        {
#if UNITY_5 || UNITY_2017 || UNITY_2018
                            Helper.SwitchPlatform(target, build, MobileTextureSubtarget.Generic);
#else
                        Helper.SwitchPlatform(target, build, AndroidBuildSubtarget.Generic);
#endif

                        }
                    }
                }
                GUI.enabled = true;

                if (Config.SHOW_DELETE && Helper.isCached(target, texAndroid))
                {
                    if (GUILayout.Button(new GUIContent(string.Empty, Helper.Icon_Delete_Big, "Delete cache from " + target)))
                    {
                        //if (GUILayout.Button (new GUIContent ("Delete", Helper.Icon_Delete, "Delete cache from " + build))) {
                        if (EditorUtility.DisplayDialog("Delete the cache for " + target + "?", "If you delete the cache, Unity must re-import all assets for this platform after a switch." + System.Environment.NewLine + "This operation could take some time." + System.Environment.NewLine + System.Environment.NewLine + "Would you like to delete the cache?", "Yes", "No"))
                        {
                            if (Config.DEBUG)
                                Debug.Log("Cache deleted: " + target);

                            Helper.DeleteCacheFromTarget(target, texAndroid);

                            GAApi.Event(typeof(ConfigBase).Name, "Delete cache from " + target);
                        }
                    }
                }
            }
            GUILayout.EndHorizontal();

            actionTextSpace = 8;
        }

        private static void drawHeader()
        {
            GUILayout.Space(8);
            GUILayout.BeginHorizontal();
            {
                if (Config.SHOW_COLUMN_PLATFORM)
                {
                    GUILayout.BeginVertical(GUILayout.Width(platformWidth + (Config.SHOW_COLUMN_PLATFORM_LOGO ? logoWidth + 4 : 0)));
                    {
                        GUILayout.Label(new GUIContent("Platform", "Platform name"), EditorStyles.boldLabel);
                    }
                    GUILayout.EndVertical();

                    drawVerticalSeparator(true);
                }

                if (Config.SHOW_COLUMN_ARCHITECTURE && Helper.hasActiveArchitecturePlatforms)
                {
                    GUILayout.BeginVertical(GUILayout.Width(architectureWidth));
                    {
                        GUILayout.Label(new GUIContent("Arch", "Architecture of the target platform."), EditorStyles.boldLabel);
                    }
                    GUILayout.EndVertical();

                    drawVerticalSeparator(true);
                }

                if (Config.SHOW_COLUMN_TEXTURE && Helper.hasActiveTexturePlatforms)
                {
                    GUILayout.BeginVertical(GUILayout.Width(textureWidth));
                    {
                        GUILayout.Label(new GUIContent("Texture", "Texture format"), EditorStyles.boldLabel);
                    }
                    GUILayout.EndVertical();

                    drawVerticalSeparator(true);
                }

                if (Config.SHOW_COLUMN_CACHE)
                {
                    GUILayout.BeginVertical(GUILayout.Width(cacheWidth));
                    {
                        GUILayout.Label(new GUIContent("Cache", "Cache-status of the platform."), EditorStyles.boldLabel);
                    }
                    GUILayout.EndVertical();

                    drawVerticalSeparator(true);
                }

                GUILayout.BeginVertical(GUILayout.Width(actionWidth));
                {
                    GUILayout.Label(new GUIContent("Action", "Action for the platform."), EditorStyles.boldLabel);
                }
                GUILayout.EndVertical();
            }
            GUILayout.EndHorizontal();

            Helper.SeparatorUI(0);
            //GUILayout.Box(string.Empty, new GUILayoutOption[] { GUILayout.ExpandWidth(true), GUILayout.Height(1) });
        }

        private static void drawContent()
        {
            GUILayout.BeginHorizontal();
            {
                drawColumnZebra();

                if (Config.SHOW_COLUMN_PLATFORM)
                {
                    if (Config.SHOW_COLUMN_PLATFORM_LOGO)
                    {
                        platformY = 0;
                        rowHeight = 0;
                        drawColumnLogo();
                    }

                    drawColumnPlatform();

                    drawVerticalSeparator();
                }

                if (Config.SHOW_COLUMN_ARCHITECTURE && Helper.hasActiveArchitecturePlatforms)
                {
                    drawColumnArchitecture();

                    drawVerticalSeparator();
                }

                if (Config.SHOW_COLUMN_TEXTURE && Helper.hasActiveTexturePlatforms)
                {
                    drawColumnTexture();

                    drawVerticalSeparator();
                }

                if (Config.SHOW_COLUMN_CACHE)
                {
                    drawColumnCached();

                    drawVerticalSeparator();
                }

                drawColumnAction();
            }
            GUILayout.EndHorizontal();
        }

        #endregion
    }
}
// © 2016-2018 crosstales LLC (https://www.crosstales.com)