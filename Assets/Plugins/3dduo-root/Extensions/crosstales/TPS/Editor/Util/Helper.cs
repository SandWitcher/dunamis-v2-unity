using UnityEngine;
using UnityEditor;

namespace Crosstales.TPS.Util
{
    /// <summary>Various helper functions.</summary>
    public abstract class Helper : Common.Util.BaseHelper
    {

        #region Static variables

        private static System.Collections.Generic.Dictionary<string, bool> cachedDict = new System.Collections.Generic.Dictionary<string, bool>(20);
        private static System.Collections.Generic.Dictionary<string, string> scanDict = new System.Collections.Generic.Dictionary<string, string>(20);

        private static Texture2D logo_asset;
        private static Texture2D logo_asset_small;
        private static Texture2D logo_ct;
        private static Texture2D logo_unity;

        private static Texture2D icon_reset;
        private static Texture2D icon_refresh;
        private static Texture2D icon_delete;
        private static Texture2D icon_delete_big;
        private static Texture2D icon_folder;

        private static Texture2D icon_manual;
        private static Texture2D icon_api;
        private static Texture2D icon_forum;
        private static Texture2D icon_product;

        private static Texture2D icon_check;

        private static Texture2D social_Facebook;
        private static Texture2D social_Twitter;
        private static Texture2D social_Youtube;
        private static Texture2D social_Linkedin;
        private static Texture2D social_Xing;

        private static Texture2D video_promo;
        private static Texture2D video_tutorial;

        private static Texture2D icon_videos;

        private static Texture2D logo_windows;
        private static Texture2D logo_mac;
        private static Texture2D logo_linux;
        private static Texture2D logo_ios;
        private static Texture2D logo_android;
        private static Texture2D logo_wsa;
        private static Texture2D logo_webplayer;
        private static Texture2D logo_webgl;
        private static Texture2D logo_tvos;
        private static Texture2D logo_tizen;
        private static Texture2D logo_samsungtv;
        private static Texture2D logo_ps3;
        private static Texture2D logo_ps4;
        private static Texture2D logo_psp;
        private static Texture2D logo_xbox360;
        private static Texture2D logo_xboxone;
        private static Texture2D logo_wiiu;
        private static Texture2D logo_3ds;
        private static Texture2D logo_switch;
        private static Texture2D icon_cachefull;
        private static Texture2D icon_cacheempty;
        //private static Texture2D icon_delete;

        #endregion


        #region Static properties

        public static Texture2D Logo_Asset
        {
            get
            {
                if (Constants.isPro)
                {
                    return loadImage(ref logo_asset, "logo_asset_pro.png");
                }
                else
                {
                    return loadImage(ref logo_asset, "logo_asset.png");
                }
            }
        }

        public static Texture2D Logo_Asset_Small
        {
            get
            {
                if (Constants.isPro)
                {
                    return loadImage(ref logo_asset_small, "logo_asset_small_pro.png");
                }
                else
                {
                    return loadImage(ref logo_asset_small, "logo_asset_small.png");
                }
            }
        }

        public static Texture2D Logo_CT
        {
            get
            {
                return loadImage(ref logo_ct, "logo_ct.png");
            }
        }

        public static Texture2D Logo_Unity
        {
            get
            {
                return loadImage(ref logo_unity, "logo_unity.png");
            }
        }

        public static Texture2D Icon_Reset
        {
            get
            {
                return loadImage(ref icon_reset, "icon_reset.png");
            }
        }

        public static Texture2D Icon_Refresh
        {
            get
            {
                return loadImage(ref icon_refresh, "icon_refresh.png");
            }
        }

        public static Texture2D Icon_Delete
        {
            get
            {
                return loadImage(ref icon_delete, "icon_delete.png");
            }
        }

        public static Texture2D Icon_Delete_Big
        {
            get
            {
                return loadImage(ref icon_delete_big, "icon_delete_big.png");
            }
        }

        public static Texture2D Icon_Folder
        {
            get
            {
                return loadImage(ref icon_folder, "icon_folder.png");
            }
        }

        public static Texture2D Icon_Manual
        {
            get
            {
                return loadImage(ref icon_manual, "icon_manual.png");
            }
        }

        public static Texture2D Icon_API
        {
            get
            {
                return loadImage(ref icon_api, "icon_api.png");
            }
        }

        public static Texture2D Icon_Forum
        {
            get
            {
                return loadImage(ref icon_forum, "icon_forum.png");
            }
        }

        public static Texture2D Icon_Product
        {
            get
            {
                return loadImage(ref icon_product, "icon_product.png");
            }
        }

        public static Texture2D Icon_Check
        {
            get
            {
                return loadImage(ref icon_check, "icon_check.png");
            }
        }

        public static Texture2D Social_Facebook
        {
            get
            {
                return loadImage(ref social_Facebook, "social_Facebook.png");
            }
        }

        public static Texture2D Social_Twitter
        {
            get
            {
                return loadImage(ref social_Twitter, "social_Twitter.png");
            }
        }

        public static Texture2D Social_Youtube
        {
            get
            {
                return loadImage(ref social_Youtube, "social_Youtube.png");
            }
        }

        public static Texture2D Social_Linkedin
        {
            get
            {
                return loadImage(ref social_Linkedin, "social_Linkedin.png");
            }
        }

        public static Texture2D Social_Xing
        {
            get
            {
                return loadImage(ref social_Xing, "social_Xing.png");
            }
        }

        public static Texture2D Video_Promo
        {
            get
            {
                return loadImage(ref video_promo, "video_promo.png");
            }
        }

        public static Texture2D Video_Tutorial
        {
            get
            {
                return loadImage(ref video_tutorial, "video_tutorial.png");
            }
        }

        public static Texture2D Icon_Videos
        {
            get
            {
                return loadImage(ref icon_videos, "icon_videos.png");
            }
        }

        public static Texture2D Logo_Windows
        {
            get
            {
                return loadImage(ref logo_windows, "logo_windows.png");
            }
        }

        public static Texture2D Logo_Mac
        {
            get
            {
                return loadImage(ref logo_mac, "logo_mac.png");
            }
        }

        public static Texture2D Logo_Linux
        {
            get
            {
                return loadImage(ref logo_linux, "logo_linux.png");
            }
        }

        public static Texture2D Logo_Ios
        {
            get
            {
                return loadImage(ref logo_ios, "logo_ios.png");
            }
        }

        public static Texture2D Logo_Android
        {
            get
            {
                return loadImage(ref logo_android, "logo_android.png");
            }
        }

        public static Texture2D Logo_Wsa
        {
            get
            {
                return loadImage(ref logo_wsa, "logo_wsa.png");
            }
        }

        public static Texture2D Logo_Webplayer
        {
            get
            {
                return loadImage(ref logo_webplayer, "logo_webplayer.png");
            }
        }

        public static Texture2D Logo_Webgl
        {
            get
            {
                return loadImage(ref logo_webgl, "logo_webgl.png");
            }
        }

        public static Texture2D Logo_Tvos
        {
            get
            {
                return loadImage(ref logo_tvos, "logo_tvos.png");
            }
        }

        public static Texture2D Logo_Tizen
        {
            get
            {
                return loadImage(ref logo_tizen, "logo_tizen.png");
            }
        }

        public static Texture2D Logo_Samsungtv
        {
            get
            {
                return loadImage(ref logo_samsungtv, "logo_samsungtv.png");
            }
        }

        public static Texture2D Logo_Ps3
        {
            get
            {
                return loadImage(ref logo_ps3, "logo_ps3.png");
            }
        }

        public static Texture2D Logo_Ps4
        {
            get
            {
                return loadImage(ref logo_ps4, "logo_ps4.png");
            }
        }

        public static Texture2D Logo_Psp
        {
            get
            {
                return loadImage(ref logo_psp, "logo_psp.png");
            }
        }

        public static Texture2D Logo_Xbox360
        {
            get
            {
                return loadImage(ref logo_xbox360, "logo_xbox360.png");
            }
        }

        public static Texture2D Logo_Xboxone
        {
            get
            {
                return loadImage(ref logo_xboxone, "logo_xboxone.png");
            }
        }

        public static Texture2D Logo_Wiiu
        {
            get
            {
                return loadImage(ref logo_wiiu, "logo_wiiu.png");
            }
        }

        public static Texture2D Logo_3ds
        {
            get
            {
                return loadImage(ref logo_3ds, "logo_3ds.png");
            }
        }

        public static Texture2D Logo_Switch
        {
            get
            {
                return loadImage(ref logo_switch, "logo_switch.png");
            }
        }

        public static Texture2D Icon_Cachefull
        {
            get
            {
                return loadImage(ref icon_cachefull, "icon_cachefull.png");
            }
        }

        public static Texture2D Icon_Cacheempty
        {
            get
            {
                return loadImage(ref icon_cacheempty, "icon_cacheempty.png");
            }
        }

        /// <summary>Checks if the user has selected any architecture platforms.</summary>
        /// <returns>True if the user has selected any architecture platforms.</returns>
        public static bool hasActiveArchitecturePlatforms
        {
            get
            {
                return Config.PLATFORM_WINDOWS || Config.PLATFORM_MAC || Config.PLATFORM_LINUX;
            }
        }

        /// <summary>Checks if the user has selected any texture platforms.</summary>
        /// <returns>True if the user has selected any texture platforms.</returns>
        public static bool hasActiveTexturePlatforms
        {
            get
            {
                return Config.PLATFORM_ANDROID;
            }
        }

        #endregion


        #region Public static methods

        /// <summary>Switches the current platform to the target.</summary>
        /// <param name="target">Target platform for the switch</param>
        /// <param name="build">Build type name for Unity, like 'win64'</param>
        /// <param name="subTarget">Texture format (Android)</param>
#if UNITY_5 || UNITY_2017 || UNITY_2018
        public static void SwitchPlatform(BuildTarget target, string build, MobileTextureSubtarget subTarget)
#else
        public static void SwitchPlatform(BuildTarget target, string build, AndroidBuildSubtarget subTarget)
#endif
        {
            string savePathExtension = getExtension(EditorUserBuildSettings.activeBuildTarget, EditorUserBuildSettings.androidBuildSubtarget);
            string loadPathExtension = getExtension(target, subTarget);

            if (!Config.CUSTOM_PATH_CACHE && Config.VCS != 0)
            {
                if (Config.VCS == 1)
                {
                    // git
                    if (System.IO.File.Exists(Constants.PATH + ".gitignore"))
                    {
                        string content = System.IO.File.ReadAllText(Constants.PATH + ".gitignore");
                        
                        if (!content.Contains(Constants.CACHE_DIRNAME + "/")) {
                            System.IO.File.WriteAllText(Constants.PATH + ".gitignore", content.TrimEnd() + System.Environment.NewLine + Constants.CACHE_DIRNAME + "/");
                        }
                    }
                    else{
                        System.IO.File.WriteAllText(Constants.PATH + ".gitignore", Constants.CACHE_DIRNAME + "/");
                    }
                }
                else if (Config.VCS == 2)
                {
                    // svn
                    using (System.Diagnostics.Process process = new System.Diagnostics.Process())
                    {
                        process.StartInfo.FileName = "svn";
                        process.StartInfo.Arguments = "propset svn: ignore " + Constants.CACHE_DIRNAME + ".";
                        process.StartInfo.WorkingDirectory = Constants.PATH;
                        process.StartInfo.UseShellExecute = false;

                        try
                        {
                            process.Start();

                            process.WaitForExit(Constants.KILL_TIME);
                        }
                        catch (System.Exception ex)
                        {
                            string errorMessage = "Could execute svn-ignore! Please do it manually in the console: 'svn propset svn:ignore " + Constants.CACHE_DIRNAME + ".'" + System.Environment.NewLine + ex;
                            Debug.LogError(errorMessage);
                        }
                    }
                }
                else
                {
                    // mercurial
                    Debug.LogError("Mercurial currently not supported. Please add the following lines to your .hgignore: " + System.Environment.NewLine + "syntax: glob" + System.Environment.NewLine + Constants.CACHE_DIRNAME + "/**");
                }
            }

#if UNITY_5_3 || UNITY_5_3_OR_NEWER
            UnityEditor.SceneManagement.EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
#else
            EditorApplication.SaveCurrentSceneIfUserWantsTo();
#endif

            using (System.Diagnostics.Process process = new System.Diagnostics.Process())
            {
                try
                {

                    if (Application.platform == RuntimePlatform.WindowsEditor)
                    {
                        string scriptfile = System.IO.Path.GetTempPath() + "TPS-" + System.Guid.NewGuid() + ".cmd";

                        System.IO.File.WriteAllText(scriptfile, generateWindowsScript(target, build, subTarget, savePathExtension, loadPathExtension));

                        process.StartInfo.FileName = '"' + scriptfile + '"';
                    }
                    else if (Application.platform == RuntimePlatform.OSXEditor)
                    {
                        string scriptfile = System.IO.Path.GetTempPath() + "TPS-" + System.Guid.NewGuid() + ".sh";

                        System.IO.File.WriteAllText(scriptfile, generateMacScript(target, build, subTarget, savePathExtension, loadPathExtension));

                        process.StartInfo.FileName = "/bin/sh";
                        process.StartInfo.Arguments = '"' + scriptfile + '"';
                    }
#if UNITY_5_5_OR_NEWER
                    else if (Application.platform == RuntimePlatform.LinuxEditor)
                    {
                        string scriptfile = System.IO.Path.GetTempPath() + "TPS-" + System.Guid.NewGuid() + ".sh";

                        System.IO.File.WriteAllText(scriptfile, generateLinuxScript(target, build, subTarget, savePathExtension, loadPathExtension));

                        process.StartInfo.FileName = "/bin/sh";
                        process.StartInfo.Arguments = '"' + scriptfile + '"';
                    }
#endif
                    else
                    {
                        Debug.LogError("Unsupported platform: " + Application.platform);
                        return;
                    }

                    process.StartInfo.UseShellExecute = true;

                    //Debug.LogWarning(process.StartInfo.FileName);
                    //Debug.LogWarning(process.StartInfo.Arguments);

                    process.Start();

                    EditorApplication.Exit(0);
                }
                catch (System.Exception ex)
                {
                    string errorMessage = "Could execute TPS!" + System.Environment.NewLine + ex;
                    Debug.LogError(errorMessage);
                }
            }
        }

        /// <summary>Scans the cache usage per platform.</summary>
        /// <param name="target">Target platform for the scan</param>
        /// <param name="subTarget">Texture format (Android)</param>
        /// <returns>Cache usage information.</returns>
#if UNITY_5 || UNITY_2017 || UNITY_2018
        public static string ScanCache(BuildTarget target, MobileTextureSubtarget subTarget)
#else
        public static string ScanCache (BuildTarget target, AndroidBuildSubtarget subTarget)
#endif
        {
            string result = "not cached";

            if (isCached(target, subTarget))
            {

                string key = dictKey(target, subTarget);

                if (scanDict.ContainsKey(key))
                {
                    result = scanDict[key];
                }
                else
                {
                    scanDict.Add(key, result); //To prevent double-load

                    if (System.IO.Directory.Exists(cachePath(target, subTarget)))
                    {
                        System.Threading.Thread worker;
                        if (isWindowsPlatform)
                        {
                            worker = new System.Threading.Thread(() => scanWindows(cachePath(target, subTarget), key));
                        }
                        else
                        {
                            worker = new System.Threading.Thread(() => scanUnix(cachePath(target, subTarget), key));
                        }
                        worker.Start();
                    }
                }
            }

            return result;
        }

        /// <summary>Scans the total cache usage of TPS.</summary>
        /// <returns>Total cache usage information.</returns>
        public static string ScanTotalCache()
        {
            string result = "no cache";

            string key = Config.PATH_CACHE;

            if (scanDict.ContainsKey(key))
            {
                result = scanDict[key];
            }
            else
            {
                scanDict.Add(key, result); //To prevent double-load

                if (System.IO.Directory.Exists(Config.PATH_CACHE))
                {
                    System.Threading.Thread worker;
                    if (isWindowsPlatform)
                    {
                        worker = new System.Threading.Thread(() => scanWindows(Config.PATH_CACHE, key));
                    }
                    else
                    {
                        worker = new System.Threading.Thread(() => scanUnix(Config.PATH_CACHE, key));
                    }
                    worker.Start();
                }
            }

            return result;
        }

        /// <summary>Checks if a platform is already cached.</summary>
        /// <param name="target">Platform to check</param>
        /// <param name="subTarget">Texture format (Android)</param>
        /// <returns>True if the platform is already cached</returns>
#if UNITY_5 || UNITY_2017 || UNITY_2018
        public static bool isCached(BuildTarget target, MobileTextureSubtarget subTarget)
#else
        public static bool isCached(BuildTarget target, AndroidBuildSubtarget subTarget)
#endif
        {
            string key = dictKey(target, subTarget);
            bool result = false;

            if (cachedDict.ContainsKey(key))
            {
                result = cachedDict[key];
            }
            else
            {
                result = System.IO.Directory.Exists(cachePath(target, subTarget));
                cachedDict.Add(key, result);
            }

            return result;
        }

        /// <summary>Deletes a cache for a target platform.</summary>
        /// <param name="target">Platform to delete the cache</param>
        /// <param name="subTarget">Texture format (Android)</param>
#if UNITY_5 || UNITY_2017 || UNITY_2018
        public static void DeleteCacheFromTarget(BuildTarget target, MobileTextureSubtarget subTarget)
#else
        public static void DeleteCacheFromTarget(BuildTarget target, AndroidBuildSubtarget subTarget)
#endif
        {
            if (isCached(target, subTarget))
            {
                try
                {
                    System.IO.Directory.Delete(cachePath(target, subTarget), true);

                    string key = dictKey(target, subTarget);
                    cachedDict.Remove(key);
                    scanDict.Remove(key);
                    scanDict.Remove(Config.PATH_CACHE);
                }
                catch (System.Exception ex)
                {
                    Debug.LogWarning("Could not delete the cache for target: " + target + System.Environment.NewLine + ex);
                }
            }
        }

        /// <summary>Delete the cache for all platforms.</summary>
        public static void DeleteCache()
        {
            if (System.IO.Directory.Exists(Config.PATH_CACHE))
            {
                try
                {
                    System.IO.Directory.Delete(Config.PATH_CACHE, true);

                    cachedDict.Clear();
                    scanDict.Clear();
                }
                catch (System.Exception ex)
                {
                    Debug.LogWarning("Could not delete the cache!" + System.Environment.NewLine + ex);
                }
            }
        }

        /// <summary>Delete all shell-scripts after a platform switch.</summary>
        public static void DeleteAllScripts()
        {
            //INFO: currently disabled since it could interfer with running scripts!

            //var dir = new DirectoryInfo(Path.GetTempPath());

            //try
            //{
            //    foreach (var file in dir.GetFiles("TPS-" + Constants.ASSET_ID + "*"))
            //    {
            //        if (Constants.DEBUG)
            //            Debug.Log("Script file deleted: " + file);

            //        file.Delete();
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Debug.LogWarning("Could not delete all script files!" + Environment.NewLine + ex);
            //}
        }

        /// <summary>Shows a separator-UI.</summary>
        /// <param name="space">Space in pixels between the component and the seperator line (default: 12, optional).</param>
        public static void SeparatorUI(int space = 12)
        {
            GUILayout.Space(space);
            GUILayout.Box(string.Empty, new GUILayoutOption[] { GUILayout.ExpandWidth(true), GUILayout.Height(1) });
        }

        #endregion


        #region Private static methods

        private static void scanWindows(string path, string key)
        {

            using (System.Diagnostics.Process scanProcess = new System.Diagnostics.Process())
            {

                string args = "/c dir * /s /a";

                if (Config.DEBUG)
                    Debug.Log("Process argruments: '" + args + "'");

                System.Collections.Generic.List<string> result = new System.Collections.Generic.List<string>();

                scanProcess.StartInfo.FileName = "cmd.exe";
                scanProcess.StartInfo.WorkingDirectory = path;
                scanProcess.StartInfo.Arguments = args;
                scanProcess.StartInfo.CreateNoWindow = true;
                scanProcess.StartInfo.RedirectStandardOutput = true;
                scanProcess.StartInfo.RedirectStandardError = true;
                scanProcess.StartInfo.StandardOutputEncoding = System.Text.Encoding.UTF8;
                scanProcess.StartInfo.UseShellExecute = false;
                scanProcess.OutputDataReceived += (sender, eventArgs) =>
                {
                    result.Add(eventArgs.Data);
                };

                bool success = true;

                try
                {
                    scanProcess.Start();
                    scanProcess.BeginOutputReadLine();
                }
                catch (System.Exception ex)
                {
                    success = false;
                    Debug.LogError("Could not start the scan process!" + System.Environment.NewLine + ex);
                }

                if (success)
                {

                    do
                    {
                        System.Threading.Thread.Sleep(50);
                    } while (!scanProcess.HasExited);

                    if (scanProcess.ExitCode == 0)
                    {
                        if (Config.DEBUG)
                            Debug.LogWarning("Scan completed: " + result.Count);

                        scanDict[key] = result[result.Count - 3].Trim();
                    }
                    else
                    {
                        using (System.IO.StreamReader sr = scanProcess.StandardError)
                        {
                            Debug.LogError("Could not scan the path: " + scanProcess.ExitCode + System.Environment.NewLine + sr.ReadToEnd());
                        }

                    }
                }
            }
        }

        private static void scanUnix(string path, string key)
        {
            using (System.Diagnostics.Process scanProcess = new System.Diagnostics.Process())
            {
                string args = "-sch \"" + path + '"';

                if (Config.DEBUG)
                    Debug.Log("Process argruments: '" + args + "'");

                System.Collections.Generic.List<string> result = new System.Collections.Generic.List<string>();

                scanProcess.StartInfo.FileName = "du";
                //scanProcess.StartInfo.WorkingDirectory = Path;
                scanProcess.StartInfo.Arguments = args;
                scanProcess.StartInfo.CreateNoWindow = true;
                scanProcess.StartInfo.RedirectStandardOutput = true;
                scanProcess.StartInfo.RedirectStandardError = true;
                scanProcess.StartInfo.StandardOutputEncoding = System.Text.Encoding.Default;
                //scanProcess.StartInfo.StandardOutputEncoding = Encoding.UTF8;
                //scanProcess.StartInfo.StandardOutputEncoding = Encoding.GetEncoding(850);
                scanProcess.StartInfo.UseShellExecute = false;
                scanProcess.OutputDataReceived += (sender, eventArgs) =>
                {
                    result.Add(eventArgs.Data);
                };

                bool success = true;

                try
                {
                    scanProcess.Start();
                    scanProcess.BeginOutputReadLine();
                }
                catch (System.Exception ex)
                {
                    success = false;
                    Debug.LogError("Could not start the scan process!" + System.Environment.NewLine + ex);
                }

                if (success)
                {

                    while (!scanProcess.HasExited)
                    {
                        System.Threading.Thread.Sleep(50);
                    }

                    if (scanProcess.ExitCode == 0)
                    {
                        if (Config.DEBUG)
                            Debug.LogWarning("Scan completed: " + result.Count);

                        scanDict[key] = result[result.Count - 2].Trim();
                    }
                    else
                    {
                        using (System.IO.StreamReader sr = scanProcess.StandardError)
                        {
                            Debug.LogError("Could not scan the path: " + scanProcess.ExitCode + System.Environment.NewLine + sr.ReadToEnd());
                        }
                    }
                }
            }
        }

#if UNITY_5 || UNITY_2017 || UNITY_2018
        private static string dictKey(BuildTarget target, MobileTextureSubtarget subTarget)
#else
        private static string dictKey (BuildTarget target, AndroidBuildSubtarget subTarget)
#endif
        {
            return target.ToString() + subTarget.ToString();
        }

#if UNITY_5 || UNITY_2017 || UNITY_2018
        private static string cachePath(BuildTarget target, MobileTextureSubtarget subTarget)
#else
        private static string cachePath (BuildTarget target, AndroidBuildSubtarget subTarget)
#endif
        {
            return ValidatePath(Config.PATH_CACHE + target.ToString() + getExtension(target, subTarget));
        }

#if UNITY_5 || UNITY_2017 || UNITY_2018
        private static string generateWindowsScript(BuildTarget target, string build, MobileTextureSubtarget subTarget, string savePathExtension, string loadPathExtension)
#else
        private static string generateWindowsScript(BuildTarget target, string build, AndroidBuildSubtarget subTarget, string savePathExtension, string loadPathExtension)
#endif
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            // setup
            sb.AppendLine("@echo off");
            sb.AppendLine("cls");

            // title
            sb.Append("title ");
            sb.Append(Constants.ASSET_NAME);
            sb.Append(" - Relaunch of the Unity project under ");
            sb.Append(target.ToString());
            sb.Append(loadPathExtension);
            sb.Append(" - DO NOT CLOSE THIS WINDOW!");
            sb.AppendLine();

            // header
            sb.AppendLine("echo ##############################################################################");
            sb.AppendLine("echo #                                                                            #");
            sb.Append("echo #  ");
            sb.Append(Constants.ASSET_NAME);
            sb.Append(" ");
            sb.Append(Constants.ASSET_VERSION);
            sb.Append(" - Windows");
            if (!Constants.isPro)
                sb.Append("    ");
            sb.Append("                                                   #");
            sb.AppendLine();
            sb.AppendLine("echo #  Copyright 2016-2018 by www.crosstales.com                                 #");
            sb.AppendLine("echo #                                                                            #");
            sb.AppendLine("echo #  The files will now be synchronized between the platforms.                 #");
            sb.AppendLine("echo #  This will take some time, so please be patient and DON'T CLOSE THIS       #");
            sb.AppendLine("echo #  WINDOW before the process is finished!                                    #");
            sb.AppendLine("echo #                                                                            #");
            sb.AppendLine("echo #  Unity will restart automatically after the sync.                          #");
            sb.AppendLine("echo #                                                                            #");
            sb.AppendLine("echo ##############################################################################");
            sb.AppendLine("echo.");
            sb.AppendLine("echo.");

            // check if Unity is closed
            sb.AppendLine(":waitloop");
            sb.Append("if not exist \"");
            sb.Append(Constants.PATH);
            sb.Append("Temp\\UnityLockfile\" goto waitloopend");
            sb.AppendLine();
            sb.AppendLine("echo.");
            sb.AppendLine("echo Waiting for Unity to close...");
            sb.AppendLine("timeout /t 3");

#if UNITY_4_6 || UNITY_4_7 || UNITY_5_0
            sb.Append("del \"");
            sb.Append(Constants.PATH);
            sb.Append("Temp\\UnityLockfile\" /q");
            sb.AppendLine();
#endif

            sb.AppendLine("goto waitloop");
            sb.AppendLine(":waitloopend");

            // Save files
            sb.AppendLine("echo.");
            sb.AppendLine("echo ##############################################################################");
            sb.Append("echo #  Saving files from ");
            sb.Append(EditorUserBuildSettings.activeBuildTarget.ToString());
            sb.Append(savePathExtension);
            sb.AppendLine();
            sb.AppendLine("echo ##############################################################################");

            // metadata
            sb.Append("robocopy \"");
            sb.Append(Constants.PATH);
            sb.Append("Library\\metadata\" \"");
            sb.Append(Config.PATH_CACHE);
            sb.Append(EditorUserBuildSettings.activeBuildTarget.ToString());
            sb.Append(savePathExtension);
            sb.Append("\\Library\\metadata");
            sb.Append("\" /MIR /W:3 /MT /NFL /NDL /NJH /NJS /nc /ns /np");
            sb.AppendLine();

            // ScriptAssemblies
            sb.Append("robocopy \"");
            sb.Append(Constants.PATH);
            sb.Append("Library\\ScriptAssemblies\" \"");
            sb.Append(Config.PATH_CACHE);
            sb.Append(EditorUserBuildSettings.activeBuildTarget.ToString());
            sb.Append(savePathExtension);
            sb.Append("\\Library\\ScriptAssemblies");
            sb.Append("\" /MIR /W:3 /MT /NFL /NDL /NJH /NJS /nc /ns /np");
            sb.AppendLine();

            // ShaderCache
            sb.Append("robocopy \"");
            sb.Append(Constants.PATH);
            sb.Append("Library\\ShaderCache\" \"");
            sb.Append(Config.PATH_CACHE);
            sb.Append(EditorUserBuildSettings.activeBuildTarget.ToString());
            sb.Append(savePathExtension);
            sb.Append("\\Library\\ShaderCache");
            sb.Append("\" /MIR /W:3 /MT /NFL /NDL /NJH /NJS /nc /ns /np");
            sb.AppendLine();

            // UnityAssemblies
            sb.Append("robocopy \"");
            sb.Append(Constants.PATH);
            sb.Append("Library\\UnityAssemblies\" \"");
            sb.Append(Config.PATH_CACHE);
            sb.Append(EditorUserBuildSettings.activeBuildTarget.ToString());
            sb.Append(savePathExtension);
            sb.Append("\\Library\\UnityAssemblies");
            sb.Append("\" /MIR /W:3 /MT /NFL /NDL /NJH /NJS /nc /ns /np");
            sb.AppendLine();

            // AtlasCache
            sb.Append("robocopy \"");
            sb.Append(Constants.PATH);
            sb.Append("Library\\AtlasCache\" \"");
            sb.Append(Config.PATH_CACHE);
            sb.Append(EditorUserBuildSettings.activeBuildTarget.ToString());
            sb.Append(savePathExtension);
            sb.Append("\\Library\\AtlasCache");
            sb.Append("\" /MIR /W:3 /MT /NFL /NDL /NJH /NJS /nc /ns /np");
            sb.AppendLine();

            // ProjectSettings
            if (Config.COPY_SETTINGS)
            {
                sb.Append("robocopy \"");
                sb.Append(Constants.PATH);
                sb.Append("ProjectSettings\" \"");
                sb.Append(Config.PATH_CACHE);
                sb.Append(EditorUserBuildSettings.activeBuildTarget.ToString());
                sb.Append(savePathExtension);
                sb.Append("\\ProjectSettings");
                sb.Append("\" /MIR /W:3 /MT /NFL /NDL /NJH /NJS /nc /ns /np");
                sb.AppendLine();
            }

            // Assets (meta files)
            if (Config.COPY_ASSETS)
            {
                sb.Append("robocopy \"");
                sb.Append(Constants.PATH);
                sb.Append("Assets\" \"");
                sb.Append(Config.PATH_CACHE);
                sb.Append(EditorUserBuildSettings.activeBuildTarget.ToString());
                sb.Append(savePathExtension);
                sb.Append("\\Assets");
                sb.Append("\" /MIR /W:3 /MT /NFL /NDL /NJH /NJS /nc /ns /np *.meta");
                sb.AppendLine();
            }

            // Restore files
            sb.AppendLine("echo.");
            sb.AppendLine("echo ##############################################################################");
            sb.Append("echo #  Restoring files from ");
            sb.Append(target.ToString());
            sb.Append(loadPathExtension);
            sb.AppendLine();
            sb.AppendLine("echo ##############################################################################");

            // metadata
            sb.Append("robocopy \"");
            sb.Append(Config.PATH_CACHE);
            sb.Append(target.ToString());
            sb.Append(loadPathExtension);
            sb.Append("\\Library\\metadata\" \"");
            sb.Append(Constants.PATH);
            sb.Append("Library\\metadata\" /MIR /W:3 /MT /NFL /NDL /NJH /NJS /nc /ns /np");
            sb.AppendLine();

            // ScriptAssemblies
            sb.Append("robocopy \"");
            sb.Append(Config.PATH_CACHE);
            sb.Append(target.ToString());
            sb.Append(loadPathExtension);
            sb.Append("\\Library\\ScriptAssemblies\" \"");
            sb.Append(Constants.PATH);
            sb.Append("Library\\ScriptAssemblies\" /MIR /W:3 /MT /NFL /NDL /NJH /NJS /nc /ns /np");
            sb.AppendLine();

            // ShaderCache
            sb.Append("robocopy \"");
            sb.Append(Config.PATH_CACHE);
            sb.Append(target.ToString());
            sb.Append(loadPathExtension);
            sb.Append("\\Library\\ShaderCache\" \"");
            sb.Append(Constants.PATH);
            sb.Append("Library\\ShaderCache\" /MIR /W:3 /MT /NFL /NDL /NJH /NJS /nc /ns /np");
            sb.AppendLine();

            // UnityAssemblies
            sb.Append("robocopy \"");
            sb.Append(Config.PATH_CACHE);
            sb.Append(target.ToString());
            sb.Append(loadPathExtension);
            sb.Append("\\Library\\UnityAssemblies\" \"");
            sb.Append(Constants.PATH);
            sb.Append("Library\\UnityAssemblies\" /MIR /W:3 /MT /NFL /NDL /NJH /NJS /nc /ns /np");
            sb.AppendLine();

            // AtlasCache
            sb.Append("robocopy \"");
            sb.Append(Config.PATH_CACHE);
            sb.Append(target.ToString());
            sb.Append(loadPathExtension);
            sb.Append("\\Library\\AtlasCache\" \"");
            sb.Append(Constants.PATH);
            sb.Append("Library\\AtlasCache\" /MIR /W:3 /MT /NFL /NDL /NJH /NJS /nc /ns /np");
            sb.AppendLine();

            // ProjectSettings
            if (Config.COPY_SETTINGS)
            {
                sb.Append("robocopy \"");
                sb.Append(Config.PATH_CACHE);
                sb.Append(target.ToString());
                sb.Append(loadPathExtension);
                sb.Append("\\ProjectSettings\" \"");
                sb.Append(Constants.PATH);
                sb.Append("ProjectSettings\" /MIR /W:3 /MT /NFL /NDL /NJH /NJS /nc /ns /np");
                sb.AppendLine();
            }

            // Assets (meta files)
            if (Config.COPY_ASSETS)
            {
                sb.Append("robocopy \"");
                sb.Append(Config.PATH_CACHE);
                sb.Append(target.ToString());
                sb.Append(loadPathExtension);
                sb.Append("\\Assets\" \"");
                sb.Append(Constants.PATH);
                sb.Append("Assets\" /E /W:3 /MT /NFL /NDL /NJH /NJS /nc /ns /np *.meta");
                sb.AppendLine();
            }

            // Restart Unity
            sb.AppendLine("echo.");
            sb.AppendLine("echo ##############################################################################");
            sb.AppendLine("echo #  Restarting Unity                                                          #");
            sb.AppendLine("echo ##############################################################################");
            sb.Append("start \"\" \"");
            sb.Append(Helper.ValidatePath(EditorApplication.applicationPath, false));
            sb.Append("\" -projectPath \"");
            sb.Append(Constants.PATH.Substring(0, Constants.PATH.Length - 1));
            sb.Append("\" -buildTarget ");
            sb.Append(build);

            if (Config.BATCHMODE)
            {
                sb.Append(" -batchmode");

                if (Config.QUIT)
                    sb.Append(" -quit");

                if (Config.NO_GRAPHICS)
                    sb.Append(" -nographics");
            }

#if UNITY_4_6 || UNITY_4_7 || UNITY_5_0
            if (target == BuildTarget.Android)
            {
                sb.Append(" -executeMethod Crosstales.TPS.Helper.setAndroidTexture");
                //sb.Append(subTarget);
            }
            else
            {
                if (!string.IsNullOrEmpty(Config.EXECUTE_METHOD))
                {
                    sb.Append(" -executeMethod ");
                    sb.Append(Config.EXECUTE_METHOD);
                }
            }
#else
            if (!string.IsNullOrEmpty(Config.EXECUTE_METHOD))
            {
                sb.Append(" -executeMethod ");
                sb.Append(Config.EXECUTE_METHOD);
            }
#endif
            sb.AppendLine();
            sb.AppendLine("echo.");

            // check if Unity is started
            sb.AppendLine(":waitloop2");
            sb.Append("if exist \"");
            sb.Append(Constants.PATH);
            sb.Append("Temp\\UnityLockfile\" goto waitloopend2");
            sb.AppendLine();
            sb.AppendLine("echo Waiting for Unity to start...");
            sb.AppendLine("timeout /t 3");
            sb.AppendLine("goto waitloop2");
            sb.AppendLine(":waitloopend2");
            sb.AppendLine("echo.");
            sb.AppendLine("echo Bye!");
            sb.AppendLine("timeout /t 1");
            sb.AppendLine("exit");

            return sb.ToString();
        }

#if UNITY_5 || UNITY_2017 || UNITY_2018
        private static string generateMacScript(BuildTarget target, string build, MobileTextureSubtarget subTarget, string savePathExtension, string loadPathExtension)
#else
        private static string generateMacScript(BuildTarget target, string build, AndroidBuildSubtarget subTarget, string savePathExtension, string loadPathExtension)
#endif
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            // setup
            sb.AppendLine("#!/bin/bash");
            sb.AppendLine("set +v");
            sb.AppendLine("clear");

            // title
            sb.Append("title='");
            sb.Append(Constants.ASSET_NAME);
            sb.Append(" - Relaunch of the Unity project under ");
            sb.Append(target.ToString());
            sb.Append(savePathExtension);
            sb.Append(" - DO NOT CLOSE THIS WINDOW!");
            sb.Append("'");
            sb.AppendLine();
            sb.AppendLine("echo -n -e \"\\033]0;$title\\007\"");

            // header
            sb.AppendLine("echo \"+----------------------------------------------------------------------------+\"");
            sb.AppendLine("echo \"¦                                                                            ¦\"");
            sb.Append("echo \"¦  ");
            sb.Append(Constants.ASSET_NAME);
            sb.Append(" ");
            sb.Append(Constants.ASSET_VERSION);
            sb.Append(" - macOS");
            if (!Constants.isPro)
                sb.Append("    ");
            sb.Append("                                                     ¦\"");
            sb.AppendLine();
            sb.AppendLine("echo \"¦  Copyright 2016-2018 by www.crosstales.com                                 ¦\"");
            sb.AppendLine("echo \"¦                                                                            ¦\"");
            sb.AppendLine("echo \"¦  The files will now be synchronized between the platforms.                 ¦\"");
            sb.AppendLine("echo \"¦  This will take some time, so please be patient and DON'T CLOSE THIS       ¦\"");
            sb.AppendLine("echo \"¦  WINDOW before the process is finished!                                    ¦\"");
            sb.AppendLine("echo \"¦                                                                            ¦\"");
            sb.AppendLine("echo \"¦  Unity will restart automatically after the sync.                          ¦\"");
            sb.AppendLine("echo \"¦                                                                            ¦\"");
            sb.AppendLine("echo \"+----------------------------------------------------------------------------+\"");
            sb.AppendLine("echo");
            sb.AppendLine("echo");

            // check if Unity is closed
            sb.Append("while [ -f \"");
            sb.Append(Constants.PATH);
            sb.Append("Temp/UnityLockfile\" ]");
            sb.AppendLine();
            sb.AppendLine("do");
            sb.AppendLine("  echo \"Waiting for Unity to close...\"");
            sb.AppendLine("  sleep 3");

#if UNITY_4_6 || UNITY_4_7 || UNITY_5_0
            sb.Append("  rm \"");
            sb.Append(Constants.PATH);
            sb.Append("Temp/UnityLockfile\"");
            sb.AppendLine();
#endif

            sb.AppendLine("done");

            // Save files
            sb.AppendLine("echo");
            sb.AppendLine("echo \"+----------------------------------------------------------------------------+\"");
            sb.Append("echo \"¦  Saving files from ");
            sb.Append(EditorUserBuildSettings.activeBuildTarget.ToString());
            sb.Append(loadPathExtension);
            sb.Append('"');
            sb.AppendLine();
            sb.AppendLine("echo \"+----------------------------------------------------------------------------+\"");

            // metadata
            sb.Append("mkdir -p \"");
            sb.Append(Config.PATH_CACHE);
            sb.Append(EditorUserBuildSettings.activeBuildTarget.ToString());
            sb.Append(savePathExtension);
            sb.Append("/Library/metadata");
            sb.Append('"');
            sb.AppendLine();
            sb.Append("rsync -aq --delete \"");
            sb.Append(Constants.PATH);
            sb.Append("Library/metadata\" \"");
            sb.Append(Config.PATH_CACHE);
            sb.Append(EditorUserBuildSettings.activeBuildTarget.ToString());
            sb.Append(savePathExtension);
            sb.Append("/Library");
            sb.Append("/\"");
            sb.AppendLine();

            // ScriptAssemblies
            sb.Append("mkdir -p \"");
            sb.Append(Config.PATH_CACHE);
            sb.Append(EditorUserBuildSettings.activeBuildTarget.ToString());
            sb.Append(savePathExtension);
            sb.Append("/Library/ScriptAssemblies");
            sb.Append('"');
            sb.AppendLine();
            sb.Append("rsync -aq --delete \"");
            sb.Append(Constants.PATH);
            sb.Append("Library/ScriptAssemblies\" \"");
            sb.Append(Config.PATH_CACHE);
            sb.Append(EditorUserBuildSettings.activeBuildTarget.ToString());
            sb.Append(savePathExtension);
            sb.Append("/Library");
            sb.Append("/\"");
            sb.AppendLine();

            // ShaderCache
            sb.Append("mkdir -p \"");
            sb.Append(Config.PATH_CACHE);
            sb.Append(EditorUserBuildSettings.activeBuildTarget.ToString());
            sb.Append(savePathExtension);
            sb.Append("/Library/ShaderCache");
            sb.Append('"');
            sb.AppendLine();
            sb.Append("rsync -aq --delete \"");
            sb.Append(Constants.PATH);
            sb.Append("Library/ShaderCache\" \"");
            sb.Append(Config.PATH_CACHE);
            sb.Append(EditorUserBuildSettings.activeBuildTarget.ToString());
            sb.Append(savePathExtension);
            sb.Append("/Library");
            sb.Append("/\"");
            sb.AppendLine();

            // UnityAssemblies
            sb.Append("mkdir -p \"");
            sb.Append(Config.PATH_CACHE);
            sb.Append(EditorUserBuildSettings.activeBuildTarget.ToString());
            sb.Append(savePathExtension);
            sb.Append("/Library/UnityAssemblies");
            sb.Append('"');
            sb.AppendLine();
            sb.Append("rsync -aq --delete \"");
            sb.Append(Constants.PATH);
            sb.Append("Library/UnityAssemblies\" \"");
            sb.Append(Config.PATH_CACHE);
            sb.Append(EditorUserBuildSettings.activeBuildTarget.ToString());
            sb.Append(savePathExtension);
            sb.Append("/Library");
            sb.Append("/\"");
            sb.AppendLine();

            // AtlasCache
            sb.Append("mkdir -p \"");
            sb.Append(Config.PATH_CACHE);
            sb.Append(EditorUserBuildSettings.activeBuildTarget.ToString());
            sb.Append(savePathExtension);
            sb.Append("/Library/AtlasCache");
            sb.Append('"');
            sb.AppendLine();
            sb.Append("rsync -aq --delete \"");
            sb.Append(Constants.PATH);
            sb.Append("Library/AtlasCache\" \"");
            sb.Append(Config.PATH_CACHE);
            sb.Append(EditorUserBuildSettings.activeBuildTarget.ToString());
            sb.Append(savePathExtension);
            sb.Append("/Library");
            sb.Append("/\"");
            sb.AppendLine();

            // ProjectSettings
            if (Config.COPY_SETTINGS)
            {
                sb.Append("mkdir -p \"");
                sb.Append(Config.PATH_CACHE);
                sb.Append(EditorUserBuildSettings.activeBuildTarget.ToString());
                sb.Append(savePathExtension);
                sb.Append("/ProjectSettings");
                sb.Append('"');
                sb.AppendLine();
                sb.Append("rsync -aq --delete \"");
                sb.Append(Constants.PATH);
                sb.Append("ProjectSettings/\" \"");
                sb.Append(Config.PATH_CACHE);
                sb.Append(EditorUserBuildSettings.activeBuildTarget.ToString());
                sb.Append(savePathExtension);
                sb.Append("/ProjectSettings");
                sb.Append("/\"");
                sb.AppendLine();
            }

            // Assets (meta files)
            if (Config.COPY_ASSETS) //TODO test it!
            {
                sb.Append("mkdir -p \"");
                sb.Append(Config.PATH_CACHE);
                sb.Append(EditorUserBuildSettings.activeBuildTarget.ToString());
                sb.Append(savePathExtension);
                sb.Append("/Assets");
                sb.Append('"');
                sb.AppendLine();
                sb.Append("rsync -aq --delete --include=\"*/\" --include=\"*.meta\" --exclude=\"*\" \"");
                sb.Append(Constants.PATH);
                sb.Append("Assets/\" \"");
                sb.Append(Config.PATH_CACHE);
                sb.Append(EditorUserBuildSettings.activeBuildTarget.ToString());
                sb.Append(savePathExtension);
                sb.Append("/Assets");
                sb.Append("/\"");
                sb.AppendLine();
            }

            // Restore files
            sb.AppendLine("echo");
            sb.AppendLine("echo \"+----------------------------------------------------------------------------+\"");
            sb.Append("echo \"¦  Restoring files from ");
            sb.Append(target.ToString());
            sb.Append(loadPathExtension);
            sb.Append('"');
            sb.AppendLine();
            sb.AppendLine("echo \"+----------------------------------------------------------------------------+\"");

            // metadata
            sb.Append("rsync -aq --delete \"");
            sb.Append(Config.PATH_CACHE);
            sb.Append(target.ToString());
            sb.Append(loadPathExtension);
            sb.Append("/Library/metadata");
            sb.Append("/\" \"");
            sb.Append(Constants.PATH);
            sb.Append("Library/metadata/\"");
            sb.AppendLine();

            // ScriptAssemblies
            sb.Append("rsync -aq --delete \"");
            sb.Append(Config.PATH_CACHE);
            sb.Append(target.ToString());
            sb.Append(loadPathExtension);
            sb.Append("/Library/ScriptAssemblies");
            sb.Append("/\" \"");
            sb.Append(Constants.PATH);
            sb.Append("Library/ScriptAssemblies/\"");
            sb.AppendLine();

            // ShaderCache
            sb.Append("rsync -aq --delete \"");
            sb.Append(Config.PATH_CACHE);
            sb.Append(target.ToString());
            sb.Append(loadPathExtension);
            sb.Append("/Library/ShaderCache");
            sb.Append("/\" \"");
            sb.Append(Constants.PATH);
            sb.Append("Library/ShaderCache/\"");
            sb.AppendLine();

            // UnityAssemblies
            sb.Append("rsync -aq --delete \"");
            sb.Append(Config.PATH_CACHE);
            sb.Append(target.ToString());
            sb.Append(loadPathExtension);
            sb.Append("/Library/UnityAssemblies");
            sb.Append("/\" \"");
            sb.Append(Constants.PATH);
            sb.Append("Library/UnityAssemblies/\"");
            sb.AppendLine();

            // AtlasCache
            sb.Append("rsync -aq --delete \"");
            sb.Append(Config.PATH_CACHE);
            sb.Append(target.ToString());
            sb.Append(loadPathExtension);
            sb.Append("/Library/AtlasCache");
            sb.Append("/\" \"");
            sb.Append(Constants.PATH);
            sb.Append("Library/AtlasCache/\"");
            sb.AppendLine();

            // ProjectSettings
            if (Config.COPY_SETTINGS)
            {
                sb.Append("rsync -aq --delete \"");
                sb.Append(Config.PATH_CACHE);
                sb.Append(target.ToString());
                sb.Append(loadPathExtension);
                sb.Append("/ProjectSettings");
                sb.Append("/\" \"");
                sb.Append(Constants.PATH);
                sb.Append("ProjectSettings/\"");
                sb.AppendLine();
            }

            // Assets (meta files)
            if (Config.COPY_ASSETS) //TODO test it!
            {
                sb.Append("rsync -aq --include=\"*/\" --include=\"*.meta\" --exclude=\"*\" \"");
                sb.Append(Config.PATH_CACHE);
                sb.Append(target.ToString());
                sb.Append(loadPathExtension);
                sb.Append("/Assets");
                sb.Append("/\" \"");
                sb.Append(Constants.PATH);
                sb.Append("Assets/\"");
                sb.AppendLine();
            }

            // Restart Unity
            sb.AppendLine("echo");
            sb.AppendLine("echo \"+----------------------------------------------------------------------------+\"");
            sb.AppendLine("echo \"¦  Restarting Unity                                                          ¦\"");
            sb.AppendLine("echo \"+----------------------------------------------------------------------------+\"");
            sb.Append("open -a \"");
            sb.Append(EditorApplication.applicationPath);
            sb.Append("\" --args -projectPath \"");
            sb.Append(Constants.PATH);
            sb.Append("\" -buildTarget ");
            sb.Append(build);

            if (Config.BATCHMODE)
            {
                sb.Append(" -batchmode");

                if (Config.QUIT)
                    sb.Append(" -quit");

                if (Config.NO_GRAPHICS)
                    sb.Append(" -nographics");
            }

#if UNITY_4_6 || UNITY_4_7 || UNITY_5_0
            if (target == BuildTarget.Android)
            {
                sb.Append(" -executeMethod Crosstales.TPS.Helper.setAndroidTexture");
                //sb.Append(subTarget);
            }
            else
            {
                if (!string.IsNullOrEmpty(Config.EXECUTE_METHOD))
                {
                    sb.Append(" -executeMethod ");
                    sb.Append(Config.EXECUTE_METHOD);
                }
            }
#else
            if (!string.IsNullOrEmpty(Config.EXECUTE_METHOD))
            {
                sb.Append(" -executeMethod ");
                sb.Append(Config.EXECUTE_METHOD);
            }
#endif

            sb.AppendLine();

            //check if Unity is started
            sb.AppendLine("echo");
            sb.Append("while [ ! -f \"");
            sb.Append(Constants.PATH);
            sb.Append("Temp/UnityLockfile\" ]");
            sb.AppendLine();
            sb.AppendLine("do");
            sb.AppendLine("  echo \"Waiting for Unity to start...\"");
            sb.AppendLine("  sleep 3");
            sb.AppendLine("done");
            sb.AppendLine("echo");
            sb.AppendLine("echo \"Bye!\"");
            sb.AppendLine("sleep 1");
            sb.AppendLine("exit");

            return sb.ToString();
        }

#if UNITY_5 || UNITY_2017 || UNITY_2018
        private static string generateLinuxScript(BuildTarget target, string build, MobileTextureSubtarget subTarget, string savePathExtension, string loadPathExtension)
#else
        private static string generateLinuxScript(BuildTarget target, string build, AndroidBuildSubtarget subTarget, string savePathExtension, string loadPathExtension)
#endif
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            // setup
            sb.AppendLine("#!/bin/bash");
            sb.AppendLine("set +v");
            sb.AppendLine("clear");

            // title
            sb.Append("title='");
            sb.Append(Constants.ASSET_NAME);
            sb.Append(" - Relaunch of the Unity project under ");
            sb.Append(target.ToString());
            sb.Append(savePathExtension);
            sb.Append(" - DO NOT CLOSE THIS WINDOW!");
            sb.Append("'");
            sb.AppendLine();
            sb.AppendLine("echo -n -e \"\\033]0;$title\\007\"");

            // header
            sb.AppendLine("echo \"+----------------------------------------------------------------------------+\"");
            sb.AppendLine("echo \"¦                                                                            ¦\"");
            sb.Append("echo \"¦  ");
            sb.Append(Constants.ASSET_NAME);
            sb.Append(" ");
            sb.Append(Constants.ASSET_VERSION);
            sb.Append(" - Linux");
            if (!Constants.isPro)
                sb.Append("    ");
            sb.Append("                                                     ¦\"");
            sb.AppendLine();
            sb.AppendLine("echo \"¦  Copyright 2016-2018 by www.crosstales.com                                 ¦\"");
            sb.AppendLine("echo \"¦                                                                            ¦\"");
            sb.AppendLine("echo \"¦  The files will now be synchronized between the platforms.                 ¦\"");
            sb.AppendLine("echo \"¦  This will take some time, so please be patient and DON'T CLOSE THIS       ¦\"");
            sb.AppendLine("echo \"¦  WINDOW before the process is finished!                                    ¦\"");
            sb.AppendLine("echo \"¦                                                                            ¦\"");
            sb.AppendLine("echo \"¦  Unity will restart automatically after the sync.                          ¦\"");
            sb.AppendLine("echo \"¦                                                                            ¦\"");
            sb.AppendLine("echo \"+----------------------------------------------------------------------------+\"");
            sb.AppendLine("echo");
            sb.AppendLine("echo");

            // check if Unity is closed
            sb.Append("while [ -f \"");
            sb.Append(Constants.PATH);
            sb.Append("Temp/UnityLockfile\" ]");
            sb.AppendLine();
            sb.AppendLine("do");
            sb.AppendLine("  echo \"Waiting for Unity to close...\"");
            sb.AppendLine("  sleep 3");

#if UNITY_4_6 || UNITY_4_7 || UNITY_5_0
            sb.Append("rm \"");
            sb.Append(Constants.PATH);
            sb.Append("Temp/UnityLockfile\"");
            sb.AppendLine();
#endif

            sb.AppendLine("done");

            // Save files
            sb.AppendLine("echo");
            sb.AppendLine("echo \"+----------------------------------------------------------------------------+\"");
            sb.Append("echo \"¦  Saving files from ");
            sb.Append(EditorUserBuildSettings.activeBuildTarget.ToString());
            sb.Append(loadPathExtension);
            sb.Append('"');
            sb.AppendLine();
            sb.AppendLine("echo \"+----------------------------------------------------------------------------+\"");

            // metadata
            sb.Append("mkdir -p \"");
            sb.Append(Config.PATH_CACHE);
            sb.Append(EditorUserBuildSettings.activeBuildTarget.ToString());
            sb.Append(savePathExtension);
            sb.Append("/Library/metadata");
            sb.Append('"');
            sb.AppendLine();
            sb.Append("rsync -aq --delete \"");
            sb.Append(Constants.PATH);
            sb.Append("Library/metadata/\" \"");
            sb.Append(Config.PATH_CACHE);
            sb.Append(EditorUserBuildSettings.activeBuildTarget.ToString());
            sb.Append(savePathExtension);
            sb.Append("/Library/metadata");
            sb.Append("/\"");
            sb.AppendLine();

            // ScriptAssemblies
            sb.Append("mkdir -p \"");
            sb.Append(Config.PATH_CACHE);
            sb.Append(EditorUserBuildSettings.activeBuildTarget.ToString());
            sb.Append(savePathExtension);
            sb.Append("/Library/ScriptAssemblies");
            sb.Append('"');
            sb.AppendLine();
            sb.Append("rsync -aq --delete \"");
            sb.Append(Constants.PATH);
            sb.Append("Library/ScriptAssemblies/\" \"");
            sb.Append(Config.PATH_CACHE);
            sb.Append(EditorUserBuildSettings.activeBuildTarget.ToString());
            sb.Append(savePathExtension);
            sb.Append("/Library/ScriptAssemblies");
            sb.Append("/\"");
            sb.AppendLine();

            // ShaderCache
            sb.Append("mkdir -p \"");
            sb.Append(Config.PATH_CACHE);
            sb.Append(EditorUserBuildSettings.activeBuildTarget.ToString());
            sb.Append(savePathExtension);
            sb.Append("/Library/ShaderCache");
            sb.Append('"');
            sb.AppendLine();
            sb.Append("rsync -aq --delete \"");
            sb.Append(Constants.PATH);
            sb.Append("Library/ShaderCache/\" \"");
            sb.Append(Config.PATH_CACHE);
            sb.Append(EditorUserBuildSettings.activeBuildTarget.ToString());
            sb.Append(savePathExtension);
            sb.Append("/Library/ShaderCache");
            sb.Append("/\"");
            sb.AppendLine();

            // UnityAssemblies
            sb.Append("mkdir -p \"");
            sb.Append(Config.PATH_CACHE);
            sb.Append(EditorUserBuildSettings.activeBuildTarget.ToString());
            sb.Append(savePathExtension);
            sb.Append("/Library/UnityAssemblies");
            sb.Append('"');
            sb.AppendLine();
            sb.Append("rsync -aq --delete \"");
            sb.Append(Constants.PATH);
            sb.Append("Library/UnityAssemblies/\" \"");
            sb.Append(Config.PATH_CACHE);
            sb.Append(EditorUserBuildSettings.activeBuildTarget.ToString());
            sb.Append(savePathExtension);
            sb.Append("/Library/UnityAssemblies");
            sb.Append("/\"");
            sb.AppendLine();

            // AtlasCache
            sb.Append("mkdir -p \"");
            sb.Append(Config.PATH_CACHE);
            sb.Append(EditorUserBuildSettings.activeBuildTarget.ToString());
            sb.Append(savePathExtension);
            sb.Append("/Library/AtlasCache");
            sb.Append('"');
            sb.AppendLine();
            sb.Append("rsync -aq --delete \"");
            sb.Append(Constants.PATH);
            sb.Append("Library/AtlasCache/\" \"");
            sb.Append(Config.PATH_CACHE);
            sb.Append(EditorUserBuildSettings.activeBuildTarget.ToString());
            sb.Append(savePathExtension);
            sb.Append("/Library/AtlasCache");
            sb.Append("/\"");
            sb.AppendLine();

            // ProjectSettings
            if (Config.COPY_SETTINGS)
            {
                sb.Append("mkdir -p \"");
                sb.Append(Config.PATH_CACHE);
                sb.Append(EditorUserBuildSettings.activeBuildTarget.ToString());
                sb.Append(savePathExtension);
                sb.Append("//ProjectSettings");
                sb.Append('"');
                sb.AppendLine();
                sb.Append("rsync -aq --delete \"");
                sb.Append(Constants.PATH);
                sb.Append("ProjectSettings/\" \"");
                sb.Append(Config.PATH_CACHE);
                sb.Append(EditorUserBuildSettings.activeBuildTarget.ToString());
                sb.Append(savePathExtension);
                sb.Append("//ProjectSettings");
                sb.Append("/\"");
                sb.AppendLine();
            }

            // Assets (meta files)
            if (Config.COPY_ASSETS) //TODO test it!
            {
                sb.Append("mkdir -p \"");
                sb.Append(Config.PATH_CACHE);
                sb.Append(EditorUserBuildSettings.activeBuildTarget.ToString());
                sb.Append(savePathExtension);
                sb.Append("//Assets");
                sb.Append('"');
                sb.AppendLine();
                sb.Append("rsync -aq --delete --include=\"*/\" --include=\"*.meta\" --exclude=\"*\" \"");
                sb.Append(Constants.PATH);
                sb.Append("Assets/\" \"");
                sb.Append(Config.PATH_CACHE);
                sb.Append(EditorUserBuildSettings.activeBuildTarget.ToString());
                sb.Append(savePathExtension);
                sb.Append("//Assets");
                sb.Append("/\"");
                sb.AppendLine();
            }

            // Restore files
            sb.AppendLine("echo");
            sb.AppendLine("echo \"+----------------------------------------------------------------------------+\"");
            sb.Append("echo \"¦  Restoring files from ");
            sb.Append(target.ToString());
            sb.Append(loadPathExtension);
            sb.Append('"');
            sb.AppendLine();
            sb.AppendLine("echo \"+----------------------------------------------------------------------------+\"");

            // metadata
            sb.Append("rsync -aq --delete \"");
            sb.Append(Config.PATH_CACHE);
            sb.Append(target.ToString());
            sb.Append(loadPathExtension);
            sb.Append("/Library/metadata");
            sb.Append("/\" \"");
            sb.Append(Constants.PATH);
            sb.Append("Library/metadata/\"");
            sb.AppendLine();

            // ScriptAssemblies
            sb.Append("rsync -aq --delete \"");
            sb.Append(Config.PATH_CACHE);
            sb.Append(target.ToString());
            sb.Append(loadPathExtension);
            sb.Append("/Library/ScriptAssemblies");
            sb.Append("/\" \"");
            sb.Append(Constants.PATH);
            sb.Append("Library/ScriptAssemblies/\"");
            sb.AppendLine();

            // ShaderCache
            sb.Append("rsync -aq --delete \"");
            sb.Append(Config.PATH_CACHE);
            sb.Append(target.ToString());
            sb.Append(loadPathExtension);
            sb.Append("/Library/ShaderCache");
            sb.Append("/\" \"");
            sb.Append(Constants.PATH);
            sb.Append("Library/ShaderCache/\"");
            sb.AppendLine();

            // UnityAssemblies
            sb.Append("rsync -aq --delete \"");
            sb.Append(Config.PATH_CACHE);
            sb.Append(target.ToString());
            sb.Append(loadPathExtension);
            sb.Append("/Library/UnityAssemblies");
            sb.Append("/\" \"");
            sb.Append(Constants.PATH);
            sb.Append("Library/UnityAssemblies/\"");
            sb.AppendLine();

            // AtlasCache
            sb.Append("rsync -aq --delete \"");
            sb.Append(Config.PATH_CACHE);
            sb.Append(target.ToString());
            sb.Append(loadPathExtension);
            sb.Append("/Library/AtlasCache");
            sb.Append("/\" \"");
            sb.Append(Constants.PATH);
            sb.Append("Library/AtlasCache/\"");
            sb.AppendLine();

            // ProjectSettings
            if (Config.COPY_SETTINGS)
            {
                sb.Append("rsync -aq --delete \"");
                sb.Append(Config.PATH_CACHE);
                sb.Append(target.ToString());
                sb.Append(loadPathExtension);
                sb.Append("//ProjectSettings");
                sb.Append("/\" \"");
                sb.Append(Constants.PATH);
                sb.Append("ProjectSettings/\"");
                sb.AppendLine();
            }

            // Assets (meta files)
            if (Config.COPY_ASSETS) //TODO test it!
            {
                sb.Append("rsync -aq --include=\"*/\" --include=\"*.meta\" --exclude=\"*\" \"");
                sb.Append(Config.PATH_CACHE);
                sb.Append(target.ToString());
                sb.Append(loadPathExtension);
                sb.Append("//Assets");
                sb.Append("/\" \"");
                sb.Append(Constants.PATH);
                sb.Append("Assets/\"");
                sb.AppendLine();
            }

            // Restart Unity
            sb.AppendLine("echo");
            sb.AppendLine("echo \"+----------------------------------------------------------------------------+\"");
            sb.AppendLine("echo \"¦  Restarting Unity                                                          ¦\"");
            sb.AppendLine("echo \"+----------------------------------------------------------------------------+\"");
            //sb.Append("nohup \"");
            sb.Append('"');
            sb.Append(EditorApplication.applicationPath);
            sb.Append("\" --args -projectPath \"");
            sb.Append(Constants.PATH);
            sb.Append("\" -buildTarget ");
            sb.Append(build);

            if (Config.BATCHMODE)
            {
                sb.Append(" -batchmode");

                if (Config.QUIT)
                    sb.Append(" -quit");

                if (Config.NO_GRAPHICS)
                    sb.Append(" -nographics");
            }

#if UNITY_4_6 || UNITY_4_7 || UNITY_5_0
            if (target == BuildTarget.Android)
            {
                sb.Append(" -executeMethod Crosstales.TPS.Helper.setAndroidTexture");
                //sb.Append(subTarget);
            }
            else
            {
                if (!string.IsNullOrEmpty(Config.EXECUTE_METHOD))
                {
                    sb.Append(" -executeMethod ");
                    sb.Append(Config.EXECUTE_METHOD);
                }
            }
#else
            if (!string.IsNullOrEmpty(Config.EXECUTE_METHOD))
            {
                sb.Append(" -executeMethod ");
                sb.Append(Config.EXECUTE_METHOD);
            }
#endif

            sb.Append(" &");
            sb.AppendLine();

            // check if Unity is started
            sb.AppendLine("echo");
            sb.Append("while [ ! -f \"");
            sb.Append(Constants.PATH);
            sb.Append("Temp/UnityLockfile\" ]");
            sb.AppendLine();
            sb.AppendLine("do");
            sb.AppendLine("  echo \"Waiting for Unity to start...\"");
            sb.AppendLine("  sleep 3");
            sb.AppendLine("done");
            sb.AppendLine("echo");
            sb.AppendLine("echo \"Bye!\"");
            sb.AppendLine("sleep 1");
            sb.AppendLine("exit");

            return sb.ToString();
        }

#if UNITY_5 || UNITY_2017 || UNITY_2018
        private static string getExtension(BuildTarget target, MobileTextureSubtarget subTarget)
        {
            if (target == BuildTarget.Android && subTarget != MobileTextureSubtarget.Generic)
            {
                return "_" + subTarget;
            }

            return string.Empty;
        }
#else
        private static string getExtension(BuildTarget target, AndroidBuildSubtarget subTarget)
        {
            if (target == BuildTarget.Android && subTarget != AndroidBuildSubtarget.Generic)
            {
                return "_" + subTarget;
            }
            
            return string.Empty;
        }
#endif

#if UNITY_4_6 || UNITY_4_7
        public static void setAndroidTexture()
        {
            if (Common.Util.CTPlayerPrefs.HasKey(Constants.KEY_TEX_ANDROID))
            {
                AndroidBuildSubtarget subTarget = AndroidBuildSubtarget.Generic;

                int selectedTexture = Common.Util.CTPlayerPrefs.GetInt(Constants.KEY_TEX_ANDROID);

                if (selectedTexture == 1)
                {
                    subTarget = AndroidBuildSubtarget.DXT;
                }
                else if (selectedTexture == 2)
                {
                    subTarget = AndroidBuildSubtarget.PVRTC;
                }
                else if (selectedTexture == 3)
                {
                    subTarget = AndroidBuildSubtarget.ATC;
                }
                else if (selectedTexture == 4)
                {
                    subTarget = AndroidBuildSubtarget.ETC;
                }

                EditorUserBuildSettings.androidBuildSubtarget = subTarget;

                //Debug.Log("new format: " + subTarget);
            }
        }


#else
        public static void setAndroidTexture()
        {
            if (Common.Util.CTPlayerPrefs.HasKey(Constants.KEY_TEX_ANDROID))
            {
                MobileTextureSubtarget subTarget = MobileTextureSubtarget.Generic;

                int selectedTexture = Common.Util.CTPlayerPrefs.GetInt(Constants.KEY_TEX_ANDROID);

                if (selectedTexture == 1)
                {
                    subTarget = MobileTextureSubtarget.DXT;
                }
                else if (selectedTexture == 2)
                {
                    subTarget = MobileTextureSubtarget.PVRTC;
                }
                else if (selectedTexture == 3)
                {
                    subTarget = MobileTextureSubtarget.ETC;
                }
                else if (selectedTexture == 4)
                {
                    subTarget = MobileTextureSubtarget.ETC;
                }
                else if (selectedTexture == 5)
                {
                    subTarget = MobileTextureSubtarget.ETC2;
                }
                else if (selectedTexture == 6)
                {
                    subTarget = MobileTextureSubtarget.ASTC;
                }

                EditorUserBuildSettings.androidBuildSubtarget = subTarget;
            }
        }
#endif

        /// <summary>Loads an image as Texture2D from 'Editor Default Resources'.</summary>
        /// <param name="logo">Logo to load.</param>
        /// <param name="fileName">Name of the image.</param>
        /// <returns>Image as Texture2D from 'Editor Default Resources'.</returns>
        private static Texture2D loadImage(ref Texture2D logo, string fileName)
        {
            if (logo == null)
            {
                //logo = (Texture2D)Resources.Load(fileName, typeof(Texture2D));

#if tps_ignore_setup
                logo = (Texture2D)AssetDatabase.LoadAssetAtPath("Assets" + Config.ASSET_PATH + "Icons/" + fileName, typeof(Texture2D));
#else
                logo = (Texture2D)EditorGUIUtility.Load("TPS/" + fileName);
#endif

                if (logo == null)
                {
                    //Debug.LogWarning("Image not found: " + fileName);
                }
            }

            return logo;
        }

        #endregion
    }
}
// © 2016-2018 crosstales LLC (https://www.crosstales.com)