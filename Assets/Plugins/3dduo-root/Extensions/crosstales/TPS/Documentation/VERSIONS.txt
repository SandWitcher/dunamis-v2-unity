﻿# Turbo Platform Switch PRO - Release notes

## 1.8.2 - 26.03.2018
* Handling of git improved
* Common 1.2.6 added

## 1.8.1 - 08.02.2018
* Fix for Windows switch - speed is now fully restored
* Automatically disables the Unity Cache Server
* Better compile defines for 2017.3 and macOS added
* Common 1.2.5 added

## 1.8.0 - 07.01.2018
* Switcher-class added
* Switch platform can be called via CLI
* Batch mode for CLI operations added
* macOS fixed for 2017.3
* Common 1.2.0 added

## 1.7.2 - 22.12.2017
* Asset moved to "Plugins"
* AtlasCache included
* UpdateCheck improved
* NYCheck added
* Code cleanup

## 1.7.1 - 04.12.2017
* Asset path can now be dynamic
* Copy 'Assets' folder (meta files, EXPERIMENTAL and hidden for now)
* WSA added to the default platforms

## 1.7.0 - 10.11.2017
* Fully compatible with "Unity Collaborate"
* Compile define symbol "CT_TPS" added
* Editor scripts better organized
* id file added
* Updated for Unity 2017.3

## 1.6.3 - 21.09.2017
* Editor components improved
* Code cleanup

## 1.6.2 - 04.09.2017
* Show configuration on first launch
* Updated for Unity 2017.2

## 1.6.1 - 27.08.2017
* Editor-components improved
* DLL version released

## 1.6.0 - 15.08.2017
* GAApi added
* General code improvements

## 1.5.5 - 21.07.2017
* Official support for Unity 2017

## 1.5.4 - 15.06.2017
* SetupResources.cs further improved
* new icons added

## 1.5.3 - 07.06.2017
* SetupResources.cs improved

## 1.5.2 - 01.06.2017
* Configuration window and menu "Tools" improved:
   * Videos added (incl. promo and tutorials)
   * README added
   * VERSIONS added
   * Social-media buttons added
* Reminder changed

## 1.5.1 - 29.05.2017
* GUIDs regenerated
* Internet check improved
* Update check improved
* Reminder added

## 1.5.0 - 05.05.2017
* Used space (total/per cache) added
* Tested with Unity 2017
* Code clean-up

## 1.4.1 - 09.04.2017
* Support for HTTPS added
* Editor-components improved
* API-template improved

## 1.4.0 - 10.03.2017
* Icons relocated under "Assets/Editor Default Resources/TPS/"
* Code improvements

## 1.3.1 - 27.01.2017
* Fix for Windows 7
* Minor code improvements

## 1.3.0 - 05.01.2017
* Nintendo Switch platform added
* Support for Unity 5.6 added
* Minor code improvements

## 1.2.1 - 30.11.2016
* "using System.xy" removed and replaced with explicit namespaces to prevent conflicts with non-namespaced classes

## 1.2.0 - 24.11.2016
* Icons are no longer inside Resources (=build)
* Code clean-up

## 1.1.0 - 11.11.2016
* Cache-directory structure changed. Please delete your caches from the previous versions!
* Android texture format support
* Support for Linux editor added
* Minor code improvements

## 1.0.1 - 31.10.2016
* TPS works now with Unity 4.6 - 5.5
* Plaforms WebPlayer, WiiU and 3DS added
* Settings are managed via CTPlayerPrefs
* Delete all shell-scripts after a platform switch
* Full C# code added!
* Code clean-up

## 0.9.0 - 21.10.2016
* tvOS added
* Auto save for window added
* Selected architecture will be saved
* Fileselector for cache-path added
* Icons for platforms, cache and delete added
* Switch confirmation can now be enabled/disabled
* Columns are now configurable

## 0.7.0 - 05.10.2016
* Beta release for UAS