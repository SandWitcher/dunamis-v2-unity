﻿using System.Collections.Generic;
using UnityEngine;
using Root.DesignPatterns;

namespace I2.Loc
{

    public class RegisterGlobalParameters : GlobalSingleton<RegisterGlobalParameters>, ILocalizationParamsManager
    {

        private Dictionary<string, string> m_paramsDict;

		protected override void OnEnable()
		{
            base.OnEnable();

            if (LocalizationManager.ParamManagers.Contains(this)) return;

            LocalizationManager.ParamManagers.Add(this);
            LocalizationManager.LocalizeAll(true);
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            LocalizationManager.ParamManagers.Remove(this);
        }

        public virtual string GetParameterValue( string paramName )
        {
            if (m_paramsDict == null)
            {
                Debug.Log("Global params dict is NULL !");
                m_paramsDict = new Dictionary<string, string>();
            }

            if (m_paramsDict.ContainsKey(paramName))
            {
                return m_paramsDict[paramName];
            }
            else
            {
                return GetDefaultParameterValue(paramName);
            }
        }


        public virtual void SetParameterValue(string paramKey, string paramValue)
        {
            if (m_paramsDict == null)
            {
                m_paramsDict = new Dictionary<string, string> {{paramKey, paramValue}};
            }
            else
            {
                if (m_paramsDict.ContainsKey(paramKey))
                {
                    m_paramsDict[paramKey] = paramValue;
                }
                else
                {
                    m_paramsDict.Add(paramKey, paramValue);
                }
            }

            LocalizationManager.LocalizeAll(true);
        }

        private string GetDefaultParameterValue(string paramName)
        {
            switch (LocalizationManager.CurrentCulture.Name)
            {
                case "en-US":
                    switch (paramName)
                    {
                        case "USER_FIRSTNAME":
                            return "Practitioner\'s";
                        case "USER_LASTNAME":
                            return "Name";
                        case "PLAYER_FIRSTNAME":
                            return "Patient\'s";
                        case "PLAYER_LASTNAME":
                            return "Name";
                        default:
                            Debug.Log($"No default parameter found for {paramName}");
                            return null;
                    }
                case "fr-FR":
                    switch (paramName)
                    {
                        case "USER_FIRSTNAME":
                            return "Nom";
                        case "USER_LASTNAME":
                            return "Praticien";
                        case "PLAYER_FIRSTNAME":
                            return "Nom";
                        case "PLAYER_LASTNAME":
                            return "Patient";
                        default:
                            Debug.Log($"No default parameter found for {paramName}");
                            return null;
                    }
                default:
                    Debug.Log($"Language {LocalizationManager.CurrentCulture.Name} not handled");
                    return null;
            }
        }
	}
}