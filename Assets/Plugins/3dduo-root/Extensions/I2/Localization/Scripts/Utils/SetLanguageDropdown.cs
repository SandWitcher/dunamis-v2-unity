﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace I2.Loc
{
	[AddComponentMenu("I2/Localization/SetLanguage Dropdown")]
	public class SetLanguageDropdown : MonoBehaviour 
	{
        private TMP_Dropdown dropdown;

        #if UNITY_5_2 || UNITY_5_3 || UNITY_5_4_OR_NEWER
        private void OnEnable()
		{
            dropdown = GetComponent<TMP_Dropdown>();
            if (dropdown == null)
                return;

			var currentLanguage = LocalizationManager.CurrentLanguage;
			if (LocalizationManager.Sources.Count==0) LocalizationManager.UpdateSources();
			var languages = LocalizationManager.GetAllLanguages();

            languages[languages.IndexOf("French")] = "Français";
            
			// Fill the dropdown elements
			dropdown.ClearOptions();
			dropdown.AddOptions( languages );

			dropdown.value = languages.IndexOf( currentLanguage );
			dropdown.onValueChanged.RemoveListener( OnValueChanged );
			dropdown.onValueChanged.AddListener( OnValueChanged );
		}

		
		private void OnValueChanged( int index )
		{
			if (index<0)
			{
				index = 0;
				dropdown.value = index;
			}

			LocalizationManager.CurrentLanguage = TranslateText(dropdown.options[index].text);
        }

        private string TranslateText(string text)
        {
            return text.Contains("Français") ? "French" : text;
        }
        #endif
    }
}