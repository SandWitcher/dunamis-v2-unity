﻿//
// Copyright (c) 2013-2016 Ancient Light Studios
// All Rights Reserved
//
// http://www.ancientlightstudios.com
//

namespace AncientLightStudios.uTomate.API
{
    using System;

    /// <summary>
    /// Enum wrapper around <see cref="XcodeVersion"/>.
    /// </summary>
    [Serializable]
    public class UTXcodeVersion : UTEnum<XcodeVersion>
    {
    }
}
