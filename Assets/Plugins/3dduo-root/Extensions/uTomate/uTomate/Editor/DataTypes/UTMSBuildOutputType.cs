namespace AncientLightStudios.uTomate
{
    using System;
    using API;

    /// <summary>
    /// Enum type that wraps around <see cref="MSBuildOutputType"/>
    /// </summary>
    [Serializable]
    public class UTMSBuildOutputType : UTEnum<MSBuildOutputType>
    {
    }

    public enum MSBuildOutputType
    {
        Library,
        Exe,
        Module,
        WinExe
    }
}
