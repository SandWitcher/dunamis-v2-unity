namespace AncientLightStudios.uTomate
{
    using System;
    using API;

    [Serializable]
    public class UTLogType : UTEnum<UTEchoAction.LogType> { }
}
