//
// Copyright (c) 2013-2016 Ancient Light Studios
// All Rights Reserved
// 
// http://www.ancientlightstudios.com
//

namespace AncientLightStudios.uTomate
{
    using System;
    using System.Collections.Generic;
    using API;
    using UnityEditor;
    using UnityEngine;

    [Serializable]
    public class UTDelayedExecution : ScriptableObject, ISerializationCallbackReceiver
    {
        [SerializeField]
        private UTAutomationPlan runAfterDeserialization;

        [SerializeField]
        private string[] propertyNames = new string[0];

        [SerializeField]
        private string[] propertyValues = new string[0];

        [SerializeField]
        private bool quitEditorOnPlanFinish;

        public UTAutomationPlan RunAfterDeserialization
        {
            set { runAfterDeserialization = value; }
        }

        public bool QuitEditorOnPlanFinish
        {
            set { quitEditorOnPlanFinish = value; }
        }

        public void OnBeforeSerialize()
        {
        }


        public void AddProperty(string key, string value)
        {
            var index = Array.IndexOf(propertyNames, key);
            if (index == -1)
            {
                Array.Resize(ref propertyNames, propertyNames.Length+1);
                Array.Resize(ref propertyValues, propertyValues.Length+1);
                index = propertyNames.Length - 1;
            }
            propertyNames[index] = key;
            propertyValues[index] = value;
        }

        public void AddProperties(Dictionary<string, string> properties)
        {
            foreach (var keyValuePair in properties)
            {
                AddProperty(keyValuePair.Key, keyValuePair.Value);
            }
        }

        public void OnAfterDeserialize()
        {
            EditorApplication.update += Revive;
        }

        private void Revive()
        {
            AssetDatabase.DeleteAsset(AssetDatabase.GetAssetPath(this));
            if (UTPreferences.DebugMode)
            {
                Debug.Log("Assembly reload complete. Now running automation plan " + runAfterDeserialization.name);
            }
            // ReSharper disable once DelegateSubtraction
            EditorApplication.update -= Revive;

            var additionalProps = new Dictionary<string,string>();
            for (var i = 0; i < propertyNames.Length; i++)
            {
                additionalProps[propertyNames[i]] = propertyValues[i];
            }

            if (quitEditorOnPlanFinish)
            {
                UTomateRunner.Instance.OnRunnerFinished += delegate (bool cancelled, bool failed)
                {
                    EditorApplication.Exit(cancelled || failed ? 1 : 0);
                };
            }

            UTomate.Run(runAfterDeserialization, additionalProps);
        }

        public static UTDelayedExecution Create(UTAutomationPlan planToRun)
        {
            var path = AssetDatabase.GenerateUniqueAssetPath(UTFileUtils.FullPathToProjectPath(UTFileUtils.CombineToPath(UTFileUtils.ProjectAssets, "uTomate_Temporary_" + Guid.NewGuid().ToString() + ".asset")));
            var item = CreateInstance<UTDelayedExecution>();
            AssetDatabase.CreateAsset(item, path);
            item.RunAfterDeserialization = planToRun;
            EditorUtility.SetDirty(item);
            return item;
        }
    }
}
