//
// Copyright (c) 2013-2016 Ancient Light Studios
// All Rights Reserved
// 
// http://www.ancientlightstudios.com
//

#if UNITY_5_4 // VR: [5.4,5.4]
namespace AncientLightStudios.uTomate
{
    using System;
    using API;
    using UnityEditor;

    /// <summary>
    /// Enum wrapper around <see cref="SplashScreenStyle"/>
    /// </summary>
    [Serializable]
    public class UTSplashScreenStyle : UTEnum<SplashScreenStyle>
    {
    }
}
#endif
