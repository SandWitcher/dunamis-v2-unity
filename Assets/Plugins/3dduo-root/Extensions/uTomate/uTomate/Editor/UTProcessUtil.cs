﻿namespace AncientLightStudios.uTomate
{
    using System;
    using System.ComponentModel;
    using System.Diagnostics;
    using API;
    using UDebug = UnityEngine.Debug;

    /// <summary>
    /// This encapulates the logic of running an external process and sending it's output into the Unity console, waiting for the process to exit
    /// and to cancel the process if needed.
    /// </summary>
    public class UTProcessUtil
    {
        private readonly UTAction action;
        private readonly string outputPrefix;
        private readonly string executable;
        private readonly string arguments;
        private readonly bool useShellExecute;
        private readonly bool createNoWindow;
        private readonly bool detachAfterStart;
        private readonly string workingDirectory;
        private Process process;
        private bool outputRedirected;
        private bool cancelled;
        private bool errorClosed;
        private bool outputClosed;


        public UTProcessUtil(UTAction action, string outputPrefix, string executable, string arguments, bool useShellExecute, bool createNoWindow, bool detachAfterStart, string workingDirectory )
        {
            this.action = action;
            this.outputPrefix = outputPrefix;
            this.executable = executable;
            this.arguments = arguments;
            this.useShellExecute = useShellExecute;
            this.createNoWindow = createNoWindow;
            this.detachAfterStart = detachAfterStart;
            this.workingDirectory = workingDirectory;
        }

        public void Start()
        {
            if (process != null)
            {
                throw new InvalidOperationException("The process has already been started.");
            }
            process = new Process
            {
                StartInfo =
                {
                    FileName = executable,
                    Arguments = arguments,
                    UseShellExecute = useShellExecute,
                    CreateNoWindow = createNoWindow
                }
            };

            if (!string.IsNullOrEmpty(workingDirectory))
            {
                UTFileUtils.EnsureFolderExists(workingDirectory);
                process.StartInfo.WorkingDirectory = workingDirectory;
            }

            if (!useShellExecute && !detachAfterStart && UTPreferences.DebugMode)
            {
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;
                process.OutputDataReceived += OnProcessOnOutputDataReceived;
                process.ErrorDataReceived += OnProcessOnErrorDataReceived;
            }

            try
            {
                UDebug.Log("Starting process " + executable);
                if (UTPreferences.DebugMode)
                {
                    UDebug.Log("Args: " + arguments);
                }

                process.Start();

                if (!useShellExecute && !detachAfterStart && UTPreferences.DebugMode)
                {
                    process.BeginOutputReadLine();
                    process.BeginErrorReadLine();
                    outputRedirected = true;
                }
            }
            catch (Win32Exception e)
            {
                throw new UTFailBuildException("Couldn't start process: " + e.Message, action);
            }
            errorClosed = false;
            outputClosed = false;
            cancelled = false;
        }

        public bool Continue(bool cancel)
        {
            if (detachAfterStart)
            {
                errorClosed = true;
                outputClosed = true;
                cancelled = cancel;
                return false;
            }

            if (cancel && !cancelled)
            {
                process.Kill();
            }

            if (!outputRedirected)
            {
                // no output redirection so we can just watch the process
                return !process.HasExited;
            }
            // otherwise, we watch the streams and wait until they have been closed
            // to not run into any race condition of process close vs still having
            // output data to process.
            return !(errorClosed && outputClosed);
        }

        public void VerifyExitCode(int code = 0)
        {
            if (detachAfterStart)
            {
                if (UTPreferences.DebugMode)
                {
                    UDebug.Log("Process was spawned without waiting for it to exit. Not checking process exit code.",
                        action);
                }
                return;
            }

            if (!cancelled && process.ExitCode != code)
            {
                throw new UTFailBuildException("Process exited with exit code " + process.ExitCode + " (expected: " + code + ").", action);
            }
        }


        private void OnProcessOnErrorDataReceived(object sender, DataReceivedEventArgs argv)
        {
            if (argv.Data == null)
            {
                errorClosed = true;
                return;
            }
            UDebug.LogWarning("[" + outputPrefix + "] " + argv.Data, action);
        }

        private void OnProcessOnOutputDataReceived(object sender, DataReceivedEventArgs argv)
        {
            if (argv.Data == null)
            {
                outputClosed = true;
                return;
            }
            UDebug.Log("[" + outputPrefix + "] " + argv.Data, action);
        }
    }
}