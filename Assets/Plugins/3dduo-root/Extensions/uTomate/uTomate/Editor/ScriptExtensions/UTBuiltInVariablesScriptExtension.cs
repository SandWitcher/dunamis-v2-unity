//
// Copyright (c) 2013-2016 Ancient Light Studios
// All Rights Reserved
// 
// http://www.ancientlightstudios.com
//

namespace AncientLightStudios.uTomate
{
    using API;
    using System;
    using UnityEditor;
    using UnityEngine;

    [UTScriptExtension(0)]
    public class UTBuiltInVariablesScriptExtension : UTIContextAware
    {
        public UTContext Context
        {
            set
            {
                SetupVariables(value);
            }
        }

        private void SetupVariables(UTContext context)
        {
            context["project:root"] = UTFileUtils.ProjectRoot;
            context["project:assets"] = UTFileUtils.ProjectAssets;

            var platform = Environment.OSVersion.Platform;
            if (Application.platform == RuntimePlatform.OSXEditor)
            {
                platform = PlatformID.MacOSX; // seems to be some bug in Mono returning "Unix" when being on a mac.
            }

            context["os:platform"] = platform.ToString();

            var isUnixLike = platform == PlatformID.MacOSX || platform == PlatformID.Unix;
            context["os:pathSeparatorType"] = isUnixLike ? "Unix" : "Windows";
            context["os:pathSeparatorChar"] = isUnixLike ? "/" : "\\";
            context["os:newLine"] = Environment.NewLine;
            context["os:newLineType"] = isUnixLike ? "Unix" : "Windows";

            if (isUnixLike)
            {
                context["user:home"] = UTFileUtils.NormalizeSlashes(Environment.GetEnvironmentVariable("HOME"));
            }
            else if (platform == PlatformID.Win32NT)
            {
                context["user:home"] = UTFileUtils.NormalizeSlashes(Environment.ExpandEnvironmentVariables("%HOMEDRIVE%%HOMEPATH%"));
            }
            else
            {
                Debug.Log("Unable to detect underlying os. Property 'user:home' is not available.");
            }

            context["user:desktop"] = UTFileUtils.NormalizeSlashes(Environment.GetFolderPath(Environment.SpecialFolder.Desktop));

            context["unity:isUnityPro"] = UTils.IsUnityPro;
#if !(UNITY_5_0 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_4 || UNITY_5_5) // VR: 5.6
            context["unity:supportsAndroid"] = UTils.IsBuildTargetSupported(BuildTargetGroup.Android,  BuildTarget.Android);
            context["unity:supportsIos"] = UTils.IsBuildTargetSupported(BuildTargetGroup.iOS, BuildTarget.iOS);
            context["unity:supportsWindows"] = UTils.IsBuildTargetSupported(BuildTargetGroup.Standalone, BuildTarget.StandaloneWindows);
            context["unity:supportsLinux"] = UTils.IsBuildTargetSupported(BuildTargetGroup.Standalone, BuildTarget.StandaloneLinux);
#else
            context["unity:supportsAndroid"] = UTils.IsBuildTargetSupported(BuildTarget.Android);
            context["unity:supportsIos"] = UTils.IsBuildTargetSupported(BuildTarget.iOS);
            context["unity:supportsWindows"] = UTils.IsBuildTargetSupported(BuildTarget.StandaloneWindows);
            context["unity:supportsOsx"] = UTils.IsBuildTargetSupported(BuildTarget.StandaloneOSXIntel);
            context["unity:supportsLinux"] = UTils.IsBuildTargetSupported(BuildTarget.StandaloneLinux);
#endif

            context["unity:version"] = Application.unityVersion;

            context["utomate:debugMode"] = UTPreferences.DebugMode;

            context["unity:selectionAtPlanStart"] = Selection.activeObject;

            context["project:picard"] = "Make it so!";
            context["project:worf"] = "Torpedos ready, sir!";
            context["project:bones"] = "I'm a doctor, no game developer!";
        }
    }
}
