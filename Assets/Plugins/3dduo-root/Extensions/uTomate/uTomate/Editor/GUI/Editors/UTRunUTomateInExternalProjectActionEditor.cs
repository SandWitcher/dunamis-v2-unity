//
// Copyright (c) 2013-2016 Ancient Light Studios
// All Rights Reserved
// 
// http://www.ancientlightstudios.com
//

namespace AncientLightStudios.uTomate
{
    using API;
    using UnityEditor;

    [CustomEditor(typeof(UTRunUTomateInExternalProjectAction))]
    public class UTRunUTomateInExternalProjectActionEditor : UTInspectorBase
    {
        public override UTVisibilityDecision IsVisible(System.Reflection.FieldInfo fieldInfo)
        {
            var self = (UTRunUTomateInExternalProjectAction)target;
            switch (fieldInfo.Name)
            {
                case "failOnError":
                    return VisibleIf(self.keepRunning.HasValueOrExpression(false));

            }

            return base.IsVisible(fieldInfo);
        }
    }
}
