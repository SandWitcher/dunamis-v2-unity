namespace AncientLightStudios.uTomate
{
    using System;
    using System.Collections;
    using System.Diagnostics.CodeAnalysis;
    using System.IO;
    using System.Reflection;
    using System.Text;
    using API;
    using UnityEditor;

    [UTActionInfo(actionCategory = "Build", sinceUTomateVersion = "1.9.1")]
    [UTDoc(title = "Create MSBuild project file", description = "Creates an MSBuild project file.")]
    [UTInspectorGroups(groups = new[] {"General", "Sources", "References"})]
    public class UTCreateMsBuildProjectFileAction : UTAction
    {
        [UTInspectorHint(group = "General", order = 10)]
        [UTDoc(description = "Position of the project file. This implicitely marks the root of the project.")]
        public UTString projectFile;

        [UTInspectorHint(group = "General", order = 20)]
        [UTDoc(description = "Name of the output assembly.")]
        public UTString assemblyName;

        [UTDoc(description = "")]
        [UTInspectorHint(order = 24, group = "General")]
        public UTString outputPath;

        [UTDoc(description = "The type of output to generate.")]
        [UTInspectorHint(group = "General", order = 25, required = true)]
        public UTMSBuildOutputType outputType;

        [UTDoc(description = "")]
        [UTInspectorHint(order = 26, group = "General")]
        public UTString targetFrameworkIdentifier;

        [UTDoc(description = "")]
        [UTInspectorHint(order = 27, group = "General")]
        public UTString targetFrameworkVersion;

        [UTDoc(description = "")]
        [UTInspectorHint(order = 28, group = "General")]
        public UTString targetFrameworkProfile;

        [UTDoc(description = "")]
        [UTInspectorHint(order = 29, group = "General")]
        public UTBool copyReferencesToOutputPath;

        [UTDoc(description = "Any symbols that should be defined,separated by a semicolon or comma, e.g. 'UNITY_EDITOR;UNITY_4_0'")]
        [UTInspectorHint(order = 30, group = "General")]
        public UTString defineSymbols;

        [UTDoc(title = "Includes", description = "Source files to include into the project. These must be located relative to the project root.")]
        [UTInspectorHint(group = "Sources", order = 30)]
        public UTString[] codeFilesIncludes;

        [UTDoc(title = "Excludes", description = "Source files not to include into the project. These must be located relative to the project root.")]
        [UTInspectorHint(group = "Sources", order = 40)]
        public UTString[] codeFilesExcludes;

        [UTDoc(title = "Framework Assemblies", description = "Assemblies from the .Net framework that should be referenced (e.g System, System.XML, etc.).")]
        [UTInspectorHint(group = "Dependencies", order = 5)]
        public UTString[] includedCoreAssemblies;


        [UTDoc(title = "Include Unity DLL", description = "Should Unity DLL be added as reference to the project?")]
        [UTInspectorHint(group = "Dependencies", order = 10)]
        public UTBool includeUnityDll;

        [UTDoc(title = "Include UnityEditor DLL", description = "Should UnityEditor.dll be added as reference to the project?")]
        [UTInspectorHint(group = "Dependencies", order = 20)]
        public UTBool includeUnityEditorDll;

        [UTDoc(title = "Include UnityEditor.Graphs DLL", description = "Should UnityEditor.Graphs.dll be added as reference to the project?")]
        [UTInspectorHint(group = "Dependencies", order = 30)]
        public UTBool includeUnityGraphsDll;

        [UTDoc(description = "A list of DLL files which should be referenced when building. The UnityEngine.dll and UnityEditor.dll will be added automatically if you tick the checkboxes above.")]
        [UTInspectorHint(order = 40, group = "Dependencies")]
        public UTString[] referencedAssemblies;



        [SuppressMessage("ReSharper", "CoVariantArrayConversion")]
        public override IEnumerator Execute(UTContext context)
        {
            var theProjectFile = projectFile.EvaluateIn(context);
            if (string.IsNullOrEmpty(theProjectFile))
            {
                throw new UTFailBuildException("You need to specify a project file location.", this);
            }

            UTFileUtils.EnsureParentFolderExists(theProjectFile);

            var theProjectRoot = UTFileUtils.GetParentPath(theProjectFile);
            var theCodeFilesIncludes = EvaluateAll(codeFilesIncludes, context);
            var theCodeFilesExcludes = EvaluateAll(codeFilesExcludes, context);

            var theCodeFiles = UTFileUtils.CalculateFileset(theProjectRoot, theCodeFilesIncludes, theCodeFilesExcludes,
                UTFileUtils.FileSelectionMode.Files);
            UTFileUtils.StripBasePath(theCodeFiles, theProjectRoot);



            var builder = new StringBuilder();

            builder.Append(@"<?xml version=""1.0"" encoding=""utf-8""?>
    <Project ToolsVersion=""4.0"" DefaultTargets=""Build"" xmlns=""http://schemas.microsoft.com/developer/msbuild/2003"">
             ");

            builder.Append(@"
            <PropertyGroup>
            ");

            var theSymbols = defineSymbols.EvaluateIn(context);
            if (!string.IsNullOrEmpty(theSymbols))
            {
                builder.Append(string.Format(@"
                <DefineConstants>{0}</DefineConstants>
                ", theSymbols));
            }

            var theAssemblyName = assemblyName.EvaluateIn(context);
            if (!string.IsNullOrEmpty(theAssemblyName))
            {
                builder.Append(string.Format(@"
                <AssemblyName>{0}</AssemblyName>", theAssemblyName));
            }

            var theOutputPath = outputPath.EvaluateIn(context);
            builder.Append(string.Format(@"
                <OutputPath>{0}</OutputPath>", theOutputPath));

            var theOutputType = outputType.EvaluateIn(context);
            builder.Append(string.Format(@"
                <OutputType>{0}</OutputType>", Enum.GetName(typeof(MSBuildOutputType), theOutputType)));

            var theTargetFrameworkIdentifier = targetFrameworkIdentifier.EvaluateIn(context);
            builder.Append(string.Format(@"
                <TargetFrameworkIdentifier>{0}</TargetFrameworkIdentifier>", theTargetFrameworkIdentifier));

            var theTargetFrameworkVersion = targetFrameworkVersion.EvaluateIn(context);
            builder.Append(string.Format(@"
                <TargetFrameworkVersion>{0}</TargetFrameworkVersion>", theTargetFrameworkVersion));

            var theTargetFrameworkProfile = targetFrameworkProfile.EvaluateIn(context);
            builder.Append(string.Format(@"
                <TargetFrameworkProfile>{0}</TargetFrameworkProfile>", theTargetFrameworkProfile));

            builder.Append(@"
            </PropertyGroup>
            ");

            var theCopyReferencesToOutputPath = copyReferencesToOutputPath.EvaluateIn(context);
            if (!theCopyReferencesToOutputPath)
            {
                builder.Append(@"
            <ItemDefinitionGroup>
                <Reference>
                    <Private>False</Private>
                </Reference>
            </ItemDefinitionGroup>
            ");
            }

            builder.Append(@"
            <ItemGroup>
            ");

            var theCoreAssemblies = EvaluateAll(includedCoreAssemblies, context);
            foreach (var theCoreAssembly in theCoreAssemblies)
            {
                builder.Append(string.Format(@"
                    <Reference Include=""{0}""/>
                ", theCoreAssembly));
            }


            if (includeUnityDll.EvaluateIn(context))
            {
                builder.Append(string.Format(@"
                <Reference Include=""UnityEngine"">
                    <HintPath>{0}</HintPath>
                </Reference>
                ", UTils.UnityDll()));
            }

            if (includeUnityEditorDll.EvaluateIn(context))
            {
                builder.Append(string.Format(@"
                <Reference Include=""UnityEditor"">
                    <HintPath>{0}</HintPath>
                </Reference>
                ", UTils.UnityEditorDll()));
            }

            if (includeUnityGraphsDll.EvaluateIn(context))
            {
                builder.Append(string.Format(@"
                <Reference Include=""UnityEditor.Graphs"">
                    <HintPath>{0}</HintPath>
                </Reference>
                ", UTils.UnityEditorGraphsDll()));
            }

            var theReferencedAssemblies = EvaluateAll(referencedAssemblies, context);

            foreach (var assemblyPath in theReferencedAssemblies)
            {
                if (!File.Exists(assemblyPath))
                {
                    throw new UTFailBuildException("Referenced assembly '" + assemblyPath + "' does not exist.", this);
                }

                var assembly = Assembly.LoadFrom(assemblyPath);
                var assemblySimpleName = assembly.GetName().Name;

                builder.Append(string.Format(@"
                <Reference Include=""{0}"">
                    <HintPath>{1}</HintPath>
                </Reference>
                ", assemblySimpleName, assemblyPath));
            }



            builder.Append(@"
            </ItemGroup>
            <ItemGroup>
");

            foreach (var theCodeFile in theCodeFiles)
            {
                builder.Append(string.Format("<Compile Include=\"{0}\" />\n", theCodeFile));
            }

            builder.Append(@"
        </ItemGroup>
        <Import Project=""$(MSBuildToolsPath)\Microsoft.CSharp.targets"" />
    </Project>
");

            File.WriteAllText(theProjectFile, builder.ToString(), Encoding.UTF8);
            yield return null;
        }


        [MenuItem("Assets/Create/uTomate/Build/Create MSBuild project file", false, 392)]
        public static void AddAction()
        {
            Create<UTCreateMsBuildProjectFileAction>();
        }
    }
}
