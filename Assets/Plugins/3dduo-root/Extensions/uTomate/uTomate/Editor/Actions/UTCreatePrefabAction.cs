﻿namespace AncientLightStudios.uTomate
{
    using System.Collections;
    using API;
    using UnityEditor;
    using UnityEngine;

    [UTActionInfo(actionCategory = "Scene Manipulation", sinceUTomateVersion = "1.9.1")]
    [UTDoc(title = "Create Prefab", description = "Creates a prefab from game objects extracted from a scene.")]
    [UTInspectorGroups(groups = new[]{"General"})]
    public class UTCreatePrefabAction : UTAction
    {
        [UTDoc(description = "The game object from which the prefab should be created.")]
        [UTInspectorHint(@group = "General", order = 10, required = true)]
        public UTGameObject gameObject;

        [UTDoc(description =
            "Full path where to store the prefab. The path must be located inside of the current project.")]
        [UTInspectorHint(displayAs = UTInspectorHint.DisplayAs.OpenFileSelect, required = true, group = "General",
            order = 20)]
        public UTString prefabPath;

        [UTDoc(description = "Should the prefab be overwritten if it exists? If false, the action will do nothing if the prefab already exists.")]
        [UTInspectorHint(group = "General", order = 30)]
        public UTBool overwriteExisting;

        public override IEnumerator Execute(UTContext context)
        {
            var theGameObject = gameObject.EvaluateIn(context);
            if (theGameObject == null)
            {
                throw new UTFailBuildException("You need to specify a game object.", this);
            }

            var thePrefabPath = prefabPath.EvaluateIn(context);
            if (string.IsNullOrEmpty(thePrefabPath))
            {
                throw new UTFailBuildException("You need to specify a prefab path.", this);
            }

            if (!UTFileUtils.IsBelow(UTFileUtils.ProjectRoot, thePrefabPath))
            {
                throw new UTFailBuildException("The prefab path must be inside of the current project.", this);
            }

            var doOverwrite = overwriteExisting.EvaluateIn(context);
            var theRelativePath = UTFileUtils.StripBasePath(thePrefabPath, UTFileUtils.ProjectRoot);

            if (AssetDatabase.LoadMainAssetAtPath(theRelativePath) != null)
            {
                if (!doOverwrite)
                {
                    if (UTPreferences.DebugMode)
                    {
                        Debug.Log("Prefab at path " + theRelativePath + " already exists. Not overwriting it.");
                    }
                    yield break;
                }
                if (UTPreferences.DebugMode)
                {
                    Debug.Log("Prefab at path " + theRelativePath + " already exists. Replacing it with a new one.");
                }
            }
            UTFileUtils.EnsureParentFolderExists(thePrefabPath);

            // PrefabUtility.CreatePrefab(theRelativePath, theGameObject, ReplacePrefabOptions.ReplaceNameBased);
            PrefabUtility.SaveAsPrefabAsset(theGameObject, theRelativePath);

            if (UTPreferences.DebugMode)
            {
                Debug.Log("Prefab at path " + theRelativePath + " has been written.");
            }

        }

        [MenuItem("Assets/Create/uTomate/Scene Manipulation/Create Prefab", false, 505)]
        public static void AddAction()
        {
            Create<UTCreatePrefabAction>();
        }
    }
}