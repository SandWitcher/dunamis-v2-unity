3dduo root contains : 

* 3dduo : some home made systems or layers for plugins
	- Design Patterns : 
		- GlobalSingleton : global singleton (don't destroy from scene to scene) that automatically instantiates on an empty game object or instantiate 
		if there is a prefab with the same name as the script in Resources/Singleton
		- SceneSingleton : singleton with scene scope (destroys when changing scene)
	- Modules : 
		- Save : help functions to ease the saving with ES2
	- Utils : 
		- Extensions : class extensions
			- IListExtensions : help functions on List (Random, NextOrNul, ...)
			- TransformExtension : help for transform components (FindDeepChild, ...)

* Extensions : 
	- Best HTTP - (Pro) : http requests and more
	- Demigiant : DOTween Pro : tween and movements
	- Easy Save 2 : save plugin
	- I2 : localization plugin
	- Opencoding : interactive console
	- RSG.Promise.2.0.0 : Promises system
	- Sirenix : Odin Editor for enhanced and nice custom editors
	- svn tools : svn plugin
	- TextMesh Pro
	- uTomate : automatization processes

* Packages : usefull packages
	- 3dduoRoot-SpecificAssets : everything that's needed for the root, contains : 
		- I2Languages
		- SaveConf
		--> everything that's potentially change in a project, and that must not be in root for update/commit problems...

