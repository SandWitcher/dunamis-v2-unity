﻿using System.Collections.Generic;
using UnityEngine;

public class UITabSystem : MonoBehaviour {

    [SerializeField]
    int DefaultSelectedTabIndex = 0;

    List<UITab> tabList = new List<UITab>();

    private UITab m_selectedTab;
    public UITab SelectedTab {
        get {
            return m_selectedTab;
        }
        set {
            if(value != m_selectedTab) {
                m_selectedTab = value;
                for(int i = 0; i < tabList.Count; i++) {
                    if(tabList[i] != m_selectedTab) {
                        tabList[i].Select(false);
                    }
                }
                m_selectedTab.Select(true);
            }
        }
    }

    private void Awake() {
        tabList = new List<UITab>(gameObject.GetComponentsInChildren<UITab>());
        for(int i = 0; i < tabList.Count; i++) {
            tabList[i].Init(this);            
        }
        
        if(tabList != null && tabList.Count > 0) {
            SelectedTab = tabList[DefaultSelectedTabIndex];
        }
    }

}
