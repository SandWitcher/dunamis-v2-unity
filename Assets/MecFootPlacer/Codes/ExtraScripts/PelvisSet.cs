﻿using UnityEngine;

namespace MecFootPlacer.Codes.ExtraScripts
{
    public class PelvisSet : StateMachineBehaviour
    {
        override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            animator.GetComponent<MecFootPlacer.MecFootPlacer> ().mAdjustPelvisVertically = true;
        }
    }
}
